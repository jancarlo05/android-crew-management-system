package com.procuro.crewu;

import com.procuro.apimmdatamanagerlib.CertificationDefinition;
import com.procuro.apimmdatamanagerlib.Schedule_Business_Site_Plan;
import com.procuro.apimmdatamanagerlib.Site;
import com.procuro.apimmdatamanagerlib.User;
import com.procuro.crewu.Chat.ChatData;
import com.procuro.crewu.Schedule.Schedule;
import com.procuro.crewu.Schedule.WeeklyPickerDate;
import com.procuro.crewu.Training.Curriculum;
import com.procuro.crewu.Training.CurriculumParent;

import java.util.ArrayList;
import java.util.Date;

public class CrewUDataManager {

    public static CrewUDataManager instance = new CrewUDataManager();

    private String SPID;
    private String username;
    private String Password;
    private User user;
    private boolean rememberMe;
    private ArrayList<Site>sites;
    private ArrayList<CertificationDefinition> certificationDefinitions;
    private ArrayList<User>users;

    private String[]countries;
    private String[]countrycode;

    private String ProfilePath;
    private ArrayList<CurriculumParent>crewCurriculumList;
    private Curriculum selectedCurriculum;
    private ArrayList<WeeklyPickerDate>weeklyPickerDates;

    private Schedule_Business_Site_Plan selectedCoverDate;

    private Schedule selectedSchedule;

    private String SelectedTrainingFilter;

    private ChatData chatData;

    public ChatData getChatData() {
        return chatData;
    }

    public void setChatData(ChatData chatData) {
        this.chatData = chatData;
    }

    public Schedule getSelectedSchedule() {
        return selectedSchedule;
    }

    public void setSelectedSchedule(Schedule selectedSchedule) {
        this.selectedSchedule = selectedSchedule;
    }

    public String getSelectedTrainingFilter() {
        return SelectedTrainingFilter;
    }

    public void setSelectedTrainingFilter(String selectedTrainingFilter) {
        SelectedTrainingFilter = selectedTrainingFilter;
    }

    public Schedule_Business_Site_Plan getSelectedCoverDate() {
        return selectedCoverDate;
    }

    public void setSelectedCoverDate(Schedule_Business_Site_Plan selectedCoverDate) {
        this.selectedCoverDate = selectedCoverDate;
    }

    public ArrayList<WeeklyPickerDate> getWeeklyPickerDates() {
        return weeklyPickerDates;
    }

    public void setWeeklyPickerDates(ArrayList<WeeklyPickerDate> weeklyPickerDates) {
        this.weeklyPickerDates = weeklyPickerDates;
    }

    public Curriculum getSelectedCurriculum() {
        return selectedCurriculum;
    }

    public void setSelectedCurriculum(Curriculum selectedCurriculum) {
        this.selectedCurriculum = selectedCurriculum;
    }

    public ArrayList<CurriculumParent> getCrewCurriculumList() {
        return crewCurriculumList;
    }

    public void setCrewCurriculumList(ArrayList<CurriculumParent> crewCurriculumList) {
        this.crewCurriculumList = crewCurriculumList;
    }

    public String getProfilePath() {
        return ProfilePath;
    }

    public void setProfilePath(String profilePath) {
        ProfilePath = profilePath;
    }

    public ArrayList<User> getUsers() {
        return users;
    }

    public void setUsers(ArrayList<User> users) {
        this.users = users;
    }

    public String[] getCountries() {
        return countries;
    }

    public void setCountries(String[] countries) {
        this.countries = countries;
    }

    public String[] getCountrycode() {
        return countrycode;
    }

    public void setCountrycode(String[] countrycode) {
        this.countrycode = countrycode;
    }

    public ArrayList<CertificationDefinition> getCertificationDefinitions() {
        return certificationDefinitions;
    }

    public void setCertificationDefinitions(ArrayList<CertificationDefinition> certificationDefinitions) {
        this.certificationDefinitions = certificationDefinitions;
    }

    public ArrayList<Site> getSites() {
        return sites;
    }

    public void setSites(ArrayList<Site> sites) {
        this.sites = sites;
    }

    public String getSPID() {
        return SPID;
    }

    public void setSPID(String SPID) {
        this.SPID = SPID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean isRememberMe() {
        return rememberMe;
    }

    public void setRememberMe(boolean rememberMe) {
        this.rememberMe = rememberMe;
    }

    public static CrewUDataManager getInstance() {
        if (instance!=null){
            return instance;
        }
        return new CrewUDataManager();
    }

    public static void setInstance(CrewUDataManager instance) {
        CrewUDataManager.instance = instance;
    }
}
