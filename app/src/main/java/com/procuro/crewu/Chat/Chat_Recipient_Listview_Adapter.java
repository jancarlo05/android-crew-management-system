package com.procuro.crewu.Chat;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.procuro.apimmdatamanagerlib.User;
import com.procuro.crewu.R;

import java.util.ArrayList;

import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;


public class Chat_Recipient_Listview_Adapter extends BaseAdapter {

    // Declare Variables

    private Context mContext;
    private LayoutInflater inflater;
    private ArrayList<User>arrayList;
    private SimpleTooltip tooltip;


    public Chat_Recipient_Listview_Adapter(Context context, ArrayList<User>arrayList, SimpleTooltip tooltip) {
        mContext = context;
        inflater = LayoutInflater.from(mContext);
        this.arrayList = arrayList;
        this.tooltip = tooltip;
    }

    public class ViewHolder {
        TextView name;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("ViewHolder")
    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.chat_recipient_list_data, null);
            view.setTag(holder);

            final User user = arrayList.get(position);

            TextView name = view.findViewById(R.id.name);
            try {
                name.setText(user.getFullName());
            }catch (Exception e){
                e.printStackTrace();
            }

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ChatData.getInstance().setSelectedRecipient(user);
                    ChatActivity.DisplayChatData(user,mContext);
                    tooltip.dismiss();
                }
            });

            return view;
    }



}

