package com.procuro.crewu.Chat;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;

import com.procuro.apimmdatamanagerlib.SMSDailyOpsPlanCustomerSatisfactionItem;
import com.procuro.crewu.R;

import java.util.ArrayList;


public class Chat_Listview_Adapter extends BaseAdapter {

    // Declare Variables

    private Context mContext;
    private LayoutInflater inflater;
    private ArrayList<MessageDefault>arrayList;
    private FragmentActivity fragmentActivity;

    public Chat_Listview_Adapter(Context context,ArrayList<MessageDefault>arrayList,FragmentActivity fragmentActivity) {
        mContext = context;
        inflater = LayoutInflater.from(mContext);
        this.arrayList = arrayList;
        this.fragmentActivity = fragmentActivity;
    }

    public class ViewHolder {
        TextView name;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("ViewHolder")
    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.chat_list_data, null);
            view.setTag(holder);

            TextView name = view.findViewById(R.id.name);
            TextView message = view.findViewById(R.id.message);

            final MessageDefault messageDefault = arrayList.get(position);

            try {
                name.setText(messageDefault.getUser().getFullName());
                message.setText(messageDefault.getMessages().get(0).getMessage());
            }catch (Exception e){
                e.printStackTrace();
            }

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ChatData.getInstance().setSelectedRecipient(messageDefault.getUser());
                    ChatData.getInstance().setCompose(false);
                    Intent intent = new Intent(fragmentActivity,ChatActivity.class);
                    fragmentActivity.startActivity(intent);
                }
            });

            return view;
    }



}

