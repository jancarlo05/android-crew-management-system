package com.procuro.crewu.Chat;

import com.procuro.apimmdatamanagerlib.User;

import java.util.ArrayList;

public class MessageDefault {

    private User user;
    private ArrayList<Message>messages;

    public MessageDefault(User user, ArrayList<Message> messages) {
        this.user = user;
        this.messages = messages;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ArrayList<Message> getMessages() {
        return messages;
    }

    public void setMessages(ArrayList<Message> messages) {
        this.messages = messages;
    }
}
