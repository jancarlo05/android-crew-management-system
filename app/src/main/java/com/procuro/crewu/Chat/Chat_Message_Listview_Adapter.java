package com.procuro.crewu.Chat;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.procuro.apimmdatamanagerlib.User;
import com.procuro.crewu.R;

import java.util.ArrayList;

import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;


public class Chat_Message_Listview_Adapter extends BaseAdapter {

    // Declare Variables

    private Context mContext;
    private LayoutInflater inflater;
    private ArrayList<Message>arrayList;
    private TextView textView;
    private SimpleTooltip tooltip;


    public Chat_Message_Listview_Adapter(Context context, ArrayList<Message>arrayList) {
        mContext = context;
        inflater = LayoutInflater.from(mContext);
        this.arrayList = arrayList;
    }

    public class ViewHolder {
        TextView name;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("ViewHolder")
    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.chat_message_list_data, null);
            view.setTag(holder);

            final Message message = arrayList.get(position);

            TextView name = view.findViewById(R.id.name);
            try {
                name.setText(message.getMessage());
            }catch (Exception e){
                e.printStackTrace();
            }

            return view;
    }



}

