package com.procuro.crewu.Chat;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.procuro.apimmdatamanagerlib.User;
import com.procuro.crewu.CrewUDataManager;
import com.procuro.crewu.Header.UserHeaderFragment;
import com.procuro.crewu.R;
import com.procuro.crewu.Schedule.SwipeRecyclerViewAdapter;
import com.procuro.crewu.Tools;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;


public class ChatFragment extends Fragment {


    ListView listView;
    RadioButton chat,admin,co_worker;
    Button compose;
    TextView message;

    public ChatFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.chat_fragment, container, false);
        listView = view.findViewById(R.id.listView);
        compose = view.findViewById(R.id.compose);
        message = view.findViewById(R.id.message);

        DisplayUserHeader();

        setupOnclicks();

        CheckMessages();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        CheckMessages();
    }

    private void CheckMessages(){
        ChatData data = ChatData.getInstance();
        if (data.getMessages()!=null && data.getMessages().size()>0){
            ArrayList<MessageDefault>messages = new ArrayList<>();
            ArrayList<User>users = Tools.getActiveUser(CrewUDataManager.getInstance().getUsers());
            User currentUer = CrewUDataManager.getInstance().getUser();

            for (User user : users){
                boolean exist = false;
                ArrayList<Message>filteredMessages = new ArrayList<>();
                for (Message message : data.getMessages()){
                    if (!currentUer.userId.equalsIgnoreCase(user.userId)){
                        if (user.userId.equalsIgnoreCase(message.getReceiverID())){
                            exist = true;
                            filteredMessages.add(message);
                        }
                    }
                }
                if (exist){
                    Collections.sort(filteredMessages, new Comparator<Message>() {
                        @Override
                        public int compare(Message o1, Message o2) {
                            return o2.getDate().compareTo(o1.getDate());
                        }
                    });
                    messages.add(new MessageDefault(user,filteredMessages));
                }
            }
            DisplayChat(messages);
        }
    }

    private void setupOnclicks(){
        compose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChatData.getInstance().setCompose(true);
                ChatData.getInstance().setSelectedRecipient(null);
                Intent intent = new Intent(getActivity(),ChatActivity.class);
                getActivity().startActivity(intent);
            }
        });
    }

    private void DisplayUserHeader(){
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.user_header_fragment_container,
                new UserHeaderFragment()).commit();
    }

    private void DisplayChat(ArrayList<MessageDefault>messageDefaults){
        try {
            if (messageDefaults.size()>0){
                Chat_Listview_Adapter adapter = new Chat_Listview_Adapter(getContext(),messageDefaults,getActivity());
                listView.setAdapter(adapter);
                message.setVisibility(View.GONE);
            }

        }catch (Exception e){
            e.printStackTrace();
        }


    }
}
