package com.procuro.crewu.Chat;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;

import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.procuro.apimmdatamanagerlib.User;
import com.procuro.crewu.CrewUDataManager;
import com.procuro.crewu.Pref;
import com.procuro.crewu.R;
import com.procuro.crewu.Tools;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;

import io.github.douglasjunior.androidSimpleTooltip.OverlayView;
import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;

public class ChatActivity extends AppCompatActivity {

    LinearLayout root,recipientContainer,non_compose_container,compose_container;
    public static ListView listView;
    public static TextView recipient,chatIndicator;
    public static EditText message;
    public static Button send,home;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        root = findViewById(R.id.root);
        recipientContainer = findViewById(R.id.recipientContainer);
        listView = findViewById(R.id.listView);
        message = findViewById(R.id.message);
        send = findViewById(R.id.send);
        chatIndicator = findViewById(R.id.chatIndicator);
        non_compose_container = findViewById(R.id.non_compose_container);
        compose_container = findViewById(R.id.compose_container);
        home = findViewById(R.id.home);

        setupData();

        KeyBoandOnlisterner(root);

        setupOnclicks(this);

        DisplayChatData(ChatData.getInstance().getSelectedRecipient(),this);

    }

    private void setupData(){
        ChatData chatData =ChatData.getInstance();
        if (chatData.isCompose()){
            non_compose_container.setVisibility(View.GONE);
            compose_container.setVisibility(View.VISIBLE);
            recipient = findViewById(R.id.recipient);
        }else {
            recipient = findViewById(R.id.non_recipient);
            non_compose_container.setVisibility(View.VISIBLE);
            compose_container.setVisibility(View.GONE);
        }

    }

    private void setupOnclicks(final Context context){
        recipientContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplayRecipientPopup(context,recipientContainer,recipient);
            }
        });

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CreateMessage(context);
                message.setText("");
                Pref.getInstance().setChatPref();
            }
        });

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void CreateMessage(Context context){
        if (message.getText().toString().length()>0){
            ChatData chatData =ChatData.getInstance();
            if (chatData.getSelectedRecipient()!=null){
                User user = chatData.getSelectedRecipient();
                if (chatData.getMessages()!=null){
                    chatData.getMessages().add(new Message(user.getFullName(),user.userId,message.getText().toString(), Calendar.getInstance().getTime()));
                }else {
                    chatData.setMessages(new ArrayList<Message>());
                    chatData.getMessages().add(new Message(user.getFullName(),user.userId,message.getText().toString(), Calendar.getInstance().getTime()));
                }
                DisplayChatData(user,context);
            }else {
                Toast.makeText(this, "Please Input Recipient", Toast.LENGTH_SHORT).show();
            }
        }else {
            Toast.makeText(this, "Please Input Message", Toast.LENGTH_SHORT).show();
        }
    }
    public static void DisplayChatData(User user,Context context){
        try {
            if (user!=null){
                recipient.setText(user.getFullName());
                ArrayList<Message>messages = getMessagesByID(user.userId);
                if (messages.size()>0){
                    Chat_Message_Listview_Adapter adapter = new Chat_Message_Listview_Adapter(context,messages);
                    listView.setAdapter(adapter);
                    chatIndicator.setVisibility(View.GONE);
                    listView.setVisibility(View.VISIBLE);
                }else {
                    chatIndicator.setVisibility(View.VISIBLE);
                    listView.setVisibility(View.INVISIBLE);
                }
            }else {
                chatIndicator.setVisibility(View.VISIBLE);
                listView.setVisibility(View.INVISIBLE);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static ArrayList<Message>getMessagesByID(String id){
        ChatData chatData =ChatData.getInstance();
        ArrayList<Message>messages = new ArrayList<>();
        if (chatData.getMessages()!=null){
            for (Message message : chatData.getMessages()){
                if (message.getReceiverID().equalsIgnoreCase(id)){
                    messages.add(message);
                }
            }
        }
        if (messages.size()>1){
            Collections.sort(messages, new Comparator<Message>() {
                @Override
                public int compare(Message o1, Message o2) {
                    return o1.getDate().compareTo(o2.getDate());
                }
            });
        }
        return messages;
    }

    private void KeyBoandOnlisterner(final View contentView){
        contentView.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {

                        Rect r = new Rect();
                        contentView.getWindowVisibleDisplayFrame(r);
                        int screenHeight = contentView.getRootView().getHeight();

                        // r.bottom is the position above soft keypad or device button.
                        // if keypad is shown, the r.bottom is smaller than that before.
                        int keypadHeight = screenHeight - r.bottom;

                        ConstraintLayout.LayoutParams params = new ConstraintLayout.LayoutParams(
                                ConstraintLayout.LayoutParams.MATCH_PARENT,
                                ConstraintLayout.LayoutParams.MATCH_PARENT
                        );


                        if (keypadHeight > screenHeight * 0.15) { // 0.15 ratio is perhaps enough to determine keypad height.
                            // keyboard is opened
                            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                        }
                        else {
                            // keyboard is closed

                        }
                    }
                });
    }

    public void DisplayRecipientPopup(final Context context, View anchor,TextView textView) {

        final SimpleTooltip tooltip = new SimpleTooltip.Builder(context)
                .anchorView(anchor)
                .gravity(Gravity.BOTTOM)
                .dismissOnOutsideTouch(true)
                .dismissOnInsideTouch(false)
                .showArrow(true)
                .arrowColor(ContextCompat.getColor(context, R.color.white))
                .transparentOverlay(false)
                .overlayOffset(0)
                .highlightShape(OverlayView.HIGHLIGHT_SHAPE_RECTANGULAR)
                .overlayWindowBackgroundColor(ContextCompat.getColor(context,R.color.tooltipOverlay))
                .contentView(R.layout.chat_recipient_popup)
                .focusable(true)
                .build();
        tooltip.show();

        ListView listView = tooltip.findViewById(R.id.listView);
        TextView message = tooltip.findViewById(R.id.message);

        ArrayList<User> activeUsers = Tools.getActiveUser(CrewUDataManager.getInstance().getUsers());

        try {
            if (activeUsers.size()>0){
                Chat_Recipient_Listview_Adapter adapter = new Chat_Recipient_Listview_Adapter(context,activeUsers,tooltip);
                listView.setAdapter(adapter);
                message.setVisibility(View.GONE);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }


}
