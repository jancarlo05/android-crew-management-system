package com.procuro.crewu.Chat;

import com.procuro.apimmdatamanagerlib.User;

import java.util.ArrayList;

public class ChatData {
    public static ChatData instance = new ChatData();

    private boolean isCompose;

    public boolean isCompose() {
        return isCompose;
    }

    public void setCompose(boolean compose) {
        isCompose = compose;
    }

    private User selectedRecipient;

    private ArrayList<Message>messages;

    public ArrayList<Message> getMessages() {
        return messages;
    }

    public void setMessages(ArrayList<Message> messages) {
        this.messages = messages;
    }

    public User getSelectedRecipient() {
        return selectedRecipient;
    }

    public void setSelectedRecipient(User selectedRecipient) {
        this.selectedRecipient = selectedRecipient;
    }

    public static ChatData getInstance() {
        return instance;
    }

    public static void setInstance(ChatData instance) {
        ChatData.instance = instance;
    }
}
