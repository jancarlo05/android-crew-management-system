package com.procuro.crewu.Chat;

import java.util.Date;

public class Message {

    private String User;
    private String ReceiverID;
    private String Message;
    private Date date;
    private String SenderID;
    private String type;

    public Message(String user, String receiverID, String message, Date date, String senderID, String type) {
        User = user;
        ReceiverID = receiverID;
        Message = message;
        this.date = date;
        SenderID = senderID;
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Message(String user, String receiverID, String message, Date date, String senderID) {
        User = user;
        ReceiverID = receiverID;
        Message = message;
        this.date = date;
        SenderID = senderID;
    }

    public String getSenderID() {
        return SenderID;
    }

    public void setSenderID(String senderID) {
        SenderID = senderID;
    }

    public Message(String user, String receiverID, String message, Date date) {
        User = user;
        ReceiverID = receiverID;
        Message = message;
        this.date = date;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Message(String user, String receiverID, String message) {
        User = user;
        ReceiverID = receiverID;
        Message = message;
    }

    public String getUser() {
        return User;
    }

    public void setUser(String user) {
        User = user;
    }

    public String getReceiverID() {
        return ReceiverID;
    }

    public void setReceiverID(String receiverID) {
        ReceiverID = receiverID;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }
}
