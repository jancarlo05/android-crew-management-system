package com.procuro.crewu.HealthCheck;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.procuro.crewu.Header.UserHeaderFragment;
import com.procuro.crewu.R;
import com.procuro.crewu.Tools;

import java.text.SimpleDateFormat;
import java.util.TimeZone;

public class HealthCheckActivity extends AppCompatActivity {

    Button home;
    TextView date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_health_check);
        home = findViewById(R.id.home);
        date = findViewById(R.id.date);

        DisplayUserHeader();

        SimpleDateFormat format = new SimpleDateFormat("EEEE, MMM dd, yyyy");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        date.setText(format.format(Tools.getCurrentDateOfSite().getTime()));

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void DisplayUserHeader(){
        getSupportFragmentManager().beginTransaction().replace(R.id.user_header_fragment_container,
                new UserHeaderFragment()).commit();
    }

}
