package com.procuro.crewu;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.View;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.procuro.crewu.Chat.ChatData;

import java.io.File;

public class Pref {

    public static Pref Instance;
    private Context context;
    //
    private SharedPreferences preferences;

    public static Pref getInstance(){
        if (Instance == null) Instance = new Pref();
        return Instance;
    }

    public void Initialize(Context ctxt){
        context = ctxt;
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void setSharedpref() {
        try {
            SharedPreferences.Editor prefsEditor = preferences.edit();
            Gson gson = new Gson();

            CrewUDataManager crewUDataManager = CrewUDataManager.getInstance();

            CrewUDataManager obj = new CrewUDataManager();
            obj.setSPID(crewUDataManager.getSPID());
            obj.setUsername(crewUDataManager.getUsername());
            obj.setPassword(crewUDataManager.getPassword());
            obj.setUser(crewUDataManager.getUser());
            obj.setProfilePath(crewUDataManager.getProfilePath());
            obj.setCountries(crewUDataManager.getCountries());
            obj.setCountrycode(crewUDataManager.getCountrycode());
            obj.setRememberMe(crewUDataManager.isRememberMe());
            String crewudatamanager = gson.toJson(obj);
            prefsEditor.putString("CrewUDataManager", crewudatamanager);
            prefsEditor.apply();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public  CrewUDataManager getSharedPref() {
        Gson gson = new Gson();
        String dataManager = preferences.getString("CrewUDataManager", "");
        return gson.fromJson(dataManager, CrewUDataManager.class);
    }

    public void setChatPref() {
        try {
            SharedPreferences.Editor prefsEditor = preferences.edit();
            Gson gson = new Gson();

            ChatData chatData = ChatData.getInstance();

            ChatData obj = new ChatData();
            obj.setMessages(chatData.getMessages());
            String ChatData = gson.toJson(obj);
            prefsEditor.putString("ChatData", ChatData);
            prefsEditor.apply();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public  ChatData getChatPref() {
        Gson gson = new Gson();
        String dataManager = preferences.getString("ChatData", "");
        return gson.fromJson(dataManager, ChatData.class);
    }



}
