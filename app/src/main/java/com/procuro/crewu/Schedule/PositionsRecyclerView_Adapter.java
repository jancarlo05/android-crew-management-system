package com.procuro.crewu.Schedule;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.procuro.apimmdatamanagerlib.Certification;
import com.procuro.apimmdatamanagerlib.CertificationDefinition;
import com.procuro.crewu.CrewUDataManager;
import com.procuro.crewu.R;
import com.procuro.crewu.Tools;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import static com.procuro.crewu.Tools.FloatRGB;


public class PositionsRecyclerView_Adapter extends RecyclerView.Adapter<PositionsRecyclerView_Adapter.MyViewHolder> {

    private Context mContext;
    private LayoutInflater inflater;
    private ArrayList<Integer> arraylist;
    boolean isNull;

    public PositionsRecyclerView_Adapter(Context context, ArrayList<Integer> arraylist,boolean isNull) {
        mContext = context;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = arraylist;
        this.isNull = isNull;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view ;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.position_list_data,parent,false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        int position_info = arraylist.get(position);
        try {
            GradientDrawable shape =  new GradientDrawable();
            if (!isNull){
                shape.setCornerRadius(Tools.dpToPx(5));
                shape.setColor(Tools.getPositionColor(position_info));

                if (position_info > 100){
                    shape.setStroke(5,Tools.getPositionColor(position_info - 100));
                }else {
                    shape.setStroke(0,ContextCompat.getColor(mContext,R.color.transparent));
                }
                holder.position_info.setText(Tools.getPositionString(position_info));
                holder.position_info.setTextColor(ContextCompat.getColor(mContext,Tools.getPositionStringColor(position_info)));
                holder.position_info.setBackground(shape);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {

        return Math.min(arraylist.size(), 2);
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView position_info;

        public MyViewHolder(View itemView) {
            super(itemView);
            position_info = itemView.findViewById(R.id.position_info);
        }
    }



}
