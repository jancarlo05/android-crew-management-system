package com.procuro.crewu.Schedule;

import com.procuro.apimmdatamanagerlib.Schedule_Business_Site_Plan;

import java.util.ArrayList;
import java.util.Date;

public class Schedule {
    private Date date;
    private ArrayList<Schedule_Business_Site_Plan> schedules;



    public Schedule(Date date) {
        this.date = date;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public ArrayList<Schedule_Business_Site_Plan> getSchedules() {
        return schedules;
    }

    public void setSchedules(ArrayList<Schedule_Business_Site_Plan> schedules) {
        this.schedules = schedules;
    }
}
