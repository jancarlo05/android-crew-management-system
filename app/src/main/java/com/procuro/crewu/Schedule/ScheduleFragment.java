package com.procuro.crewu.Schedule;

import android.content.Context;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.procuro.apimmdatamanagerlib.OnCompleteListeners;
import com.procuro.apimmdatamanagerlib.SMSPositioning;
import com.procuro.apimmdatamanagerlib.Schedule_Business_Site_Plan;
import com.procuro.apimmdatamanagerlib.Site;
import com.procuro.apimmdatamanagerlib.User;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;
import com.procuro.crewu.CrewUDataManager;
import com.procuro.crewu.Header.HeaderData;
import com.procuro.crewu.Header.UserHeaderFragment;
import com.procuro.crewu.MainActivity;
import com.procuro.crewu.R;
import com.procuro.crewu.Report.EmployeeReportData;
import com.procuro.crewu.Report.ReportFragment;
import com.procuro.crewu.Report.Report_Employee_Sched_Fragment;
import com.procuro.crewu.Tools;
import com.procuro.crewu.Training.Courses.TrainingCoursesFragment;
import com.procuro.crewu.Training.Summary.TrainingSummaryFragment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.TimeZone;

import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;

import static com.procuro.crewu.Tools.getDecimalTimeDuration;


public class ScheduleFragment extends Fragment {

    public static int Employee_Schedule_Report = 0, Employee_Schedule_Summary = 1;
    private RecyclerView listView;
    private TextView date,message,total_weekly;
    private ConstraintLayout date_container;
    private Button EmployeeReport;
    private com.procuro.apimmdatamanagerlib.aPimmDataManager dataManager = aPimmDataManager.getInstance();
    private ProgressBar progressBar;
    private Button refresh;
    private CountDownTimer countDownTimer;
    private boolean isTicking;


    public ScheduleFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.schedule_fragment, container, false);

        listView = view.findViewById(R.id.listView);
        date = view.findViewById(R.id.date);
        date_container = view.findViewById(R.id.date_container);
        EmployeeReport = view.findViewById(R.id.EmployeeReport);
        message = view.findViewById(R.id.message);
        progressBar = view.findViewById(R.id.progressBar);
        total_weekly = view.findViewById(R.id.total_weekly);
        refresh = view.findViewById(R.id.refresh);

        CreateWeekPickerData();

        DisplayUserHeader();

        CheckSched(Tools.getCurrentDateOfSite(),false);

        setUpOnclicks();

        setupCOuntdownTimer();

        return view;

    }

    private void setUpOnclicks(){
        date_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplayCarrier(getContext(),date_container);
            }
        });

        EmployeeReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplayEmployeeReport();
            }
        });
    }

    public void DisplayCarrier(final Context context, View anchor) {

        final SimpleTooltip tooltip = new SimpleTooltip.Builder(context)
                .anchorView(anchor)
                .gravity(Gravity.BOTTOM)
                .dismissOnOutsideTouch(true)
                .dismissOnInsideTouch(false)
                .showArrow(true)
                .arrowColor(ContextCompat.getColor(context,R.color.white))
                .transparentOverlay(true)
                .contentView(R.layout.weekly_picker)
                .focusable(true)
                .build();
        tooltip.show();

        final NumberPicker picker =tooltip.findViewById(R.id.picker);
        Button accept = tooltip.findViewById(R.id.apply);
        picker.setWrapSelectorWheel(false);


        String[] values= null;
        final CrewUDataManager data = CrewUDataManager.getInstance();
        ArrayList<WeeklyPickerDate>weeklyPickerDates = data.getWeeklyPickerDates();
        int currentWeek = Calendar.getInstance().get(Calendar.WEEK_OF_YEAR);
        int currenyYear = Calendar.getInstance().get(Calendar.YEAR);

        if (weeklyPickerDates!=null){
            values = new String[weeklyPickerDates.size()];

            for (int i = 0; i < weeklyPickerDates.size(); i++) {
                WeeklyPickerDate weekdate = weeklyPickerDates.get(i);
                values[i] = weekdate.getName();

                Calendar calendar = Calendar.getInstance();
                calendar.setTime(weekdate.getWeedays().get(0));
                int year = calendar.get(Calendar.YEAR);
                if (year== currenyYear){
                    if (weekdate.getWeeknumber() == currentWeek){
                        currentWeek = i;
                    }
                }
            }

            picker.setDisplayedValues(values);
            picker.setMinValue(0);
            picker.setMaxValue(values.length-1);
            picker.setValue(currentWeek);

        }else {
            Toast.makeText(context, "NULL", Toast.LENGTH_SHORT).show();
        }

        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
                calendar.setTime(data.getWeeklyPickerDates().get(picker.getValue()).getWeedays().get(0));
                CheckSched(calendar,true);
                tooltip.dismiss();
            }
        });
    }

    private void CreateWeekPickerData(){
        CrewUDataManager data = CrewUDataManager.getInstance();
        if (data.getWeeklyPickerDates() ==null){
            ArrayList<WeeklyPickerDate>weeklyPickerDates = new ArrayList<>();

            int currentWeek = Calendar.getInstance().get(Calendar.WEEK_OF_YEAR);
            int startWeek = currentWeek - 10;

//            int weeksOfYear = Calendar.getInstance().getActualMaximum(Calendar.WEEK_OF_YEAR);

            for (int i = startWeek; i <currentWeek+11; i++) {
                getStartEndOFWeek(i,weeklyPickerDates);
            }
            data.setWeeklyPickerDates(weeklyPickerDates);
        }
    }

    private int getPrevYear(){
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.YEAR,-1);
        return calendar.getActualMaximum(Calendar.WEEK_OF_YEAR);
    }

    private int getFutureYear(){
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.YEAR,+1);
        return calendar.getActualMaximum(Calendar.WEEK_OF_YEAR);
    }

    void getStartEndOFWeek(int enterWeek,ArrayList<WeeklyPickerDate>weeklyPickerDates){

        SimpleDateFormat formatter = new SimpleDateFormat("MMM dd, yyyy"); // PST`
        formatter.setTimeZone(TimeZone.getTimeZone("UTC"));

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(TimeZone.getTimeZone("UTC"));
        calendar.set(Calendar.WEEK_OF_YEAR, enterWeek);
        ArrayList<Date> Weekdays = new ArrayList<>();
        int delta = -calendar.get(Calendar.DAY_OF_WEEK) + 2; //add 2 if your week start on monday
        calendar.add(Calendar.DAY_OF_MONTH, delta );
        for (int i = 0; i < 7; i++) {
            Weekdays.add(calendar.getTime());
            calendar.add(Calendar.DAY_OF_MONTH, 1);
        }

        String name = formatter.format(Weekdays.get(0))+ " to " +formatter.format(Weekdays.get(Weekdays.size()-1));
        weeklyPickerDates.add(new WeeklyPickerDate(name,Weekdays,enterWeek));

    }


    private void CheckSched(Calendar calendar, boolean hasUpdate){
        ScheduleData scheduleData = ScheduleData.getIntance();
        if (scheduleData.getSchedules()==null){
            GenerateWeekDays(calendar);
        }else {
            if (hasUpdate){
                GenerateWeekDays(calendar);
            }else {
                try {
                    date.setText(scheduleData.getTitle());
                    setupSChed(scheduleData.getStartDate(),scheduleData.getEndDate(),scheduleData.getSchedules());
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
    }

    private void GenerateWeekDays(Calendar calendar){
        progressBar.setVisibility(View.VISIBLE);
        message.setVisibility(View.VISIBLE);
        listView.setVisibility(View.INVISIBLE);
        ScheduleData scheduleData = ScheduleData.getIntance();
        try {
            ArrayList<Schedule>schedules = new ArrayList<>();
            Calendar now = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
            now.setTime(calendar.getTime());

            ArrayList<Date>Weekdays = new ArrayList<>();
            int delta = -now.get(Calendar.DAY_OF_WEEK) + 2; //add 2 if your week start on monday
            now.add(Calendar.DAY_OF_MONTH, delta );
            for (int i = 0; i < 7; i++) {
                Weekdays.add(now.getTime());
                schedules.add(new Schedule(now.getTime()));
                now.add(Calendar.DAY_OF_MONTH, 1);
            }

            SimpleDateFormat format = new SimpleDateFormat("MMM dd, yyyy");
            format.setTimeZone(TimeZone.getTimeZone("UTC"));
            date.setText(format.format(Weekdays.get(0)));
            date.append(" to ");
            date.append(format.format(Weekdays.get(Weekdays.size()-1)));

            scheduleData.setTitle(date.getText().toString());
            scheduleData.setSchedules(schedules);
            scheduleData.setStartDate(Weekdays.get(0));
            scheduleData.setEndDate(Weekdays.get((Weekdays.size()-1)));
            DownloadSchedule(Weekdays.get(0),Weekdays.get(Weekdays.size()-1),schedules);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void DownloadSchedule(final Date startDate, final Date endDate, final ArrayList<Schedule>customSched){

        listView.setVisibility(View.INVISIBLE);
        progressBar.setVisibility(View.VISIBLE);
        message.setVisibility(View.VISIBLE);

        final CrewUDataManager crewUDataManager = CrewUDataManager.getInstance();
        final SimpleDateFormat format = new SimpleDateFormat("MMM dd, yyyy");
        final User user = CrewUDataManager.getInstance().getUser();
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        final Site site = Tools.getSelectedSite();
        dataManager.getEmployeeSchedule(site.siteid, startDate, endDate, crewUDataManager.getUser().userId, new OnCompleteListeners.getEmployeeScheduleListener() {
            @Override
            public void getEmployeeScheduleCallback(ArrayList<Schedule_Business_Site_Plan> schedules, Error error) {
                if (error == null && schedules.size()>0){
                    for (Schedule schedule : customSched){
                        ArrayList<Schedule_Business_Site_Plan>filteredSchedules = new ArrayList<>();
                        for (Schedule_Business_Site_Plan plan : schedules){
                            if (format.format(schedule.getDate()).equalsIgnoreCase(format.format(plan.date))){
                                if (user.userId.equalsIgnoreCase(plan.userID)){
                                    filteredSchedules.add(plan);
                                }
                            }
                        }
                        schedule.setSchedules(filteredSchedules);
                    }
                    setupSChed(startDate,endDate,customSched);
                }else {
                    GetSchedulesByDate(startDate,endDate,customSched);
                }
            }
        });

    }

    private void setupCOuntdownTimer(){
//        /1000 * 60 * 5
        countDownTimer = new CountDownTimer(1000 * 60 * 5, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
            }
            @Override
            public void onFinish() {
                ScheduleData data = ScheduleData.getIntance();
                message.setText("Refreshing Data");
                DownloadSchedule(data.getStartDate(),data.getEndDate(),data.getSchedules());
            }
        }.start();

    }

    private void ResetCountdownTImer(){
        try {
            if (countDownTimer!=null){
                countDownTimer.cancel();
                countDownTimer.start();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void GetSchedulesByDate(final Date startDate, final Date endDate, final ArrayList<Schedule>customSched){
        CrewUDataManager crewUDataManager = CrewUDataManager.getInstance();
        final SimpleDateFormat format = new SimpleDateFormat("MMM dd, yyyy");
        final User user = CrewUDataManager.getInstance().getUser();
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        Site site = Tools.getSelectedSite();
        dataManager.getSchedulesByDate(site.siteid, startDate, endDate, new OnCompleteListeners.getEmployeeScheduleListener() {
            @Override
            public void getEmployeeScheduleCallback(ArrayList<Schedule_Business_Site_Plan> schedules, Error error) {
                if (error == null){
                    for (Schedule schedule : customSched){
                        ArrayList<Schedule_Business_Site_Plan> filteredSchedule = new ArrayList<>();
                        for (Schedule_Business_Site_Plan plan : schedules){
                            if (format.format(schedule.getDate()).equalsIgnoreCase(format.format(plan.date))){
                                if (user.userId.equalsIgnoreCase(plan.userID)) {
                                    filteredSchedule.add(plan);
                                }
                            }
                        }
                        schedule.setSchedules(filteredSchedule);
                    }
                }
                setupSChed(startDate,endDate,customSched);
            }
        });

    }

    private void setupSChed(final Date startDate, final Date endDate, final ArrayList<Schedule>schedules){
        try {
//            ScheduleParent_listAdapter adapter = new ScheduleParent_listAdapter(getContext(),schedules,getActivity());
//            listView.setAdapter(adapter);
            SwipeParentRecyclerViewAdapter adapter = new SwipeParentRecyclerViewAdapter(getContext(),schedules,getActivity());
            listView.setLayoutManager(new GridLayoutManager(getContext(),1));
            listView.setAdapter(adapter);

            listView.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
            message.setVisibility(View.GONE);

            User user = CrewUDataManager.getInstance().getUser();

            if(schedules!=null){
                if (schedules.size()>0){

                   long OverAllStart = 0;
                   long OverAllEND = 0;

                    for (Schedule schedule : schedules){
                        if (schedule.getSchedules()!=null){
                            ArrayList<Date>startDates = new ArrayList<>();
                            ArrayList<Date>endDates = new ArrayList<>();

                            for (Schedule_Business_Site_Plan schedplan : schedule.getSchedules()){
                                if (user.userId.equalsIgnoreCase(schedplan.userID)){
                                    startDates.add(schedplan.startTime);
                                    endDates.add(schedplan.endTime);
                                }
                            }
                            if (startDates.size()>0){
                                Collections.sort(startDates);
                                Collections.sort(endDates);

                                OverAllStart +=startDates.get(0).getTime();
                                OverAllEND +=endDates.get(endDates.size()-1).getTime();
                            }
                        }
                    }

                    total_weekly.setText(getDecimalTimeDuration(new Date(OverAllEND),new Date(OverAllStart)));
                }
            }

            refresh.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    message.setText("Refreshing Data");
                    ResetCountdownTImer();
                    DownloadSchedule(startDate,endDate,schedules);
                    ResetCountdownTImer();
                }
            });

            ResetCountdownTImer();
        }catch (Exception e){
            e.printStackTrace();
        }


    }

    private void DisplayUserHeader(){
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.user_header_fragment_container,
                new UserHeaderFragment()).commit();
    }

    private void DisplayEmployeeReport(){
        MainActivity.navView.setSelectedItemId(R.id.nav_report);
//        CrewUDataManager data = CrewUDataManager.getInstance();
//        EmployeeReportData employeeReportData = new EmployeeReportData();
//        employeeReportData.setReportType(Employee_Schedule_Report);
//        employeeReportData.setPositionForm(null);
//        employeeReportData.setUser(data.getUser());
//        employeeReportData.setUsers(data.getUsers());
//        employeeReportData.setSite(Tools.getSelectedSite());
//        employeeReportData.setSPID(data.getSPID());
//        EmployeeReportData.setInstance(employeeReportData);
//        HeaderData headerData = HeaderData.getInstance();
//
//        headerData.setSched_EmployeeReportEnabled(true);
//        headerData.setTitle(employeeReportData.getTitle());
//        HeaderData.DisplayHeaderFragment(getActivity());
//        getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_left,R.anim.slide_left_out).replace(R.id.resturant_fragment_container,
//                new ReportFragment()).commit();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (countDownTimer!=null){
            countDownTimer.cancel();;
        }
    }
}
