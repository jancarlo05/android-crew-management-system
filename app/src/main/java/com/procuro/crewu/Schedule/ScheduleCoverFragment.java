package com.procuro.crewu.Schedule;

import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;

import com.procuro.apimmdatamanagerlib.Schedule_Business_Site_Plan;
import com.procuro.apimmdatamanagerlib.User;
import com.procuro.crewu.CrewUDataManager;
import com.procuro.crewu.R;
import com.procuro.crewu.Tools;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.TimeZone;


public class ScheduleCoverFragment extends Fragment {

    ListView listView;
    private TextView date,start_time,end_time,position,total_hours;
    private Button cover_request;
    private CheckBox selectAll;

    public ScheduleCoverFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.schedule_cover_fragment, container, false);

        date = view.findViewById(R.id.date);
        position  = view.findViewById(R.id.position_info);
        start_time = view.findViewById(R.id.startTime);
        end_time = view.findViewById(R.id.endTime);
        total_hours = view.findViewById(R.id.total_hours);
        listView = view.findViewById(R.id.listView);
        cover_request =view.findViewById(R.id.cover_request);
        selectAll = view.findViewById(R.id.select_all);


        CheckInstance();

        DisplayCoverList(selectAll.isChecked());

        setUpOnClicks();


        return view;

    }

    private void CheckInstance(){
        Schedule schedule = CrewUDataManager.getInstance().getSelectedSchedule();
        if (schedule!=null){
            CheckParent();
        }else {
            CheckCHild();
        }
    }

    private void CheckCHild(){
        Schedule_Business_Site_Plan schedule = CrewUDataManager.getInstance().getSelectedCoverDate();
        if (schedule!=null){
            SimpleDateFormat format = new SimpleDateFormat("EEEE MM/dd");
            format.setTimeZone(TimeZone.getTimeZone("UTC"));
            String dateFormat = format.format(schedule.date).replace(" ","\n");
            date.setText(dateFormat);

            SimpleDateFormat timeFormat = new SimpleDateFormat("h:mm a");
            timeFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

            try {
                start_time.setText(timeFormat.format(schedule.startTime));
            }catch (Exception e){
                e.printStackTrace();
            }
            try {
                end_time.setText(timeFormat.format(schedule.endTime));
            }catch (Exception e){
                e.printStackTrace();
            }
            try {
                long diff = schedule.endTime.getTime() - schedule.startTime.getTime();
                int diffhours = (int) (diff / (60 * 60 * 1000));
                int diffmin = (int) (diffhours / (60 * 1000));
                total_hours.setText(String.format("%02d", diffhours)+":"+String.format("%02d", diffmin));
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    private void CheckParent(){
        Schedule schedule = CrewUDataManager.getInstance().getSelectedSchedule();
        if (schedule!=null){
            SimpleDateFormat format = new SimpleDateFormat("EEEE MM/dd");
            format.setTimeZone(TimeZone.getTimeZone("UTC"));
            String dateFormat = format.format(schedule.getDate()).replace(" ","\n");
            date.setText(dateFormat);

            SimpleDateFormat timeFormat = new SimpleDateFormat("h:mm a");
            timeFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

            ArrayList<Date>startDates = new ArrayList<>();
            ArrayList<Date>endDates = new ArrayList<>();

            if (schedule.getSchedules()!=null){

                Collections.sort(schedule.getSchedules(), new Comparator<Schedule_Business_Site_Plan>() {
                    @Override
                    public int compare(Schedule_Business_Site_Plan o1, Schedule_Business_Site_Plan o2) {
                        return o1.startTime.compareTo(o2.startTime);
                    }
                });

                for (Schedule_Business_Site_Plan plan : schedule.getSchedules()){
                    startDates.add(plan.startTime);
                    endDates.add(plan.endTime);
                }
            }

            if (startDates.size()>0){
                Collections.sort(startDates);
                Collections.sort(endDates);
                start_time.setText(timeFormat.format(startDates.get(0)));
                end_time.setText(timeFormat.format(endDates.get(endDates.size()-1)));

                long diff = endDates.get(endDates.size()-1).getTime() - startDates.get(0).getTime();
                int diffhours = (int) (diff / (60 * 60 * 1000));
                int Minute = (int) diff / (60 * 1000) % 60;
                total_hours.setText(String.format("%02d", diffhours)+":"+String.format("%02d", Minute));
            }
        }
    }

    private void setUpOnClicks(){
        selectAll.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                DisplayCoverList(isChecked);
            }
        });

    }


    private void DisplayCoverList(boolean selectAll){
        ArrayList<User>users =new ArrayList<>();
        users.addAll(Tools.getActiveUser(CrewUDataManager.getInstance().getUsers()));
        User user = CrewUDataManager.getInstance().getUser();

        for (int i = 0; i <users.size() ; i++) {
            User user1 = users.get(i);
            if (user.userId.equalsIgnoreCase(user1.userId)){
                users.remove(i);
                break;
            }
        }

        Schedule_Cover_listAdapter adapter = new Schedule_Cover_listAdapter(getContext(),users,selectAll);
        listView.setAdapter(adapter);
    }
}
