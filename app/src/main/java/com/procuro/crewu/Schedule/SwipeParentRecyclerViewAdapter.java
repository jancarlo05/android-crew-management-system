package com.procuro.crewu.Schedule;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.procuro.apimmdatamanagerlib.Schedule_Business_Site_Plan;
import com.procuro.crewu.CrewUDataManager;
import com.procuro.crewu.Header.HeaderData;
import com.procuro.crewu.R;
import com.procuro.crewu.Tools;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.TimeZone;

public class SwipeParentRecyclerViewAdapter extends RecyclerSwipeAdapter<SwipeParentRecyclerViewAdapter.SimpleViewHolder> {

    private Context mContext;
    private ArrayList<Schedule>schedules;
    private FragmentActivity fragmentActivity;

    SwipeParentRecyclerViewAdapter(Context context, ArrayList<Schedule>schedules, FragmentActivity fragmentActivity) {
        this.mContext = context;
        this.schedules = schedules;
        this.fragmentActivity = fragmentActivity;

    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.sched_parent_layout, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final SimpleViewHolder viewHolder, final int position) {

        final Schedule schedule = schedules.get(position);

        SimpleDateFormat format = new SimpleDateFormat("EEE_MM/dd");
        SimpleDateFormat timeFormat = new SimpleDateFormat("h:mm a");

        timeFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        format.setTimeZone(TimeZone.getTimeZone("UTC"));

        String dateFormat = format.format(schedule.getDate()).replace("_","\n");
        viewHolder.dateTilte.setText(dateFormat);

        ArrayList<Date>startDates = new ArrayList<>();
        ArrayList<Date>endDates = new ArrayList<>();
        ArrayList<Integer>positions = new ArrayList<>();

        long total_start =0, total_end = 0;

        if (schedule.getSchedules()!=null){

            Collections.sort(schedule.getSchedules(), new Comparator<Schedule_Business_Site_Plan>() {
                @Override
                public int compare(Schedule_Business_Site_Plan o1, Schedule_Business_Site_Plan o2) {
                    return o1.startTime.compareTo(o2.startTime);
                }
            });

            for (Schedule_Business_Site_Plan plan : schedule.getSchedules()){
                if (!positions.contains(plan.position)){
                    positions.add(plan.position);
                }
                total_start+= plan.startTime.getTime();
                total_end+= plan.endTime.getTime();

                startDates.add(plan.startTime);
                endDates.add(plan.endTime);
            }
            SwipeRecyclerViewAdapter adapter = new SwipeRecyclerViewAdapter(mContext,schedule.getSchedules(),fragmentActivity);
            viewHolder.recyclerView.setLayoutManager(new GridLayoutManager(mContext,1));
            viewHolder.recyclerView.setAdapter(adapter);
        }

        if (startDates.size()>0){
            Collections.sort(startDates);
            Collections.sort(endDates);
            viewHolder.startTime.setText(timeFormat.format(startDates.get(0)));
            viewHolder.endTime.setText(timeFormat.format(endDates.get(endDates.size()-1)));

            try {
                viewHolder.total_hours.setText(Tools.getDecimalTimeDuration(endDates.get(endDates.size()-1),startDates.get(0)));
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        boolean isNull = false;

        if (positions.size()==0){
            positions.add(-404);
            isNull = true;
        }

        if (positions.size()>2){
            viewHolder.sizeIndicator.setVisibility(View.VISIBLE);
            viewHolder.sizeIndicator.setText(getSizeIndicator(positions.size()));
        }else {
            viewHolder.sizeIndicator.setVisibility(View.INVISIBLE);
        }

        PositionsRecyclerView_Adapter adapter = new PositionsRecyclerView_Adapter(mContext,positions,isNull);
        viewHolder.positionRecyclerView.setLayoutManager(new GridLayoutManager(mContext,2));
        viewHolder.positionRecyclerView.setAdapter(adapter);
//


        final boolean[] open = {false};

        viewHolder.indicator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!open[0]){
                    open[0] = true;
                    viewHolder.container.setVisibility(View.VISIBLE);
                    RotateView(viewHolder.indicator,true);
                }else {
                    open[0] = false;
                    viewHolder.container.setVisibility(View.GONE);
                    RotateView(viewHolder.indicator,false);
                }
            }
        });


        viewHolder.cover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplayCover(schedule);
            }
        });

        viewHolder.swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);
        viewHolder.swipeLayout.addDrag(SwipeLayout.DragEdge.Right, viewHolder.swipeLayout.findViewById(R.id.bottom_wraper));
        viewHolder.swipeLayout.addSwipeListener(new SwipeLayout.SwipeListener() {

            @Override
            public void onStartOpen(SwipeLayout layout) {
                if (open[0]){
                    open[0] = false;
                    viewHolder.container.setVisibility(View.GONE);
                    RotateView(viewHolder.indicator,false);

                    SwipeRecyclerViewAdapter adapter = new SwipeRecyclerViewAdapter(mContext,schedule.getSchedules(),fragmentActivity);
                    viewHolder.recyclerView.setLayoutManager(new GridLayoutManager(mContext,1));
                    viewHolder.recyclerView.setAdapter(adapter);
                }
            }
            @Override
            public void onOpen(SwipeLayout layout) {


            }

            @Override
            public void onStartClose(SwipeLayout layout) {


            }

            @Override
            public void onClose(SwipeLayout layout) {


            }

            @Override
            public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {


            }

            @Override
            public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {

            }
        });
        mItemManger.bindView(viewHolder.itemView, position);
    }

    private void RotateView(View view,boolean active){
        if (active){
            view.setRotation(180);
        }else {
            view.setRotation(0);
        }
    }

    @Override
    public int getItemCount() {
        return schedules.size();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;

    }

    public static class SimpleViewHolder extends RecyclerView.ViewHolder{
        SwipeLayout swipeLayout;
        LinearLayout btm_wrapper;
        TextView callOut,cover,dateTilte,startTime,endTime,total_hours,sizeIndicator;
        LinearLayout container,indicator;
        RecyclerView recyclerView,positionRecyclerView;

        SimpleViewHolder(View itemView) {
            super(itemView);
            swipeLayout = (SwipeLayout) itemView.findViewById(R.id.swipe);
            btm_wrapper = (LinearLayout) itemView.findViewById(R.id.bottom_wraper);
            callOut = itemView.findViewById(R.id.callout);
            cover = itemView.findViewById(R.id.cover);
            container = itemView.findViewById(R.id.last_continer);
            indicator = itemView.findViewById(R.id.indicator);
            dateTilte = itemView.findViewById(R.id.date);
            startTime = itemView.findViewById(R.id.startTime);
            endTime = itemView.findViewById(R.id.endTime);
            total_hours = itemView.findViewById(R.id.total_hours);
            recyclerView = itemView.findViewById(R.id.recyclerView);
            positionRecyclerView = itemView.findViewById(R.id.positionRecyclerView);
            sizeIndicator = itemView.findViewById(R.id.sizeIndicator);

        }
    }

    private void DisplayCover(Schedule sitePlan){
        HeaderData headerData = HeaderData.getInstance();
        headerData.setSched_EmployeeReportEnabled(true);
        headerData.setTitle("Cover Request");
        HeaderData.DisplayHeaderFragment(fragmentActivity);

        CrewUDataManager.getInstance().setSelectedSchedule(sitePlan);

        fragmentActivity.getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_left,R.anim.slide_left_out).replace(R.id.resturant_fragment_container,
                new ScheduleCoverFragment()).commit();
    }


    private String getSizeIndicator(int Size){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("+");
        stringBuilder.append(Size -2);
        return stringBuilder.toString();
    }
}
