package com.procuro.crewu.Schedule;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.procuro.apimmdatamanagerlib.User;
import com.procuro.crewu.R;

import java.util.ArrayList;


public class Schedule_Cover_listAdapter extends BaseAdapter {

    // Declare Variables

    private Context mContext;
    private LayoutInflater inflater;
    private ArrayList<User>arrayList;
    private boolean selectAll;


    public Schedule_Cover_listAdapter(Context context, ArrayList<User>arrayList,boolean SelectAll) {
        mContext = context;
        inflater = LayoutInflater.from(mContext);
        this.arrayList = arrayList;
        this.selectAll = SelectAll;
    }

    public class ViewHolder {
        TextView name;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("ViewHolder")
    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.schdule_cover_list_data, null);
            view.setTag(holder);

            User user = arrayList.get(position);
            TextView name = view.findViewById(R.id.name);

            CheckBox checkBox = view.findViewById(R.id.checkbox);

            if (selectAll){
                checkBox.setChecked(true);
            }else {
                checkBox.setChecked(false);
            }

            name.setText(user.getFullName());


            return view;
    }



}

