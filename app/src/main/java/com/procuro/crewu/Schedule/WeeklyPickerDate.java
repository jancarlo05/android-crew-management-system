package com.procuro.crewu.Schedule;

import java.util.ArrayList;
import java.util.Date;

public class WeeklyPickerDate {

    private String name;
    private ArrayList<Date>weedays;
    private int weeknumber;

    public WeeklyPickerDate() {
    }

    public WeeklyPickerDate(String name, ArrayList<Date> weedays, int weeknumber) {
        this.name = name;
        this.weedays = weedays;
        this.weeknumber = weeknumber;
    }

    public ArrayList<Date> getWeedays() {
        return weedays;
    }

    public void setWeedays(ArrayList<Date> weedays) {
        this.weedays = weedays;
    }

    public int getWeeknumber() {
        return weeknumber;
    }

    public void setWeeknumber(int weeknumber) {
        this.weeknumber = weeknumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
