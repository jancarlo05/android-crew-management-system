package com.procuro.crewu.Schedule;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.daimajia.swipe.SwipeLayout;
import com.procuro.apimmdatamanagerlib.Schedule_Business_Site_Plan;
import com.procuro.apimmdatamanagerlib.User;
import com.procuro.crewu.R;
import com.procuro.crewu.Tools;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.TimeZone;


public class ScheduleParent_listAdapter extends BaseAdapter {

    // Declare Variables

    private Context mContext;
    private LayoutInflater inflater;
    private ArrayList<Schedule>schedules;
    private FragmentActivity fragmentActivity;


    public ScheduleParent_listAdapter(Context context, ArrayList<Schedule>schedules,FragmentActivity fragmentActivity) {
        mContext = context;
        inflater = LayoutInflater.from(mContext);
        this.schedules = schedules;
        this.fragmentActivity = fragmentActivity;
    }

    public class ViewHolder {
        TextView name;
    }

    @Override
    public int getCount() {
        return schedules.size();
    }

    @Override
    public Object getItem(int position) {
        return schedules.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("ViewHolder")
    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        holder = new ViewHolder();
        view = inflater.inflate(R.layout.sched_parent_layout, null);
        view.setTag(holder);

        Schedule schedule = schedules.get(position);
        final LinearLayout container = view.findViewById(R.id.last_continer);
        final LinearLayout indicator = view.findViewById(R.id.indicator);
        TextView dateTilte = view.findViewById(R.id.date);
        TextView startTime = view.findViewById(R.id.startTime);
        TextView endTime = view.findViewById(R.id.endTime);
        TextView total_hours = view.findViewById(R.id.total_hours);
        RecyclerView recyclerView = view.findViewById(R.id.recyclerView);
        RecyclerView positionRecyclerView = view.findViewById(R.id.positionRecyclerView);


        SimpleDateFormat format = new SimpleDateFormat("EEE_MM/dd");
        SimpleDateFormat timeFormat = new SimpleDateFormat("h:mm a");

        timeFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        format.setTimeZone(TimeZone.getTimeZone("UTC"));

        String dateFormat = format.format(schedule.getDate()).replace("_","\n");
        dateTilte.setText(dateFormat);

        ArrayList<Date>startDates = new ArrayList<>();
        ArrayList<Date>endDates = new ArrayList<>();
        ArrayList<Integer>positions = new ArrayList<>();

        long total_start =0, total_end = 0;

        if (schedule.getSchedules()!=null){

            Collections.sort(schedule.getSchedules(), new Comparator<Schedule_Business_Site_Plan>() {
                @Override
                public int compare(Schedule_Business_Site_Plan o1, Schedule_Business_Site_Plan o2) {
                    return o1.startTime.compareTo(o2.startTime);
                }
            });

            for (Schedule_Business_Site_Plan plan : schedule.getSchedules()){
                if (!positions.contains(plan.position)){
                    positions.add(plan.position);
                }
                total_start+= plan.startTime.getTime();
                total_end+= plan.endTime.getTime();

                startDates.add(plan.startTime);
                endDates.add(plan.endTime);
            }
            SwipeRecyclerViewAdapter adapter = new SwipeRecyclerViewAdapter(mContext,schedule.getSchedules(),fragmentActivity);
            recyclerView.setLayoutManager(new GridLayoutManager(mContext,1));
            recyclerView.setAdapter(adapter);
        }

        if (startDates.size()>0){
            Collections.sort(startDates);
            Collections.sort(endDates);
            startTime.setText(timeFormat.format(startDates.get(0)));
            endTime.setText(timeFormat.format(endDates.get(endDates.size()-1)));

            long diff = total_end - total_start;
//            int diffhours = (int) (diff / (60 * 60 * 1000));
//            int diffmin = (int) (diffhours / (60 * 1000));
            try {
                total_hours.setText(Tools.getDecimalTimeDuration(new Date(total_end),new Date(total_start)));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        boolean isNull = false;

        if (positions.size()==0){
            positions.add(-404);
            isNull = true;
        }
        PositionsRecyclerView_Adapter adapter = new PositionsRecyclerView_Adapter(mContext,positions,isNull);
        positionRecyclerView.setLayoutManager(new GridLayoutManager(mContext,3));
        positionRecyclerView.setAdapter(adapter);


//


        final boolean[] open = {false};

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!open[0]){
                    open[0] = true;
                    container.setVisibility(View.VISIBLE);
                    RotateView(indicator,true);
                }else {
                    open[0] = false;
                    container.setVisibility(View.GONE);
                    RotateView(indicator,false);
                }
            }
        });

        indicator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!open[0]){
                    open[0] = true;
                    container.setVisibility(View.VISIBLE);
                    RotateView(indicator,true);
                }else {
                    open[0] = false;
                    container.setVisibility(View.GONE);
                    RotateView(indicator,false);
                }
            }
        });

        return view;
    }

    private void RotateView(View view,boolean active){
        if (active){
            view.setRotation(180);
        }else {
            view.setRotation(0);
        }
    }

}

