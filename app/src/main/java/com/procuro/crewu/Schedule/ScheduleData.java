package com.procuro.crewu.Schedule;

import java.util.ArrayList;
import java.util.Date;

public class ScheduleData {
    public static ScheduleData intance = new ScheduleData();

    private String title;
    private ArrayList<Schedule>schedules ;
    private Date startDate;
    private Date endDate;

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<Schedule> getSchedules() {
        return schedules;
    }

    public void setSchedules(ArrayList<Schedule> schedules) {
        this.schedules = schedules;
    }

    public static ScheduleData getIntance() {
        if (intance!=null){
            return intance;
        }
        return new ScheduleData();
    }

    public static void setIntance(ScheduleData intance) {
        ScheduleData.intance = intance;
    }
}
