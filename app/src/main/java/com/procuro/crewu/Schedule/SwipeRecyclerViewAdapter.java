package com.procuro.crewu.Schedule;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.procuro.apimmdatamanagerlib.AssignmentInfo;
import com.procuro.apimmdatamanagerlib.Schedule_Business_Site_Plan;
import com.procuro.apimmdatamanagerlib.SdrGisSite;
import com.procuro.apimmdatamanagerlib.SdrPointOfInterest;
import com.procuro.apimmdatamanagerlib.SdrStop;
import com.procuro.apimmdatamanagerlib.User;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;
import com.procuro.crewu.CrewUDataManager;
import com.procuro.crewu.Header.HeaderData;
import com.procuro.crewu.R;
import com.procuro.crewu.Report.EmployeeReportData;
import com.procuro.crewu.Report.Report_Employee_Sched_Fragment;
import com.procuro.crewu.Tools;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class SwipeRecyclerViewAdapter extends RecyclerSwipeAdapter<SwipeRecyclerViewAdapter.SimpleViewHolder> {

    private Context mContext;
    private ArrayList<Schedule_Business_Site_Plan>schedules;
    private FragmentActivity fragmentActivity;

    SwipeRecyclerViewAdapter(Context context,ArrayList<Schedule_Business_Site_Plan>schedules,FragmentActivity fragmentActivity) {
        this.mContext = context;
        this.schedules = schedules;
        this.fragmentActivity = fragmentActivity;

    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.swipe_layout, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final SimpleViewHolder viewHolder, final int position) {


        final Schedule_Business_Site_Plan schedule = schedules.get(position);


        viewHolder.position_info.setText(Tools.getPositionString(schedule.position));
        viewHolder.position_info.setTextColor(ContextCompat.getColor(mContext,Tools.getPositionStringColor(schedule.position)));

        GradientDrawable shape =  new GradientDrawable();
        shape.setCornerRadius(Tools.dpToPx(5));
        shape.setColor(Tools.getPositionColor(schedule.position));

        if (schedule.position > 100){
            shape.setStroke(5,Tools.getPositionColor(schedule.position - 100));
        }else {
            shape.setStroke(0,ContextCompat.getColor(mContext,R.color.transparent));
        }
        viewHolder.position_info.setBackground(shape);

        SimpleDateFormat timeFormat = new SimpleDateFormat("h:mm a");
        timeFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        try {
            viewHolder.startTime.setText(timeFormat.format(schedule.startTime));
        }catch (Exception e){
            e.printStackTrace();
        }
        try {
            viewHolder.endTime.setText(timeFormat.format(schedule.endTime));
        }catch (Exception e){
            e.printStackTrace();
        }
//        try {
//            long diff = schedule.endTime.getTime() - schedule.startTime.getTime();
//            viewHolder.total_hours.setText(Tools.getDecimalTimeDuration(schedule.endTime,schedule.startTime));
//        }catch (Exception e){
//            e.printStackTrace();
//        }

        viewHolder.cover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplayCover(schedule);
            }
        });


        viewHolder.swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);
        viewHolder.swipeLayout.addDrag(SwipeLayout.DragEdge.Right, viewHolder.swipeLayout.findViewById(R.id.bottom_wraper));
        viewHolder.swipeLayout.addSwipeListener(new SwipeLayout.SwipeListener() {

            @Override
            public void onStartOpen(SwipeLayout layout) {

            }

            @Override
            public void onOpen(SwipeLayout layout) {


            }

            @Override
            public void onStartClose(SwipeLayout layout) {


            }

            @Override
            public void onClose(SwipeLayout layout) {


            }

            @Override
            public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {


            }

            @Override
            public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {

            }
        });
        mItemManger.bindView(viewHolder.itemView, position);
    }

    @Override
    public int getItemCount() {
        return schedules.size();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;

    }

    public static class SimpleViewHolder extends RecyclerView.ViewHolder{
        SwipeLayout swipeLayout;
        LinearLayout btm_wrapper;
        TextView position_info,callOut,cover,endTime,startTime,total_hours;
        SimpleViewHolder(View itemView) {
            super(itemView);
            swipeLayout = (SwipeLayout) itemView.findViewById(R.id.swipe);
            btm_wrapper = (LinearLayout) itemView.findViewById(R.id.bottom_wraper);
            position_info = itemView.findViewById(R.id.position_info);
            callOut = itemView.findViewById(R.id.callout);
            cover = itemView.findViewById(R.id.cover);
            endTime = itemView.findViewById(R.id.endTime);
            total_hours = itemView.findViewById(R.id.total_hours);
            startTime = itemView.findViewById(R.id.startTime);
        }
    }

    private void DisplayCover(Schedule_Business_Site_Plan sitePlan){
        HeaderData headerData = HeaderData.getInstance();
        headerData.setSched_EmployeeReportEnabled(true);
        headerData.setTitle("Cover Request");
        HeaderData.DisplayHeaderFragment(fragmentActivity);

        CrewUDataManager.getInstance().setSelectedCoverDate(sitePlan);

        fragmentActivity.getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_left,R.anim.slide_left_out).replace(R.id.resturant_fragment_container,
                new ScheduleCoverFragment()).commit();

    }
}
