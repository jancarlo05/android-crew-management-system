package com.procuro.crewu.Reward;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.procuro.apimmdatamanagerlib.Award;
import com.procuro.apimmdatamanagerlib.User;
import com.procuro.crewu.Chat.Chat_Listview_Adapter;
import com.procuro.crewu.CrewUDataManager;
import com.procuro.crewu.Header.UserHeaderFragment;
import com.procuro.crewu.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;


public class RewardFragment extends Fragment {


    ListView listView;
    RadioButton chat,admin,co_worker;
    public TextView rank, reward;
    Button reward_info;

    public RewardFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.reward_fragment, container, false);
        listView = view.findViewById(R.id.listView);
        rank= view.findViewById(R.id.rank);
        reward = view.findViewById(R.id.rewards);
        reward_info = view.findViewById(R.id.reward_info);

        DisplayUserHeader();

        DisplayRewards();

        return view;

    }

    private void DisplayUserHeader(){
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.user_header_fragment_container,
                new UserHeaderFragment()).commit();
    }

    private void DisplayRewards(){
        ArrayList<User>users = CrewUDataManager.getInstance().getUsers();
        ArrayList<User>activeUsers = new ArrayList<>();
        final User currentUser = CrewUDataManager.getInstance().getUser();;

        boolean found = false;
        if (users!=null){
            for (final User user : users){
                if (user.active){
                    activeUsers.add(user);
                    if (currentUser.userId.equalsIgnoreCase(user.userId)){
                        reward_info.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                DisplayRewardPopup(getContext(),reward_info,user);
                            }
                        });
                        found = true;
                    }
                }
            }
        }
        if (!found){
            activeUsers.add(currentUser);
            reward_info.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DisplayRewardPopup(getContext(),reward_info,currentUser);
                }
            });

        }

        Collections.sort(activeUsers, new Comparator<User>() {
            @Override
            public int compare(User o1, User o2) {

                Integer r1 = o1.awardList.size();
                Integer r2 = o2.awardList.size();

                Integer s1 = o1.old_certificationList.size();
                Integer s2 = o2.old_certificationList.size();

                int rCompare = r2.compareTo(r1);
                if (rCompare == 0){
                    return s2.compareTo(s1);
                }

                return  rCompare;
            }
        });

        Reward_Listview_Adapter adapter = new Reward_Listview_Adapter(getContext(),activeUsers);
        listView.setAdapter(adapter);
        DisplayRewardDetails(activeUsers);

    }

    private void DisplayRewardDetails(ArrayList<User> users){
        try {
            User currentUser = CrewUDataManager.getInstance().getUser();
            int reward = 0;
            int ranking = 0;

            for (User user : users){
                ranking++;
                if (currentUser.userId.equalsIgnoreCase(user.userId)){
                    if (user.awardList.size()>0){
                        reward = user.awardList.size();
                    }
                    break;
                }
            }

            this.rank.setText(String.valueOf(ranking));
            this.reward.setText(String.valueOf(reward));
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void DisplayRewardPopup(final Context context, View anchor,User user) {

        final SimpleTooltip tooltip = new SimpleTooltip.Builder(context)
                .anchorView(anchor)
                .gravity(Gravity.BOTTOM)
                .dismissOnOutsideTouch(true)
                .dismissOnInsideTouch(false)
                .showArrow(true)
                .arrowColor(ContextCompat.getColor(context, R.color.white))
                .transparentOverlay(false)
                .overlayOffset(0)
                .overlayWindowBackgroundColor(ContextCompat.getColor(context,R.color.tooltipOverlay))
                .contentView(R.layout.reward_popup)
                .focusable(true)
                .build();
        tooltip.show();

        ListView  listView = tooltip.findViewById(R.id.listView);
        TextView message = tooltip.findViewById(R.id.message);

        ArrayList<Award>awards = user.awardList;
        try {
            if (awards!=null && awards.size()>0){
                Reward_history_listviewAdapter  adapter = new Reward_history_listviewAdapter(getContext(),awards);
                listView.setAdapter(adapter);
                message.setVisibility(View.GONE);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

}
