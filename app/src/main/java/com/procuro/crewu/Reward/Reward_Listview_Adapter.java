package com.procuro.crewu.Reward;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.procuro.apimmdatamanagerlib.User;
import com.procuro.crewu.R;

import java.util.ArrayList;


public class Reward_Listview_Adapter extends BaseAdapter {

    // Declare Variables

    private Context mContext;
    private LayoutInflater inflater;
    private ArrayList<User>arrayList;


    public Reward_Listview_Adapter(Context context, ArrayList<User>arrayList) {
        mContext = context;
        inflater = LayoutInflater.from(mContext);
        this.arrayList = arrayList;
    }

    public class ViewHolder {
        TextView name;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("ViewHolder")
    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.reward_list_data, null);
            view.setTag(holder);

            User user = arrayList.get(position);
            TextView name = view.findViewById(R.id.name);
            TextView rank = view.findViewById(R.id.rank);
            TextView reward = view.findViewById(R.id.reward);
            TextView skills = view.findViewById(R.id.skills);

            int rewardsize = 0;
            int skillsize = 0;

            if (user.certificationList!=null){
                skillsize = user.certificationList.size();
            }

            if (user.awardList!=null){
                rewardsize = user.awardList.size();
            }
            name.setText(user.getFullName());
            rank.setText(String.valueOf(position+1));
            reward.setText(String.valueOf(rewardsize));
            skills.setText(String.valueOf(skillsize));

            return view;
    }



}

