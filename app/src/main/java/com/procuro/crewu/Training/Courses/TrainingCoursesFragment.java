package com.procuro.crewu.Training.Courses;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.procuro.apimmdatamanagerlib.User;
import com.procuro.crewu.CrewUDataManager;
import com.procuro.crewu.R;
import com.procuro.crewu.Training.Curriculum;
import com.procuro.crewu.Training.CurriculumParent;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Objects;


public class TrainingCoursesFragment extends Fragment {

    public static ExpandableListView listView;
    public static TextView message;
    private Button filter;

    public TrainingCoursesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.training_courses_fragment, container, false);
        listView = view.findViewById(R.id.listView);
        filter = view.findViewById(R.id.sort);

        setUpFilter(getContext(),getActivity());
        
        setupOnlclicks();
        return view;

    }

    private void setupOnlclicks(){
        filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplayTrainingFilterPopup();
            }
        });


    }

    public static void setupList(ArrayList<CurriculumParent>curriculumParents, Context context, FragmentActivity activity){
        try {
            listView.setGroupIndicator(null);
            CrewCurriculum_ExpandandableListAdapter adapter = new CrewCurriculum_ExpandandableListAdapter(context,curriculumParents,activity);
            listView.setAdapter(adapter);


            CrewUDataManager crewUDataManager = CrewUDataManager.getInstance();
            if (curriculumParents!=null){
                for (int i = 0; i <adapter.getGroupCount() ; i++) {
                    CurriculumParent parent = curriculumParents.get(i);
                    for (Curriculum curriculum : parent.curriculumArrayList){
                        if (crewUDataManager.getSelectedCurriculum()!=null){
                            if (curriculum.courseName.equalsIgnoreCase(crewUDataManager.getSelectedCurriculum().courseName)){
                                listView.expandGroup(i);
                                break;
                            }
                        }
                    }
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void setUpFilter(Context context,FragmentActivity fragmentActivity){
        CrewUDataManager dataManager = CrewUDataManager.getInstance();

        if (dataManager.getSelectedTrainingFilter()==null){
            dataManager.setSelectedTrainingFilter("Course#");
            setupList(sortedByCourseNumber(),context,fragmentActivity);
        }else {
            String category = dataManager.getSelectedTrainingFilter();
            if (category.equalsIgnoreCase("Course#")){
                setupList(sortedByCourseNumber(),context,fragmentActivity);
            }else {
                setupList(sortedAlphabetically(),context,fragmentActivity);
            }
        }
    }

    public static ArrayList<CurriculumParent> sortedByCourseNumber(){
        ArrayList <CurriculumParent>parents = new ArrayList<>();
        if (CrewUDataManager.getInstance().getCrewCurriculumList()!=null){
            parents.addAll(CrewUDataManager.getInstance().getCrewCurriculumList());
        }
        return parents;
    }

    public static ArrayList<CurriculumParent> sortedAlphabetically(){
        ArrayList <CurriculumParent>parents = new ArrayList<>();
        if (CrewUDataManager.getInstance().getCrewCurriculumList()!=null){
            parents.addAll(CrewUDataManager.getInstance().getCrewCurriculumList());
        }

        Collections.sort(parents, new Comparator<CurriculumParent>() {
            @Override
            public int compare(CurriculumParent o1, CurriculumParent o2) {
                return o1.courseName.compareTo(o2.courseName);
            }
        });

//        for (CurriculumParent parent : parents){
//            if (parent.curriculumArrayList!=null){
//                Collections.sort(parent.curriculumArrayList, new Comparator<Curriculum>() {
//                    @Override
//                    public int compare(Curriculum o1, Curriculum o2) {
//                        return o1.courseName.compareTo(o2.courseName);
//                    }
//                });
//            }
//        }

        return parents;
    }

    private void DisplayTrainingFilterPopup(){

        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = this.getLayoutInflater();
        View view = inflater.inflate(R.layout.training_filter_popup, null);
        dialogBuilder.setView(view);
        final AlertDialog alertDialog = dialogBuilder.create();
        int width = (int)(getResources().getDisplayMetrics().widthPixels*0.90);
        int height = (int)(getResources().getDisplayMetrics().heightPixels*0.60);
        Objects.requireNonNull(alertDialog.getWindow()).setGravity(Gravity.BOTTOM);
        Objects.requireNonNull(alertDialog.getWindow()).setLayout(width, ViewGroup.LayoutParams.WRAP_CONTENT);
        Objects.requireNonNull(alertDialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Objects.requireNonNull(alertDialog.getWindow()).getAttributes().windowAnimations = R.style.slide_down_up;
        alertDialog.setCancelable(false);
        alertDialog.show();

        final ConstraintLayout courseNumber,alphabetically,cancel;
        ImageView course_check,alpha_check;
        courseNumber = view.findViewById(R.id.course_container);
        alphabetically = view.findViewById(R.id.alphabetically_container);
        cancel = view.findViewById(R.id.canvel_container);
        course_check = view.findViewById(R.id.course_check);
        alpha_check = view.findViewById(R.id.alpha_check);


        if (CrewUDataManager.getInstance().getSelectedTrainingFilter().equalsIgnoreCase("Course#")){
            alpha_check.setVisibility(View.INVISIBLE);
            course_check.setVisibility(View.VISIBLE);
        }else {
            alpha_check.setVisibility(View.VISIBLE);
            course_check.setVisibility(View.INVISIBLE);
        }

        courseNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CrewUDataManager.getInstance().setSelectedTrainingFilter("Course#");
                setUpFilter(getContext(),getActivity());
                alertDialog.dismiss();
            }
        });

        alphabetically.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CrewUDataManager.getInstance().setSelectedTrainingFilter("alpha");
                setUpFilter(getContext(),getActivity());
                alertDialog.dismiss();
            }
        });


        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

    }
}
