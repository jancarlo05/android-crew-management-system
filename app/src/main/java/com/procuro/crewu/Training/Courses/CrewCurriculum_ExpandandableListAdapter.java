package com.procuro.crewu.Training.Courses;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;

import com.procuro.crewu.CrewUDataManager;
import com.procuro.crewu.Header.HeaderData;
import com.procuro.crewu.R;
import com.procuro.crewu.Training.Curriculum;
import com.procuro.crewu.Training.CurriculumParent;

import java.util.ArrayList;

public class CrewCurriculum_ExpandandableListAdapter extends BaseExpandableListAdapter {

    private Context context;
    private ArrayList<CurriculumParent> curriculumParents;
    private FragmentActivity fragmentActivity;

    public CrewCurriculum_ExpandandableListAdapter(Context context, ArrayList<CurriculumParent> curriculumParents, FragmentActivity fragmentActivity) {
        this.context = context;
        this.curriculumParents = curriculumParents;
        this.fragmentActivity = fragmentActivity;

    }

    @Override
    public int getGroupCount() {
        return curriculumParents.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return curriculumParents.get(groupPosition).curriculumArrayList.size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return curriculumParents.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return curriculumParents.get(groupPosition).curriculumArrayList.get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        View view = LayoutInflater.from(context).inflate(R.layout.training_curriculum_parent_data,null);
        TextView name = view.findViewById(R.id.name);
        TextView count = view.findViewById(R.id.count);
        ImageView indicator = view.findViewById(R.id.indicator);

        CurriculumParent curriculumParent = curriculumParents.get(groupPosition);

        name.setText(curriculumParent.courseName);
        count.setText("0/"+curriculumParent.curriculumArrayList.size());

        if (isExpanded){
            indicator.setRotation(180);
        }else {
            indicator.setRotation(270);
        }


        return view;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        View view = LayoutInflater.from(context).inflate(R.layout.training_curriculum_child_data,null);

        final Curriculum crewCurriculum = curriculumParents.get(groupPosition).curriculumArrayList.get(childPosition);

        TextView name = view.findViewById(R.id.name);
        TextView time = view.findViewById(R.id.time);
        TextView last_rev = view.findViewById(R.id.rev);
        TextView optional = view.findViewById(R.id.optional);

        ImageView icon = view.findViewById(R.id.icon);
        ImageView video = view.findViewById(R.id.video);
        ImageView quiz = view.findViewById(R.id.quiz);

        name.setText(crewCurriculum.courseName);
        time.setText("Time : "+crewCurriculum.time +" Minute");
        last_rev.setText("Last Rev.: "+crewCurriculum.lastRev);

        if (crewCurriculum.required){
            optional.setVisibility(View.INVISIBLE);
        }else {
            optional.setVisibility(View.VISIBLE);
        }

        if (crewCurriculum.video_Quiz.equalsIgnoreCase("Video")){
            quiz.setVisibility(View.INVISIBLE);
        }else if (crewCurriculum.video_Quiz.equalsIgnoreCase("Quiz")){
            video.setVisibility(View.INVISIBLE);
        }

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplayVideoQuizFragment(crewCurriculum);
            }
        });

        return view;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }


    private void DisplayVideoQuizFragment(Curriculum crewCurriculum){
        CrewUDataManager dataManager = CrewUDataManager.getInstance();
        dataManager.setSelectedCurriculum(crewCurriculum);

        HeaderData headerData = HeaderData.getInstance();
        headerData.setVideoQuizEnabled(true);
        headerData.setTitle(crewCurriculum.courseName);
        HeaderData.DisplayHeaderFragment(fragmentActivity);

        fragmentActivity.getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_left,R.anim.slide_left_out).replace(R.id.resturant_fragment_container,
                new TrainingQuiz_VideoFragment()).commit();

    }
}
