package com.procuro.crewu.Training.Summary;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.fragment.app.Fragment;

import com.procuro.crewu.Header.UserHeaderFragment;
import com.procuro.crewu.R;


public class TrainingSummaryFragment extends Fragment {


    public TrainingSummaryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.training_summary_fragment, container, false);


        DisplayUserHeader();

        return view;

    }

    private void DisplayUserHeader(){
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.user_header_fragment_container,
                new UserHeaderFragment()).commit();
    }

}
