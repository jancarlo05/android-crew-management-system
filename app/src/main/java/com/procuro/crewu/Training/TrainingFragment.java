package com.procuro.crewu.Training;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.fragment.app.Fragment;

import com.procuro.apimmdatamanagerlib.User;
import com.procuro.crewu.CrewUDataManager;
import com.procuro.crewu.Profile.CustomRole;
import com.procuro.crewu.R;
import com.procuro.crewu.Training.Courses.TrainingCoursesFragment;
import com.procuro.crewu.Training.Summary.TrainingSummaryFragment;

import java.util.ArrayList;

import static com.procuro.crewu.Tools.DisableView;


public class TrainingFragment extends Fragment {

    private RadioButton courses,summary;
    private info.hoang8f.android.segmented.SegmentedGroup segmentedGroup ;

    public TrainingFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.traning_fragment, container, false);

        courses = view.findViewById(R.id.courses);
        summary = view.findViewById(R.id.summary);
        segmentedGroup = view.findViewById(R.id.segmented2);

        setupOnclicks();

        getCurrentUserRole(CrewUDataManager.getInstance().getUser());

        return view;

    }

    private void getCurrentUserRole(User currentUser){
        if (currentUser!=null){
            if (currentUser.roles!=null){
                ArrayList<CustomRole>customRoles = CustomRole.getAllCustomRole(currentUser.roles);
                if (customRoles.size()>=1){
                    CustomRole customRole = customRoles.get(0);
                    System.out.println("ROLE : "+customRole.getStatus() + " "+customRole.getName());
                    if (customRole.getStatus()>5){
                        CreateTrainingList();
                    }else {
                        CreateMangerList();
                    }
                }else {
                    CreateTrainingList();
                }
            }
        }
    }

    private void setupOnclicks(){
        segmentedGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (courses.getId() == checkedId){

                    DisplayCourses();

                }else {

                    DisplaySummary();
                }
            }
        });
        courses.setChecked(true);
    }

    private void DisplayCourses() {
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.training_fragment_container,
                new TrainingCoursesFragment()).commit();
    }

    private void DisplaySummary(){
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.training_fragment_container,
                new TrainingSummaryFragment()).commit();
    }

    private void CreateTrainingList(){
        CrewUDataManager crewUDataManager = CrewUDataManager.getInstance();
        if (crewUDataManager.getCrewCurriculumList()==null){
            ArrayList<CurriculumParent>crewCurriculumList = new ArrayList<>();
            ArrayList<Curriculum>curricula =  new ArrayList<>();

            curricula.add(new Curriculum("00006","Welcome to Wendy's","US/Canada","Video/Quiz",true,"20","02/15"));
            curricula.add(new Curriculum("00008","Special Needs Guests","US/Canada","Video/Quiz",true,"5","02/13"));
            curricula.add(new Curriculum("00012","Safety & Security","US/Canada","Video/Quiz",true,"10","02/14"));
            curricula.add(new Curriculum("00014","Preventing Disc. & Harrassment","US/Canada","Video/Quiz",true,"5","05/13"));
            curricula.add(new Curriculum("00017","Guests & Disabilities (Canada Only)","Canada","Video/Quiz",true,"5","05/13"));
            curricula.add(new Curriculum("00018","Employee Appearance","US/Canada","Video/Quiz",false,"5","01/13"));
            curricula.add(new Curriculum("00019","Safety Data Sheet Compliance","US/Canada","Video/Quiz",true,"10","08/13"));
            crewCurriculumList.add(new CurriculumParent("00002","Orientations","","","Yes/No","Minutes","Mo/Yr",curricula));


            curricula =  new ArrayList<>();
            curricula.add(new Curriculum("00106","Food Handling & Handwashing","US/Canada","Video/Quiz",true,"10","05/16"));
            curricula.add(new Curriculum("00108","Dishwashing","US/Canada","Video/Quiz",true,"10","05/16"));
            curricula.add(new Curriculum("00110","Food Safety Quiz - Crew","US/Canada","Quiz",true,"15","01/16"));
            curricula.add(new Curriculum("00114","Food Alergens Awareness","US/Canada","Video/Quiz",true,"15","11/15"));
            curricula.add(new Curriculum("00116","Body Fluid Clean-up","US/Canada","Video/Quiz",true,"10","06/16"));
            crewCurriculumList.add(new CurriculumParent("00100","Food Safety","","","Yes/No","Minutes","Mo/Yr",curricula));

            curricula =  new ArrayList<>();
            curricula.add(new Curriculum("00202","My Wendy's Culture","US/Canada","Video/Quiz",true,"20","10/15"));
            crewCurriculumList.add(new CurriculumParent("00200","Customer Courtesy","","","Yes/No","Minutes","Mo/Yr",curricula));


            curricula =  new ArrayList<>();
            curricula.add(new Curriculum("00322","Frosty & Beverages","US/Canada","Video/Quiz",true,"8","09/16"));
            curricula.add(new Curriculum("00334","Freestyle","US/Canada","Video/Quiz",false,"25","03/12"));
            curricula.add(new Curriculum("00340","Iced Tea","US/Canada","Video/Quiz",true,"8","10/16"));
            curricula.add(new Curriculum("00342","All-Natural Lemonade","US/Canada","Video/Quiz",true,"11","10/16"));
            curricula.add(new Curriculum("00344","Iced Coffee","US/Canada","Video/Quiz",false,"7","10/16"));
            crewCurriculumList.add(new CurriculumParent("00300","Beverages & Frosty","","","Yes/No","Minutes","Mo/Yr",curricula));

            curricula =  new ArrayList<>();
            curricula.add(new Curriculum("00404","Natural-Cut Fries","US/Canada","Video/Quiz",true,"10","07/16"));
            curricula.add(new Curriculum("00406","Crispy Chicken Nuggets","US/Canada","Video/Quiz",true,"10","07/16"));
            curricula.add(new Curriculum("00410","Fry Station Challenge","US/Canada","Video/Quiz",true,"25" ,"09/16"));
            curricula.add(new Curriculum("00414","Next Gen Fry Station","US/Canada","Video/Quiz",false,"10","07/16"));
            crewCurriculumList.add(new CurriculumParent("00400","Fry Station","","","Yes/No","Minutes","Mo/Yr",curricula));


            curricula =  new ArrayList<>();
            curricula.add(new Curriculum("00512","Operating Overview - 500lb Fryer","US/Canada","Video/Quiz",false,"15","07/14"));
            curricula.add(new Curriculum("00513","Operating Overview - 300lb Fryer","US/Canada","Video/Quiz",false,"15","07/14"));
            curricula.add(new Curriculum("00514","Close - 500lb Fryer","US/Canada","Video/Quiz",false,"15" ,"07/14"));
            curricula.add(new Curriculum("00515","Close - 300lb Fryer","US/Canada","Video/Quiz",false,"15","07/14"));
            curricula.add(new Curriculum("00516","Open - 500lb Fryer","US/Canada","Video/Quiz",false,"15","07/14"));
            curricula.add(new Curriculum("00517","Open - 300lb Fryer","US/Canada","Video/Quiz",false,"15","07/14"));
            crewCurriculumList.add(new CurriculumParent("00500","Fryer Overview","","","Yes/No","Minutes","Mo/Yr",curricula));

            curricula =  new ArrayList<>();
            curricula.add(new Curriculum("00602","Sandwich Station Overview","US/Canada","Video/Quiz",true,"5","09/16"));
            curricula.add(new Curriculum("00604","Buns, Wrapping, Warming..","US/Canada","Video/Quiz",true,"10","12/15"));
            curricula.add(new Curriculum("00606","Chicken Sandwiches","US/Canada","Video/Quiz",true,"5" ,"09/16"));
            curricula.add(new Curriculum("00610","Single, Double, Triple","US/Canada","Video/Quiz",true,"5","09/16"));
            curricula.add(new Curriculum("00612","Baconator","US/Canada","Video/Quiz",true,"5","09/16"));
            curricula.add(new Curriculum("00614","Junior and Double Stack","US/Canada","Video/Quiz",true,"5","09/16"));
            curricula.add(new Curriculum("00616","Kid's Meal Sandwich","US/Canada","Video/Quiz",true,"5","09/16"));
            curricula.add(new Curriculum("00680","Chicken Go Wrap","US/Canada","Video/Quiz",true,"5","09/16"));
            curricula.add(new Curriculum("00682","Serving Salads","US/Canada","Video/Quiz",true,"12","04/16"));
            curricula.add(new Curriculum("00684","Serving Backed Potatoes","US/Canada","Video/Quiz",true,"5","06/13"));
            crewCurriculumList.add(new CurriculumParent("00600","Sandwich Station","","","Yes/No","Minutes","Mo/Yr",curricula));



            curricula =  new ArrayList<>();
            curricula.add(new Curriculum("00702","DSG Prep & Clean","US/Canada","Video/Quiz",false,"15","10/14"));
            curricula.add(new Curriculum("00704","DSG Operations","US/Canada","Video/Quiz",false,"20","5/140"));
            curricula.add(new Curriculum("00734","DSG Close","US/Canada","Video/Quiz",false,"10" ,"10/14"));
            curricula.add(new Curriculum("00752","Flat Grill Prep & Setup","US/Canada","Video/Quiz",false,"10","01/13"));
            curricula.add(new Curriculum("00754","Flat Grill Operations","US/Canada","Video/Quiz",false,"20","06/14"));
            curricula.add(new Curriculum("00784","Flat Grill Close","US/Canada","Video/Quiz",false,"10","02/14"));
            crewCurriculumList.add(new CurriculumParent("00700","Grill Operations","","","Yes/No","Minutes","Mo/Yr",curricula));



            curricula =  new ArrayList<>();
            curricula.add(new Curriculum("00805","DR Maintenance & Restrooms","US/Canada","Video/Quiz",true,"12","10/16"));
            crewCurriculumList.add(new CurriculumParent("00800","Dining Room","","","Yes/No","Minutes","Mo/Yr",curricula));


            curricula =  new ArrayList<>();
            curricula.add(new Curriculum("00906","Register Overview","US/Canada","Video/Quiz",true,"15","06/14"));
            curricula.add(new Curriculum("00910","Register - Aloha POS","US/Canada","Video/Quiz",false,"20","06/14"));
            crewCurriculumList.add(new CurriculumParent("00900","Register","","","Yes/No","Minutes","Mo/Yr",curricula));


            curricula =  new ArrayList<>();
            curricula.add(new Curriculum("01152","Bagging","US/Canada","Video/Quiz",true,"20","05/13"));
            curricula.add(new Curriculum("01154","Front Line Coordinator","US/Canada","Video/Quiz",true,"25","11/14"));
            curricula.add(new Curriculum("01156","PUW Coordinator","US/Canada","Video/Quiz",true,"20","07/14"));
            crewCurriculumList.add(new CurriculumParent("01150","Service Coordinator","","","Yes/No","Minutes","Mo/Yr",curricula));


            curricula =  new ArrayList<>();
            curricula.add(new Curriculum("01202","Product Coordination Intro","US/Canada","Video/Quiz",true,"7","08/16"));
            curricula.add(new Curriculum("01204","Product Drops","US/Canada","Video/Quiz",true,"20","08/16"));
            curricula.add(new Curriculum("01206","Product Refills & Cheese Sauce","US/Canada","Video/Quiz",true,"10","08/16"));
            crewCurriculumList.add(new CurriculumParent("01200","Product Coordinator","","","Yes/No","Minutes","Mo/Yr",curricula));


            curricula =  new ArrayList<>();
            curricula.add(new Curriculum("01302","Tomatoes & Onions","US/Canada","Video/Quiz",true,"10","07/13"));
            curricula.add(new Curriculum("01306","Prep Lettuce","US/Canada","Video/Quiz",true,"20","03/14"));
            curricula.add(new Curriculum("01308","Bacon","US/Canada","Video/Quiz",true,"15","04/16"));
            crewCurriculumList.add(new CurriculumParent("01300","Product Prep","","","Yes/No","Minutes","Mo/Yr",curricula));


            curricula =  new ArrayList<>();
            curricula.add(new Curriculum("01402","Salad Ingredient Prep","US/Canada","Video/Quiz",true,"10","04/16"));
            curricula.add(new Curriculum("01404","Salad Assembly","US/Canada","Video/Quiz",true,"15","04/16"));
            crewCurriculumList.add(new CurriculumParent("01400","Salads","","","Yes/No","Minutes","Mo/Yr",curricula));


            curricula =  new ArrayList<>();
            curricula.add(new Curriculum("01402CA","Salad Ingredient Prep","US/Canada","Video/Quiz",false,"12","05/16"));
            curricula.add(new Curriculum("01404","Salad Assembly","US/Canada","Video/Quiz",true,"15","05/16"));
            crewCurriculumList.add(new CurriculumParent("01400 CA","Salads - Canada","","","Yes/No","Minutes","Mo/Yr",curricula));


            curricula =  new ArrayList<>();
            curricula.add(new Curriculum("01602","Service Line Setup","US/Canada","Video/Quiz",true,"10","01/13"));
            curricula.add(new Curriculum("01604","Beverage & Ice Station Setup","US/Canada","Video/Quiz",true,"10","04/14"));
            curricula.add(new Curriculum("01606","Frosty Machine Setup","US/Canada","Video/Quiz",true,"20","07/14"));
            curricula.add(new Curriculum("01608","Toaster Cleaning Prep","US/Canada","Video/Quiz",false,"10","05/14"));
            crewCurriculumList.add(new CurriculumParent("01600","Line Setup Prep","","","Yes/No","Minutes","Mo/Yr",curricula));


            curricula =  new ArrayList<>();
            curricula.add(new Curriculum("01702","DR Floor Cleaning - Post Rush","US/Canada","Video/Quiz",true,"5","04/14"));
            curricula.add(new Curriculum("02302","DR Floor Cleaning - Close","US/Canada","Video/Quiz",false,"10","04/14"));
            crewCurriculumList.add(new CurriculumParent("01700","Floor Maintenance & Close","","","Yes/No","Minutes","Mo/Yr",curricula));


            curricula =  new ArrayList<>();
            curricula.add(new Curriculum("01802","Outside Maintenance","US/Canada","Video/Quiz",true,"15","05/14"));
            crewCurriculumList.add(new CurriculumParent("01800","Opening Maintenance","","","Yes/No","Minutes","Mo/Yr",curricula));


            curricula =  new ArrayList<>();
            curricula.add(new Curriculum("01904","Close Restrooms","US/Canada","Video/Quiz",true,"10","02/14"));
            curricula.add(new Curriculum("01910","Close Condiment, Carpet, Trash","US/Canada","Video/Quiz",true,"20","02/14"));
            crewCurriculumList.add(new CurriculumParent("01900","Close Dining Room","","","Yes/No","Minutes","Mo/Yr",curricula));


            curricula =  new ArrayList<>();
            curricula.add(new Curriculum("02003","Close Frosty, Beverage & Ice Station","US/Canada","Video/Quiz",true,"20","02/15"));
            curricula.add(new Curriculum("02006","Close Oven","US/Canada","Video/Quiz",true,"10","05/14"));
            crewCurriculumList.add(new CurriculumParent("02000","Close Equipment","","","Yes/No","Minutes","Mo/Yr",curricula));


            curricula =  new ArrayList<>();
            curricula.add(new Curriculum("02102","Close Sandwich Station & Buns","US/Canada","Video/Quiz",true,"10","06/14"));
            curricula.add(new Curriculum("02104","Close Fry Station","US/Canada","Video/Quiz",true,"5","05/14"));
            crewCurriculumList.add(new CurriculumParent("02100","Close Production Line","","","Yes/No","Minutes","Mo/Yr",curricula));


            curricula =  new ArrayList<>();
            curricula.add(new Curriculum("02402","Tables, Chairs & Condiments","US/Canada","Video/Quiz",true,"7","04/16"));
            curricula.add(new Curriculum("02404","Personal Protective Equipment","US/Canada","Video/Quiz",true,"8","04/16"));
            curricula.add(new Curriculum("02406","General Deliming","US/Canada","Video/Quiz",true,"10","04/16"));
            curricula.add(new Curriculum("02410","Exhaust Hoods","US/Canada","Video/Quiz",true,"5","04/16"));
            curricula.add(new Curriculum("02412","Stainless Steel, Vents & Returns","US/Canada","Video/Quiz",true,"7","04/16"));
            curricula.add(new Curriculum("02414","Shelving, Cleaning & Racks","US/Canada","Video/Quiz",true,"8","04/16"));
            crewCurriculumList.add(new CurriculumParent("02400","Non-Task Cleaning - Weekly","","","Yes/No","Minutes","Mo/Yr",curricula));


            curricula =  new ArrayList<>();
            curricula.add(new Curriculum("02502","Walk-ins, Bun Freezer & Uprights","US/Canada","Video/Quiz",true,"10","04/16"));
            crewCurriculumList.add(new CurriculumParent("02500","Non-Task Cleaning - Monthly","","","Yes/No","Minutes","Mo/Yr",curricula));


            curricula =  new ArrayList<>();
            curricula.add(new Curriculum("02614","Premium Cod Sandwich","US/Canada","Video/Quiz",false,"15","01/16"));
            curricula.add(new Curriculum("02643","Poutine (Canada Only))","US/Canada","Video/Quiz",false,"5","12/12"));
            curricula.add(new Curriculum("02675","FruiTea Chillers","US/Canada","Video/Quiz",false,"10","03/16"));
            curricula.add(new Curriculum("02681","New Grilled Chicken","US/Canada","Video/Quiz",false,"10","05/16"));
            curricula.add(new Curriculum("02683","Taco Salad (US Only)","US/Canada","Video/Quiz",false,"7","09/16"));
            curricula.add(new Curriculum("02684","Spicy Sriracha Chicken","US/Canada","Video/Quiz",false,"13","10/16"));
            crewCurriculumList.add(new CurriculumParent("02600","New Products","","","Yes/No","Minutes","Mo/Yr",curricula));


            curricula =  new ArrayList<>();
            curricula.add(new Curriculum("02655","Gold Chili - Meat Procedures","US/Canada","Video/Quiz",true,"15","08/14"));
            curricula.add(new Curriculum("02656","Gold Chili - Prep & Serve","US/Canada","Video/Quiz",true,"15","08/14"));
            crewCurriculumList.add(new CurriculumParent("02650","Chili","","","Yes/No","Minutes","Mo/Yr",curricula));

            crewUDataManager.setCrewCurriculumList(crewCurriculumList);
        }
    }

    private void CreateMangerList(){
        CrewUDataManager crewUDataManager = CrewUDataManager.getInstance();
        if (crewUDataManager.getCrewCurriculumList()==null){
            ArrayList<CurriculumParent>crewCurriculumList = new ArrayList<>();
            ArrayList<Curriculum>curricula =  new ArrayList<>();

            curricula.add(new Curriculum("00112","Food Safety Quiz - Manager","Both","US/Canada","Video/Quiz",true,"20","01/16"));
            curricula.add(new Curriculum("00114","Food Alergens Awareness","Both","US/Canada","Video/Quiz",true,"15","11/15"));
            crewCurriculumList.add(new CurriculumParent("00100","Food Safety","","","","Yes/No","Minutes","Mo/Yr",curricula));

            curricula =  new ArrayList<>();
            curricula.add(new Curriculum("05016","Preventing Harrassment","Both","US/Canada","Video/Quiz",true,"20","05/13"));
            curricula.add(new Curriculum("05040","Information Security Awareness","Company","US/Canada","Video/Quiz",true,"5","05/15"));
            curricula.add(new Curriculum("05042","Gift Card Registration","Both","US/Canada","Video/Quiz",false,"15","05/12"));
            curricula.add(new Curriculum("05052","Building a Cleaning Culture","Both","US/Canada","Video/Quiz",true,"20","08/10"));
            curricula.add(new Curriculum("05054","Employee Appearance","Both","US/Canada","Video/Quiz",false,"5","01/13"));
            curricula.add(new Curriculum("05110","Building a \"My Wendy's\" Culture","Both","US/Canada","Video/Quiz",true,"25","09/10"));
            crewCurriculumList.add(new CurriculumParent("05000","Orientations","","","","Yes/No","Minutes","Mo/Yr",curricula));

            curricula =  new ArrayList<>();
            curricula.add(new Curriculum("05061","VOC Orientation","Both","US/Canada","Video/Quiz",true,"15","08/16"));
            curricula.add(new Curriculum("05063","VOC Restaurant Level Dashboard","Both","US/Canada","Video/Quiz",true,"20","08/16"));
            curricula.add(new Curriculum("05065","VOC Operator Dashboard","Both","US/Canada","Video/Quiz",true,"5","08/16"));
            curricula.add(new Curriculum("05067","VOC Alert Summary & Resolution","Both","US/Canada","Video/Quiz",true,"25","08/16"));
            crewCurriculumList.add(new CurriculumParent("05060","Voice of Customer","","","","Yes/No","Minutes","Mo/Yr",curricula));

            curricula =  new ArrayList<>();
            curricula.add(new Curriculum("05102","Training the \"Wendy's Way\"","Both","US/Canada","Video/Quiz",true,"20","10/11"));
            curricula.add(new Curriculum("05104","Exeecuting New Product Implementation","Both","US/Canada","Video/Quiz",true,"20","06/13"));
            crewCurriculumList.add(new CurriculumParent("05100","Training","","","","Yes/No","Minutes","Mo/Yr",curricula));

            curricula =  new ArrayList<>();
            curricula.add(new Curriculum("05132","Talk To Me (TTM)","Both","US/Canada","Video/Quiz",true,"45","01/09"));
            curricula.add(new Curriculum("05134","Conducting Huddles","Both","US/Canada","Video/Quiz",true,"10","12/14"));
                    crewCurriculumList.add(new CurriculumParent("05130","Coaching","","","","Yes/No","Minutes","Mo/Yr",curricula));


            curricula =  new ArrayList<>();
            curricula.add(new Curriculum("05151","Completing the Daily Plan","Both","US/Canada","Video/Quiz",true,"15","08/10"));
            curricula.add(new Curriculum("05152","Sequential Positioning","Both","US/Canada","Video/Quiz",true,"30","08/10"));
            crewCurriculumList.add(new CurriculumParent("05150","Positioning","","","","Yes/No","Minutes","Mo/Yr",curricula));

            curricula =  new ArrayList<>();
            curricula.add(new Curriculum("05170","Operations Walk-thru","Both","US/Canada","Video/Quiz",true,"45","02/09"));
            crewCurriculumList.add(new CurriculumParent("05170","Walkthroughs","","","","Yes/No","Minutes","Mo/Yr",curricula));

            curricula =  new ArrayList<>();
            curricula.add(new Curriculum("05182","POPDR","Both","US/Canada","Video/Quiz",true,"10","08/14"));
            crewCurriculumList.add(new CurriculumParent("05180","POPDR","","","","Yes/No","Minutes","Mo/Yr",curricula));

            curricula =  new ArrayList<>();
            curricula.add(new Curriculum("05112","Achieving A-Level Operations","Both","US/Canada","Video/Quiz",true,"35","11/11"));
            curricula.add(new Curriculum("05114","Diagnosing HME Timers","Company","US/Canada","Video/Quiz",true,"20","12/11"));
            curricula.add(new Curriculum("05126","Zoom Timers Overview","Company","US/Canada","Video/Quiz",true,"10","08/16"));
            curricula.add(new Curriculum("05128","Zoom Timers Reports","Company","US/Canada","Video/Quiz",true,"15","08/16"));
            curricula.add(new Curriculum("05192","Operations Knowledge Test","Both","US/Canada","Video/Quiz",true,"30","01/16"));
            curricula.add(new Curriculum("05193","Mini Ops Test - Fry Station","Both","US/Canada","Video/Quiz",false,"15","10/11"));
            curricula.add(new Curriculum("05194","Mini Ops Test - Sandwich Station","Both","US/Canada","Video/Quiz",false,"15","04/12"));
            crewCurriculumList.add(new CurriculumParent("05190","Operations Leader","","","","Yes/No","Minutes","Mo/Yr",curricula));

            curricula =  new ArrayList<>();
            curricula.add(new Curriculum("05302","Labor Management Overview","Both","US/Canada","Video/Quiz",true,"15","08/09"));
            curricula.add(new Curriculum("05304","Scheduling I - Building a Schedule","Both","US/Canada","Video/Quiz",true,"40","12/09"));
            curricula.add(new Curriculum("05306","Scheduling II - Copying a Schedule","Both","US/Canada","Video/Quiz",true,"15","12/09"));
            curricula.add(new Curriculum("05308","Scheduling  and Labor Analysis","Both","US/Canada","Video/Quiz",true,"15","12/09"));
            curricula.add(new Curriculum("05310","The Schedule and Labor Challenge","Both","US/Canada","Video/Quiz",true,"15","02/10"));
            curricula.add(new Curriculum("05312","Aloha Back Office","Company","US/Canada","Video/Quiz",false,"20","05/13"));
            crewCurriculumList.add(new CurriculumParent("05300","Labor Management","","","","Yes/No","Minutes","Mo/Yr",curricula));

            curricula =  new ArrayList<>();
            curricula.add(new Curriculum("05402","Forecasting Basics","Both","US/Canada","Video/Quiz",true,"15","04/15"));
            curricula.add(new Curriculum("05404","Food Management 101","Both","US/Canada","Video/Quiz",true,"20","10/10"));
            curricula.add(new Curriculum("05406","Back Office - Prep & Bin Charts","Both","US/Canada","Video/Quiz",true,"10","08/12"));
            curricula.add(new Curriculum("05408","Back Office - Waste","Company","US/Canada","Video/Quiz",true,"10","08/12"));
            curricula.add(new Curriculum("05410","Back Office - Daily Targets","Company","US/Canada","Video/Quiz",true,"15","09/12"));
            curricula.add(new Curriculum("05412","Back Office - Receiving & Credits","Company","US/Canada","Video/Quiz",true,"15","11/12"));
            curricula.add(new Curriculum("05414","Back Office - Ordering & Transfers","Company","US/Canada","Video/Quiz",true,"15","01/13"));
            curricula.add(new Curriculum("05416","Back Office - Inventory","Company","US/Canada","Video/Quiz",true,"10","03/13"));
            curricula.add(new Curriculum("05418","Back Office - Inventory Reports","Company","US/Canada","Video/Quiz",true,"10","03/13"));
            crewCurriculumList.add(new CurriculumParent("05400","Food Management - Cost Control","","","","Yes/No","Minutes","Mo/Yr",curricula));

            curricula =  new ArrayList<>();
            curricula.add(new Curriculum("05602","Pest Management","Both","US/Canada","Video/Quiz",true,"15","02/15"));
            crewCurriculumList.add(new CurriculumParent("05600","Pest Management","","","","Yes/No","Minutes","Mo/Yr",curricula));

            curricula =  new ArrayList<>();
            curricula.add(new Curriculum("05704","CEI Overview","Both","US/Canada","Video/Quiz",true,"10","12/14"));
            curricula.add(new Curriculum("05706","CEI Operational Evaluation","Both","US/Canada","Video/Quiz",true,"10","12/14"));
            curricula.add(new Curriculum("05708","CEI People Evaluation","Both","US/Canada","Video/Quiz",true,"10","12/14"));
            curricula.add(new Curriculum("05710","CEI Facility Evaluation","Both","US/Canada","Video/Quiz",true,"5","12/14"));
            crewCurriculumList.add(new CurriculumParent("05700","CEI","","","","Yes/No","Minutes","Mo/Yr",curricula));

            curricula =  new ArrayList<>();
            curricula.add(new Curriculum("06002","Preventive Maintenance Quiz","Both","US/Canada","Video/Quiz",false,"45","05/09"));
            curricula.add(new Curriculum("06050","My TechConnect","Both","US/Canada","Video/Quiz",false,"30","04/13"));
            crewCurriculumList.add(new CurriculumParent("06000","Preventive Maintenance","","","","Yes/No","Minutes","Mo/Yr",curricula));

            curricula =  new ArrayList<>();
            curricula.add(new Curriculum("06102","Hiring Management System (HMS)","Both","US/Canada","Video/Quiz",true,"15","02/12"));
            crewCurriculumList.add(new CurriculumParent("06100","Talent Management","","","","Yes/No","Minutes","Mo/Yr",curricula));

            curricula =  new ArrayList<>();
            curricula.add(new Curriculum("08002","Store Business Review Process","Both","US/Canada","Video/Quiz",false,"15","04/09"));
            curricula.add(new Curriculum("08050","Store Business Measures for Franchisees","Both","US/Canada","Video/Quiz",false,"20","03/10"));
            crewCurriculumList.add(new CurriculumParent("08000","Store Business Measures","","","","Yes/No","Minutes","Mo/Yr",curricula));

            curricula =  new ArrayList<>();
            curricula.add(new Curriculum("08102","Register Operator Overview","Both","US/Canada","Video/Quiz",true,"5","03/11"));
            curricula.add(new Curriculum("08110","Operations Knowledge Test","Both","US/Canada","Video/Quiz",true,"30","01/15"));
            curricula.add(new Curriculum("08120","Completion Acknowledgement - Phase I","Both","US/Canada","Video/Quiz",true,"5","03/11"));
            crewCurriculumList.add(new CurriculumParent("08100","Advanced Training - Phase I","","","","Yes/No","Minutes","Mo/Yr",curricula));


            curricula =  new ArrayList<>();
            curricula.add(new Curriculum("08210","Safety & Security Policy","Both","US/Canada","Video/Quiz",true,"10","03/11"));
            curricula.add(new Curriculum("08212","Cash Manager Policy","Both","US/Canada","Video/Quiz",true,"10","03/11"));
            curricula.add(new Curriculum("08220","Service MBS Acknowledgement","Both","US/Canada","Video/Quiz",true,"5","03/11"));
            curricula.add(new Curriculum("08230","Completion Acknowledgement - Phase II","Both","US/Canada","Video/Quiz",true,"5","03/11"));
            crewCurriculumList.add(new CurriculumParent("08200","Advanced Training - Phase II","","","","Yes/No","Minutes","Mo/Yr",curricula));

            curricula =  new ArrayList<>();
            curricula.add(new Curriculum("08402","TTT - Overview","Both","US/Canada","Video/Quiz",false,"15","09/11"));
            curricula.add(new Curriculum("08404","TTT - Standards, Skills & Best Practices","Both","US/Canada","Video/Quiz",false,"30","09/11"));
            curricula.add(new Curriculum("08406","TTT - Tools & Resources","Both","US/Canada","Video/Quiz",false,"30","09/11"));
            curricula.add(new Curriculum("08408","TTT - Phase III Transition ","Both","US/Canada","Video/Quiz",false,"10","09/11"));
            crewCurriculumList.add(new CurriculumParent("08400","Executing Training - Phase III","","","","Yes/No","Minutes","Mo/Yr",curricula));

            curricula =  new ArrayList<>();
            curricula.add(new Curriculum("08502","EMRD Basics","Both","US/Canada","Video/Quiz",false,"5","09/13"));
            curricula.add(new Curriculum("08504","EMRD Essentials","Both","US/Canada","Video/Quiz",false,"20","09/13"));
            crewCurriculumList.add(new CurriculumParent("08500","Restaurant Reporting Systems","","","","Yes/No","Minutes","Mo/Yr",curricula));


            crewUDataManager.setCrewCurriculumList(crewCurriculumList);
        }
    }
}


