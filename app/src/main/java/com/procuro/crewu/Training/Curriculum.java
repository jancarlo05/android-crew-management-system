package com.procuro.crewu.Training;

public class Curriculum {
    public String courseNumber;
    public String courseName;
    public String corp_Franchise;
    public String us_Canada;
    public String video_Quiz;
    public boolean required;
    public String time;
    public String lastRev;

    public Curriculum(String courseNumber, String courseName, String us_Canada, String video_Quiz, boolean required, String time, String lastRev) {
        this.courseNumber = courseNumber;
        this.courseName = courseName;
        this.us_Canada = us_Canada;
        this.video_Quiz = video_Quiz;
        this.required = required;
        this.time = time;
        this.lastRev = lastRev;
    }

    public Curriculum(String courseNumber, String courseName, String corp_Franchise, String us_Canada, String video_Quiz, boolean required, String time, String lastRev) {
        this.courseNumber = courseNumber;
        this.courseName = courseName;
        this.corp_Franchise = corp_Franchise;
        this.us_Canada = us_Canada;
        this.video_Quiz = video_Quiz;
        this.required = required;
        this.time = time;
        this.lastRev = lastRev;
    }
}
