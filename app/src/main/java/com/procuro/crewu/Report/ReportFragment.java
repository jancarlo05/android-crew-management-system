package com.procuro.crewu.Report;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.fragment.app.Fragment;

import com.procuro.crewu.CrewUDataManager;
import com.procuro.crewu.Header.HeaderData;
import com.procuro.crewu.R;
import com.procuro.crewu.Tools;


public class ReportFragment extends Fragment {

    public static int Employee_Schedule_Report = 0, Employee_Schedule_Summary = 1;
    private info.hoang8f.android.segmented.SegmentedGroup infogroup;
    private RadioButton performance_report,schedule_report,health_report;


    public ReportFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.report_fragment, container, false);
        performance_report = view.findViewById(R.id.performance_report);
        schedule_report = view.findViewById(R.id.schedule_report);
        infogroup = view.findViewById(R.id.segmented2);
        health_report = view.findViewById(R.id.health_report);

        setupOnlicks();

        return view;

    }

    private void setupOnlicks(){
        infogroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (schedule_report.getId()== checkedId){
                    DisplayScheduleReport();
                }
                else {
                    DisplayPerformanceReport();
                }
            }
        });
        schedule_report.setChecked(true);
    }
    private void DisplayScheduleReport(){
        CrewUDataManager data = CrewUDataManager.getInstance();
        EmployeeReportData employeeReportData = new EmployeeReportData();
        employeeReportData.setReportType(Employee_Schedule_Report);
        employeeReportData.setPositionForm(null);
        employeeReportData.setUser(data.getUser());
        employeeReportData.setUsers(data.getUsers());
        employeeReportData.setSite(Tools.getSelectedSite());
        employeeReportData.setSPID(data.getSPID());
        EmployeeReportData.setInstance(employeeReportData);

        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.report_fragment_container,
                new Report_Employee_Sched_Fragment()).commit();
    }

    private void DisplayPerformanceReport(){
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.report_fragment_container,
                new Report_Employee_Performance_Fragment()).commit();
    }

    private void DisplayHeathCheck(){
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.report_fragment_container,
                new Report_Employee_Performance_Fragment()).commit();
    }
}
