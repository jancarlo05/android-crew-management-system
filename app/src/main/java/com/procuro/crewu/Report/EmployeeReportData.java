package com.procuro.crewu.Report;


import com.procuro.apimmdatamanagerlib.SMSPositioning;
import com.procuro.apimmdatamanagerlib.Schedule_Business_Site_Plan;
import com.procuro.apimmdatamanagerlib.Site;
import com.procuro.apimmdatamanagerlib.User;

import java.util.ArrayList;

public class EmployeeReportData {

    public static int Employee_Schedule_Report = 0;
    public static int Employee_Schedule_Summary = 1;

    private String title;
    private String SPID;
    private String name;
    private String template;

    public static EmployeeReportData instance = new EmployeeReportData();
    private ArrayList<User>users ;
    private SMSPositioning PositionForm;
    private Site site;
    private User user;

    private int reportType;

    private ArrayList<Schedule_Business_Site_Plan>schedules;


    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
        if (user!=null){
            setName(user.getFullName());
        }
    }

    public ArrayList<Schedule_Business_Site_Plan> getSchedules() {
        return schedules;
    }

    public void setSchedules(ArrayList<Schedule_Business_Site_Plan> schedules) {
        this.schedules = schedules;
    }

    public Site getSite() {
        return site;
    }

    public void setSite(Site site) {
        this.site = site;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSPID() {
        return SPID;
    }

    public void setSPID(String SPID) {
        this.SPID = SPID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public int getReportType() {
        return reportType;
    }

    public void setReportType(int reportType) {
        this.reportType = reportType;

        if (reportType == Employee_Schedule_Summary){
            this.setTemplate("employeeSchedule");
            this.setTitle("Employee Schedule Report");
        }else {
            this.setTemplate("employeeScheduleSummary");
            this.setTitle("Employee Schedule Summary");
        }
    }

    public SMSPositioning getPositionForm() {
        return PositionForm;
    }

    public void setPositionForm(SMSPositioning positionForm) {
        PositionForm = positionForm;
    }

    public ArrayList<User> getUsers() {
        return users;
    }

    public void setUsers(ArrayList<User> users) {
        this.users = users;
    }

    public static EmployeeReportData getInstance() {
        if (instance == null){
            instance = new EmployeeReportData();
        }
        return instance;
    }

    public static void setInstance(EmployeeReportData instance) {
        EmployeeReportData.instance = instance;
    }




}
