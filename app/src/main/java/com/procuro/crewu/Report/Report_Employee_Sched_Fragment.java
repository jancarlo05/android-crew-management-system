package com.procuro.crewu.Report;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.procuro.apimmdatamanagerlib.OnCompleteListeners;
import com.procuro.apimmdatamanagerlib.SMSPositioning;
import com.procuro.apimmdatamanagerlib.SMSPositioningEmployeeScheduleAssignment;
import com.procuro.apimmdatamanagerlib.SMSPositioningEmployeeScheduleAssignmentPositions;
import com.procuro.apimmdatamanagerlib.Schedule_Business_Site_Plan;
import com.procuro.apimmdatamanagerlib.User;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;
import com.procuro.crewu.CrewUDataManager;
import com.procuro.crewu.R;
import com.procuro.crewu.Tools;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.TimeZone;

import static com.procuro.crewu.Tools.getDecimalTimeDuration;
import static com.procuro.crewu.Tools.getDecimalTimeDuration2;


public class Report_Employee_Sched_Fragment extends Fragment {

    public static int Employee_Schedule_Report = 0;
    public static int Employee_Schedule_Summary = 1;

    public static ProgressBar progressBar;
    public WebView webView;
    public static TextView date , wait,name;

    com.procuro.apimmdatamanagerlib.aPimmDataManager dataManager = aPimmDataManager.getInstance();

    public Report_Employee_Sched_Fragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.report_employee_sched_fragment, container, false);


        webView = view.findViewById(R.id.webview);
        progressBar = view.findViewById(R.id.progressBar2);
        wait =view.findViewById(R.id.wait);

        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setDomStorageEnabled(true);
        settings.setSupportZoom(true);
        settings.supportZoom();
        settings.setBuiltInZoomControls(true);

        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.addJavascriptInterface(new JavaScriptInterface(getContext(),webView),"AndroidApp");
        webView.setWebViewClient(new mywebviewclient());
        webView.setWebChromeClient(new Webcromeclient());

        if (EmployeeReportData.getInstance().getSchedules()==null){
            DownloadSChedules();
        }else {
            webView.loadUrl("file:///android_asset/EmployeeScheduleReport/wendys.min.html");
        }
        return view;

    }
    private class Webcromeclient extends WebChromeClient {
        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            super.onProgressChanged(view, newProgress);
            webView.setVisibility(View.INVISIBLE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                progressBar.setProgress(newProgress, true);
            }else {
                progressBar.setProgressTintList(ColorStateList.valueOf(Color.parseColor("#F68725")));
                progressBar.setProgress(newProgress);
            }
            if(newProgress == 100) {
                webView.setVisibility(View.VISIBLE);
                wait.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);

            }
        }
    }

    private class mywebviewclient extends WebViewClient {
        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            setUpEmployeeReport();
        }
        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            super.onReceivedError(view, request, error);
            System.out.println("ERROR"+error.getDescription());
            System.out.println("ERROR"+error.toString());
        }
    }
    private void setUpEmployeeReport(){
        EmployeeReportData reportData  = EmployeeReportData.getInstance();

        if (reportData.getReportType() == Employee_Schedule_Report){
            Generate_Employee_Schedule_Report(reportData);
        }else {
            Generate_Employee_Schedule_Summary_Report(reportData);
        }
    }

    private void Generate_Employee_Schedule_Summary_Report(EmployeeReportData reportData){
        String params = Create_Employee_Schedule_Summary_Params(reportData);
        StringBuilder sbldr = new StringBuilder();
        sbldr.append("loadPackage(");
        sbldr.append(params);
        sbldr.append(")");
        System.out.println(sbldr);
        webView.evaluateJavascript(sbldr.toString(), null);
    }

    private String Create_Employee_Schedule_Summary_Params(EmployeeReportData reportData) {
        String param = "";
        try {
            JSONObject dataJObj = new JSONObject();
            JSONObject header = getHeaderDays(reportData);

            dataJObj.put("headerDays", header.getJSONArray("headerDays"));
            dataJObj.put("storeName", reportData.getSite().sitename);
            dataJObj.put("spid", reportData.getSPID());
            dataJObj.put("title", reportData.getTitle());
            dataJObj.put("template", reportData.getTemplate());
            dataJObj.put("date", header.getString("date"));
            dataJObj.put("employees", Create_Employee_Schedule_Summary_Report_Employee_Items(reportData));

            param = dataJObj.toString();

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return param;
    }

    private JSONArray Create_Employee_Schedule_Summary_Report_Employee_Items(EmployeeReportData reportData){

        JSONArray jsonArray = new JSONArray();
        ArrayList<User> users = Tools.getActiveUser(reportData.getUsers());

        Collections.sort(users, new Comparator<User>() {
            @Override
            public int compare(User o1, User o2) {
                return o1.firstName.compareTo(o2.firstName);
            }
        });

        Calendar selectedDate = Tools.getCurrentDateOfSite();
        if (selectedDate.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY){
            selectedDate.add(Calendar.WEEK_OF_YEAR,-1);
        }

        try {

            ArrayList<Date>Weekdays = getWeekDates(selectedDate);

            for (User user : users){

                try {
                    JSONObject EmployeeObject = new JSONObject();

                    JSONArray schedules = new JSONArray();
                    for (Date date : Weekdays){
                        schedules.put(getEmployeeSched(date,reportData,user));
                    }

                    EmployeeObject.put("name",user.getFullName());
                    EmployeeObject.put("userId",user.userId);
                    EmployeeObject.put("schedules",schedules);

                    jsonArray.put(EmployeeObject);

                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        }catch (Exception e){
            e.printStackTrace();
        }

        return jsonArray;
    }

    private void Generate_Employee_Schedule_Report(EmployeeReportData reportData){
        String params = Create_Employee_Schedule_Report_Params(reportData);
        StringBuilder sbldr = new StringBuilder();
        sbldr.append("loadPackage(");
        sbldr.append(params);
        sbldr.append(")");
        System.out.println(sbldr);
        webView.evaluateJavascript(sbldr.toString(), null);
    }

    private String Create_Employee_Schedule_Report_Params(EmployeeReportData reportData) {
        String param = "";
        try {
            JSONObject dataJObj = new JSONObject();
            dataJObj.put("storeName", reportData.getName());
            dataJObj.put("spid", reportData.getSPID());
            dataJObj.put("title", reportData.getTitle());
            dataJObj.put("template", reportData.getTemplate());
            dataJObj.put("date", getWeekDatesSummary(reportData));
            dataJObj.put("schedules", Create_Employee_Schedule_Report_Schedules(reportData));
            param = dataJObj.toString();

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return param;
    }

    private JSONObject Create_Employee_Schedule_Report_Schedules_Item(EmployeeReportData reportData,Date selectedDate){
        JSONObject jsonObject = new JSONObject();
        try {
            TimeZone timeZone = TimeZone.getTimeZone("UTC");
            SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yy");
            SimpleDateFormat weekFormat = new SimpleDateFormat("w");
            SimpleDateFormat dayformat = new SimpleDateFormat("EEEE");
            SimpleDateFormat dateTitleFormat = new SimpleDateFormat(" M/dd/yy");

            dayformat.setTimeZone(timeZone);
            dateFormat.setTimeZone(timeZone);
            weekFormat.setTimeZone(timeZone);
            dateTitleFormat.setTimeZone(timeZone);

            JSONArray scheduleHeader = new JSONArray();
            JSONArray schedules = new JSONArray();

            Calendar now = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
            now.setTime(selectedDate);
            ArrayList<Date>Weekdays = getWeekDates(now);

            long OverAllStart = 0;
            long OverAllEND = 0;

            for (Date date : Weekdays){

                ArrayList<Date>startDates = new ArrayList<>();
                ArrayList<Date>endDates = new ArrayList<>();

                JSONObject headerItem = new JSONObject();
                headerItem.put("name",dayformat.format(date));
                headerItem.put("date",dateFormat.format(date));
                scheduleHeader.put(headerItem);

                schedules.put(getEmployeeSchedWithWeeklyTotal(date,reportData,reportData.getUser(),startDates,endDates));


                if (startDates.size()>0){
                  OverAllStart += startDates.get(0).getTime();
                  OverAllEND += endDates.get(endDates.size()-1).getTime();
                }
            }

            Date start = Weekdays.get(0);
            Date middle  = Weekdays.get(Weekdays.size()/2);
            Date end  = Weekdays.get(Weekdays.size()-1);

            StringBuilder title = new StringBuilder();
            title.append("Week ");
            title.append(weekFormat.format(middle));
            title.append(" (");
            title.append(dateTitleFormat.format(start));
            title.append(" - ");
            title.append(dateTitleFormat.format(end));
            title.append(")");


            jsonObject.put("title",title.toString());
            jsonObject.put("headerDays",scheduleHeader);
            jsonObject.put("schedules",schedules);
            jsonObject.put("weeklyTotalHous",getDecimalTimeDuration2(new Date(OverAllEND),new Date(OverAllStart)));

        }catch (Exception e){
            e.printStackTrace();
        }

        return jsonObject;
    }

    private JSONObject getEmployeeSchedWithWeeklyTotal(Date selectedDate, EmployeeReportData reportData, User user,ArrayList<Date>startDates,ArrayList<Date>endDates){
        JSONObject jsonObject = new JSONObject();
        SimpleDateFormat timeFormat = new SimpleDateFormat("h:mm a");
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE MMM/dd/yy");

        TimeZone timeZone = TimeZone.getTimeZone("UTC");
        dateFormat.setTimeZone(timeZone);
        timeFormat.setTimeZone(timeZone);

        if (reportData.getSchedules()!=null){
            try {
                for (Schedule_Business_Site_Plan sched : reportData.getSchedules()){
                    if (user.userId.equalsIgnoreCase(sched.userID)) {
                        if (dateFormat.format(sched.date).equalsIgnoreCase(dateFormat.format(selectedDate))){
                            startDates.add(sched.startTime);
                            endDates.add(sched.endTime);
                        }
                    }
                }

                Collections.sort(startDates);
                Collections.sort(endDates);

                if (startDates.size()>0){
                    jsonObject.put("timeIn",timeFormat.format(startDates.get(0)));
                }else {
                    jsonObject.put("timeIn","--");
                }

                if (endDates.size()>0){
                    jsonObject.put("timeOut",timeFormat.format(endDates.get(endDates.size()-1)));
                }else {
                    jsonObject.put("timeOut","--");
                }

            }catch (Exception e){
                e.printStackTrace();
            }
        }

        return jsonObject;
    }


    private JSONObject getEmployeeSched(Date selectedDate, EmployeeReportData reportData, User user){
        JSONObject jsonObject = new JSONObject();
        SimpleDateFormat timeFormat = new SimpleDateFormat("h:mm a");
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE MMM/dd/yy");

        TimeZone timeZone = TimeZone.getTimeZone("UTC");
        dateFormat.setTimeZone(timeZone);
        timeFormat.setTimeZone(timeZone);

        ArrayList<Date>startDates = new ArrayList<>();
        ArrayList<Date>endDates = new ArrayList<>();

        if (reportData.getSchedules()!=null){
            try {
                for (Schedule_Business_Site_Plan sched : reportData.getSchedules()){
                    if (user.userId.equalsIgnoreCase(sched.userID)) {
                        if (dateFormat.format(sched.date).equalsIgnoreCase(dateFormat.format(selectedDate))){
                            startDates.add(sched.startTime);
                            endDates.add(sched.endTime);
                        }
                    }
                }
                Collections.sort(startDates);
                Collections.sort(endDates);

                if (startDates.size()>0){
                    jsonObject.put("timeIn",timeFormat.format(startDates.get(0)));
                }else {
                    jsonObject.put("timeIn","--");
                }

                if (endDates.size()>0){
                    jsonObject.put("timeOut",timeFormat.format(endDates.get(endDates.size()-1)));
                }else {
                    jsonObject.put("timeOut","--");
                }

            }catch (Exception e){
                e.printStackTrace();
            }
        }

        return jsonObject;
    }

    private ArrayList<Date> getWeekDates(Calendar now){
        ArrayList<Date>Weekdays = new ArrayList<>();
        int delta = -now.get(Calendar.DAY_OF_WEEK) + 2; //add 2 if your week start on monday
        now.add(Calendar.DAY_OF_MONTH, delta );

        for (int i = 0; i < 7; i++) {
            Weekdays.add(now.getTime());
            now.add(Calendar.DAY_OF_MONTH, 1);
        }

        return Weekdays;
    }

    private String getWeekDatesSummary(EmployeeReportData reportData){
        SimpleDateFormat dateFormat = new SimpleDateFormat("M/dd/yy");
        SimpleDateFormat weekformat = new SimpleDateFormat("w");
        weekformat.setTimeZone(TimeZone.getTimeZone("UTC"));
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Calendar selectedDate = Tools.getCurrentDateOfSite();

        ArrayList<Date>dates = new ArrayList<>();
        if (selectedDate.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY){
            selectedDate.add(Calendar.WEEK_OF_YEAR,-1);
        }

        for (int i = 0; i <3 ; i++) {
            dates.add(selectedDate.getTime());
            selectedDate.add(Calendar.WEEK_OF_YEAR,1);

        }
        Collections.sort(dates);

        StringBuilder dateTitle = new StringBuilder();

        dateTitle.append("Week ");
        dateTitle.append(weekformat.format(dates.get(0)));
        dateTitle.append(" - ");
        dateTitle.append(weekformat.format(dates.get(dates.size()-1)));

        return dateTitle.toString();

    }

    private JSONArray Create_Employee_Schedule_Report_Schedules(EmployeeReportData reportData){
        JSONArray jsonArray  = new JSONArray();

        SimpleDateFormat format = new SimpleDateFormat("w");

        format.setTimeZone(TimeZone.getTimeZone("UTC"));

        ArrayList<Date>dates = new ArrayList<>();

        try {
            Calendar selectedDate = Tools.getCurrentDateOfSite();

            if (selectedDate.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY){
                selectedDate.add(Calendar.WEEK_OF_YEAR,-1);
            }

            for (int i = 0; i <3 ; i++) {
                dates.add(selectedDate.getTime());
                selectedDate.add(Calendar.WEEK_OF_YEAR,1);

            }
            Collections.sort(dates);

        }catch (Exception e){
            e.printStackTrace();
        }


        for (Date date :dates){
            jsonArray.put(Create_Employee_Schedule_Report_Schedules_Item(reportData,date));
        }

        return jsonArray;
    }

    private JSONObject getHeaderDays(EmployeeReportData reportData){

        JSONObject jsonObject = new JSONObject();

        try {
            TimeZone timeZone = TimeZone.getTimeZone("UTC");
            SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yy");
            SimpleDateFormat dayformat = new SimpleDateFormat("EEEE");

            dayformat.setTimeZone(timeZone);
            dateFormat.setTimeZone(timeZone);

            JSONArray headerDays = new JSONArray();

            Calendar selectedDate = Tools.getCurrentDateOfSite();
            if (selectedDate.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY){
                selectedDate.add(Calendar.WEEK_OF_YEAR,-1);
            }

            ArrayList<Date>Weekdays = getWeekDates(selectedDate);
            for (Date date : Weekdays){
                JSONObject headerItem = new JSONObject();
                headerItem.put("name",dayformat.format(date));
                headerItem.put("date",dateFormat.format(date));
                headerDays.put(headerItem);
            }
            StringBuilder dateTitle = new StringBuilder();

            dateTitle.append(dateFormat.format(Weekdays.get(0)));
            dateTitle.append(" - ");
            dateTitle.append(dateFormat.format(Weekdays.get(Weekdays.size()-1)));


            jsonObject.put("headerDays",headerDays);
            jsonObject.put("date",dateTitle);


        }catch (Exception e){
            e.printStackTrace();
        }

        return jsonObject;
    }

    static class  JavaScriptInterface {

        Context context;
        WebView webView;

        JavaScriptInterface(Context c, WebView w) {
            context = c;
            webView = w;
        }

        @JavascriptInterface
        public void onClickEmployee(String userId) {
            EmployeeReportData reportData = EmployeeReportData.getInstance();
            reportData.setReportType(Employee_Schedule_Report);

            reportData.setUser(getEmployeeByUserId(reportData,userId));
            Handler handler = new Handler(Looper.getMainLooper());

            handler.post(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(View.VISIBLE);
                    wait.setVisibility(View.VISIBLE);
                    webView.loadUrl("file:///android_asset/EmployeeScheduleReport/index.html");
                }
            });

        }

        private User getEmployeeByUserId(EmployeeReportData reportsData,String userID){
            ArrayList<User>users = reportsData.getUsers();
            try {
                for (User user :users){
                    if (user.userId.equalsIgnoreCase(userID)){
                        return user;
                    }
                }

            }catch (Exception e){
                e.printStackTrace();
            }
            return users.get(0);
        }

    }

    private void DownloadSChedules(){

        ArrayList<Date>totalDays = new ArrayList<>();
        ArrayList<Date>dates = new ArrayList<>();
        try {
            Calendar selectedDate = Tools.getCurrentDateOfSite();

            if (selectedDate.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY){
                selectedDate.add(Calendar.WEEK_OF_YEAR,-1);
            }

            for (int i = 0; i <3 ; i++) {
                dates.add(selectedDate.getTime());
                selectedDate.add(Calendar.WEEK_OF_YEAR,1);

            }
            Collections.sort(dates);

        }catch (Exception e){
            e.printStackTrace();
        }

        for (Date date :dates){
            Calendar now = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
            now.setTime(date);
            ArrayList<Date>Weekdays = getWeekDates(now);
            totalDays.addAll(Weekdays);
        }
        Collections.sort(totalDays);
        try {
            dataManager.getEmployeeSchedule(Tools.getSelectedSite().siteid, totalDays.get(0), totalDays.get(totalDays.size() - 1), CrewUDataManager.getInstance().getUser().userId, new OnCompleteListeners.getEmployeeScheduleListener() {
                @Override
                public void getEmployeeScheduleCallback(ArrayList<Schedule_Business_Site_Plan> schedules, Error error) {
                    if (error == null){
                        EmployeeReportData.getInstance().setSchedules(schedules);
                    }
                    webView.loadUrl("file:///android_asset/EmployeeScheduleReport/wendys.min.html");
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }


    }
}
