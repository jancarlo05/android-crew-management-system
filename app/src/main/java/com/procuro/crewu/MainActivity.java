package com.procuro.crewu;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.procuro.crewu.Chat.ChatFragment;
import com.procuro.crewu.Header.HeaderData;
import com.procuro.crewu.Header.HeaderFragment;
import com.procuro.crewu.Profile.User_Profile_Overall_Info_Fragment;
import com.procuro.crewu.Report.ReportFragment;
import com.procuro.crewu.Schedule.ScheduleData;
import com.procuro.crewu.Schedule.ScheduleFragment;
import com.procuro.crewu.Training.TrainingFragment;

import static com.procuro.crewu.Tools.SetStatusBarColor;

public class MainActivity extends AppCompatActivity {

    public static BottomNavigationView navView;
    public static boolean custom;
    int PreviousItem = 0;
    public ConstraintLayout root;
    public FrameLayout resturant_fragment_container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        navView = findViewById(R.id.btm_nav_view);
        resturant_fragment_container = findViewById(R.id.resturant_fragment_container);
        root = findViewById(R.id.root);

        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);


        navView.setSelectedItemId(R.id.nav_training);
        KeyBoandOnlisterner(root);
        SetStatusBarColor(this,R.color.orange);
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(MenuItem item) {
            if (item.getItemId()!=PreviousItem){
                PreviousItem = item.getItemId();
                switch (item.getItemId()) {
                    case R.id.nav_profile:
                        DisplayProfileFragment();
                        return true;
                    case R.id.nav_training:
                        DisplayTraining();
                        return true;
                    case R.id.nav_sched:
                        DisplaySchedule();
                        return true;
                    case R.id.nav_report:
                        DisplayReport();

                        return true;
                    case R.id.nav_chat:
                        DisplayChat();
                        return true;
                }
            }

            return false;
        }
    };

    private void DisplayChat() {
        getSupportFragmentManager().beginTransaction().replace(R.id.resturant_fragment_container,
                new ChatFragment()).commit();
        DisplayHeaderFragment(new ChatFragment());
    }

    private void DisplayReport() {
        getSupportFragmentManager().beginTransaction().replace(R.id.resturant_fragment_container,
                new ReportFragment()).commit();
        DisplayHeaderFragment( new ReportFragment());
    }

    private void DisplaySchedule() {
        getSupportFragmentManager().beginTransaction().replace(R.id.resturant_fragment_container,
                new ScheduleFragment()).commit();
        DisplayHeaderFragment(new ScheduleFragment());
    }

    private void DisplayTraining() {
        getSupportFragmentManager().beginTransaction().replace(R.id.resturant_fragment_container,
                new TrainingFragment()).commit();

        DisplayHeaderFragment(new TrainingFragment());
    }

    private void DisplayProfileFragment() {
        getSupportFragmentManager().beginTransaction().replace(R.id.resturant_fragment_container,
                new User_Profile_Overall_Info_Fragment()).commit();

        DisplayHeaderFragment(new User_Profile_Overall_Info_Fragment());
    }

    private void DisplayHeaderFragment(Fragment fragment) {
        HeaderData.getInstance().setPrevFragment(fragment);
        HeaderData.getInstance().setVideoQuizEnabled(false);
        HeaderData.getInstance().setSched_EmployeeReportEnabled(false);
        HeaderData.getInstance().setRewardEnabled(false);
        getSupportFragmentManager().beginTransaction().replace(R.id.header_fragment_container,
                new HeaderFragment()).commit();
    }

    private void KeyBoandOnlisterner(final View contentView){
        contentView.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {

                        Rect r = new Rect();
                        contentView.getWindowVisibleDisplayFrame(r);
                        int screenHeight = contentView.getRootView().getHeight();

                        // r.bottom is the position above soft keypad or device button.
                        // if keypad is shown, the r.bottom is smaller than that before.
                        int keypadHeight = screenHeight - r.bottom;

                        ConstraintLayout.LayoutParams params = new ConstraintLayout.LayoutParams(
                                ConstraintLayout.LayoutParams.MATCH_PARENT,
                                ConstraintLayout.LayoutParams.MATCH_PARENT
                        );


                        if (keypadHeight > screenHeight * 0.15) { // 0.15 ratio is perhaps enough to determine keypad height.
                            // keyboard is opened
                            navView.setVisibility(View.GONE);
                            params.setMargins(0, Tools.dpToPx(60), 0, 0);
                            resturant_fragment_container.setLayoutParams(params);

                        }
                        else {
                            // keyboard is closed
                            navView.setVisibility(View.VISIBLE);
                            params.setMargins(0, Tools.dpToPx(60), 0, Tools.dpToPx(60));
                            resturant_fragment_container.setLayoutParams(params);
                        }
                    }
                });
    }


    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setMessage("Are you sure you want to exit?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        CrewUDataManager.setInstance(new CrewUDataManager());
                        ScheduleData.setIntance(new ScheduleData());
                        Intent intent = new Intent(MainActivity.this,LoginActivity.class);
                        startActivity(intent);
                        finish();
                    }
                })
                .setNegativeButton("No", null)
                .show();
    }
}
