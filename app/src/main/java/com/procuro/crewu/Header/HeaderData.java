package com.procuro.crewu.Header;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.procuro.crewu.R;

public class HeaderData {

    public static HeaderData Instance = new HeaderData();

    private boolean RewardEnabled;
    private boolean VideoQuizEnabled;
    private boolean Sched_EmployeeReportEnabled;
    private boolean OnRewardFragment;
    private String title;
    private Fragment prevFragment;

    public Fragment getPrevFragment() {
        return prevFragment;
    }

    public void setPrevFragment(Fragment prevFragment) {
        this.prevFragment = prevFragment;
    }

    public boolean isSched_EmployeeReportEnabled() {
        return Sched_EmployeeReportEnabled;
    }

    public void setSched_EmployeeReportEnabled(boolean sched_EmployeeReportEnabled) {
        Sched_EmployeeReportEnabled = sched_EmployeeReportEnabled;
        if (sched_EmployeeReportEnabled){
            RewardEnabled = false;
            VideoQuizEnabled = false;
        }
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isVideoQuizEnabled() {
        return VideoQuizEnabled;
    }

    public void setVideoQuizEnabled(boolean videoQuizEnabled) {
        VideoQuizEnabled = videoQuizEnabled;
        if (videoQuizEnabled){
            RewardEnabled = false;
            Sched_EmployeeReportEnabled = false;
        }
    }

    public boolean isOnRewardFragment() {
        return OnRewardFragment;
    }

    public void setOnRewardFragment(boolean onRewardFragment) {
        OnRewardFragment = onRewardFragment;
    }

    public boolean isRewardEnabled() {
        return RewardEnabled;
    }

    public void setRewardEnabled(boolean rewardEnabled) {
        RewardEnabled = rewardEnabled;
        if (rewardEnabled){
            VideoQuizEnabled = false;
            Sched_EmployeeReportEnabled = false;
        }
    }

    public static HeaderData getInstance() {
        if (Instance==null){
            return new HeaderData();
        }
        return Instance;
    }

    public static void setInstance(HeaderData instance) {
        Instance = instance;
    }

    public static void DisplayHeaderFragment(FragmentActivity fragmentActivity){
        fragmentActivity.getSupportFragmentManager().beginTransaction().replace(R.id.header_fragment_container,
                new HeaderFragment()).commit();
    }
}
