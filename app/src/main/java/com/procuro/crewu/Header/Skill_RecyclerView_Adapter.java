package com.procuro.crewu.Header;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.procuro.apimmdatamanagerlib.Certification;
import com.procuro.apimmdatamanagerlib.CertificationDefinition;
import com.procuro.crewu.CrewUDataManager;
import com.procuro.crewu.R;
import com.procuro.crewu.Tools;

import java.text.SimpleDateFormat;
import java.util.ArrayList;


public class Skill_RecyclerView_Adapter extends RecyclerView.Adapter<Skill_RecyclerView_Adapter.MyViewHolder> {

    private Context mContext;
    private LayoutInflater inflater;
    private ArrayList<Certification> arraylist;
    private ArrayList<String>skills = getPositionSkills();

    public Skill_RecyclerView_Adapter(Context context, ArrayList<Certification> arraylist) {
        mContext = context;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = arraylist;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view ;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.reward_skill_layout,parent,false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        final Certification certification = arraylist.get(position);
        SimpleDateFormat format = new SimpleDateFormat("EEE MM/dd/yyyy hh:mm a");

        try {
            if (skills!=null){
                if (skills.contains(certification.Certification)){
                    int icon = Tools.getSkillIconByCertification(certification);
                    Glide.with(mContext).asDrawable().load(icon).into(holder.icon);
                }
            }else {
                int icon = Tools.getSkillIconByCertification(certification);
                Glide.with(mContext).asDrawable().load(icon).into(holder.icon);
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {

        return Math.min(arraylist.size(), 8);
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView icon;

        public MyViewHolder(View itemView) {
            super(itemView);
            icon = itemView.findViewById(R.id.icon);
        }
    }

    private static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    private ArrayList<String>getPositionSkills(){
        ArrayList<String>skills = new ArrayList<>();
        ArrayList<CertificationDefinition>certifications = CrewUDataManager.getInstance().getCertificationDefinitions();
        if (certifications!=null){
            for (CertificationDefinition certification : certifications){
             if (certification.category.equalsIgnoreCase("0")){
                 skills.add(certification.certification);
             }
            }
        }
        return skills;
    }

}
