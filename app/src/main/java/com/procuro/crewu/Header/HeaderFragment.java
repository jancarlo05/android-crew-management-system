package com.procuro.crewu.Header;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.procuro.crewu.Chat.ChatFragment;
import com.procuro.crewu.CrewUDataManager;
import com.procuro.crewu.HealthCheck.HealthCheckActivity;
import com.procuro.crewu.LoginActivity;
import com.procuro.crewu.R;
import com.procuro.crewu.Reward.RewardFragment;
import com.procuro.crewu.Schedule.ScheduleData;
import com.procuro.crewu.Schedule.ScheduleFragment;
import com.procuro.crewu.Training.Courses.TrainingCoursesFragment;
import com.procuro.crewu.Training.Courses.TrainingQuiz_VideoFragment;
import com.procuro.crewu.Training.TrainingFragment;


public class HeaderFragment extends Fragment {

    private ImageButton reward,healthCheck;
    private Button home;
    TextView title;

    public HeaderFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.main_header, container, false);
        reward = view.findViewById(R.id.reward);
        home = view.findViewById(R.id.home);
        title = view.findViewById(R.id.username);
        healthCheck = view.findViewById(R.id.healthCheck);

        SetupHeader();

        return view;

    }

    private void SetupHeader(){
        final HeaderData headerData = HeaderData.getInstance();

        if (headerData.isVideoQuizEnabled()){
            DisplayTraining(headerData);

        }
        else if (headerData.isSched_EmployeeReportEnabled()){
            DisplayScheduleHeader(headerData);
        }
        else {
            DisplayDefaultHeader();
        }
    }

    private void DisplayReward(final HeaderData headerData) {
        home.setBackgroundResource(R.drawable.arrow_back_white);
        healthCheck.setVisibility(View.GONE);
        reward.setVisibility(View.GONE);
        DisplayFragment(new RewardFragment());

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplayDefaultHeader();
                Back(headerData.getPrevFragment());
            }
        });

    }

    private void DisplayTraining(final HeaderData headerData) {

        title.setText(headerData.getTitle());
        healthCheck.setVisibility(View.GONE);
        reward.setVisibility(View.GONE);
        home.setBackgroundResource(R.drawable.arrow_back_white);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplayDefaultHeader();
                Back(new TrainingFragment());
            }
        });
    }

    private void DisplayScheduleHeader(final HeaderData headerData) {
        title.setText(headerData.getTitle());
        reward.setVisibility(View.GONE);
        healthCheck.setVisibility(View.GONE);
        home.setBackgroundResource(R.drawable.arrow_back_white);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                headerData.setVideoQuizEnabled(false);
                CrewUDataManager.getInstance().setSelectedSchedule(null);
                CrewUDataManager.getInstance().setSelectedCoverDate(null);
                DisplayDefaultHeader();
                Back(new ScheduleFragment());
            }
        });
    }

    private void DisplayFragment(Fragment fragment){
        getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_left,R.anim.slide_left_out).replace(R.id.resturant_fragment_container,
                fragment).commit();
    }
    private void Back(Fragment fragment){
        getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_right,R.anim.slide_right_out).replace(R.id.resturant_fragment_container,
                fragment).commit();
    }


    private void DisplayDefaultHeader(){
        HeaderData.getInstance().setVideoQuizEnabled(false);
        HeaderData.getInstance().setOnRewardFragment(false);
        HeaderData.getInstance().setSched_EmployeeReportEnabled(false);
        reward.setVisibility(View.VISIBLE);
        healthCheck.setVisibility(View.VISIBLE);
        home.setBackgroundResource(R.drawable.logout);
        title.setText("PIMM™ CrewU");
        reward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplayReward(HeaderData.getInstance());
            }
        });

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(getContext())
                        .setMessage("Are you sure you want to exit?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                CrewUDataManager.setInstance(new CrewUDataManager());
                                ScheduleData.setIntance(new ScheduleData());
                                Intent intent = new Intent(getActivity(),LoginActivity.class);
                                getActivity().startActivity(intent);
                                getActivity().finish();
                            }
                        })
                        .setNegativeButton("No", null)
                        .show();
            }
        });

        healthCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), HealthCheckActivity.class);
                getActivity().startActivity(intent);
            }
        });
    }


}
