package com.procuro.crewu.Header;

import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.procuro.apimmdatamanagerlib.Certification;
import com.procuro.apimmdatamanagerlib.CertificationDefinition;
import com.procuro.apimmdatamanagerlib.User;
import com.procuro.crewu.CrewUDataManager;
import com.procuro.crewu.Profile.CustomRole;
import com.procuro.crewu.Profile.User_Skill_Options_ListviewAdapter;
import com.procuro.crewu.Profile.User_Skills_RecyclerViewAdapter;
import com.procuro.crewu.R;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;

import static com.procuro.crewu.Tools.DisableView;
import static com.procuro.crewu.Tools.DisplayProfilePicture;


public class UserHeaderFragment extends Fragment {

    private TextView name,title,message;
    private Button info;
    private RecyclerView skills;
    private CircleImageView circleImageView;

    public UserHeaderFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.user_header_fragment, container, false);

        name = view.findViewById(R.id.name);
        title = view.findViewById(R.id.title);
        info = view.findViewById(R.id.position_info);
        skills = view.findViewById(R.id.skills);
        message = view.findViewById(R.id.message);
        circleImageView = view.findViewById(R.id.profile_picture);

        DisplayData();

        setupOnclicks();

        return view;

    }


    private void setupOnclicks(){
        info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplaySkillPopUp(getContext(),info);
            }
        });
    }

    private void DisplayData(){
        CrewUDataManager crewUDataManager = CrewUDataManager.getInstance();
        User user = crewUDataManager.getUser();
        try {

            name.setText(user.getFullName());

            DisplayProfilePicture(circleImageView,getContext());

            getCurrentUserRole(user);

            if (user.certificationList !=null && user.certificationList.size()>0){
                ArrayList<Certification>FilteredPositions = new ArrayList<>();

                for (Certification certification : user.certificationList){
                    if (getPositionSkills().contains(certification.Certification)){
                        FilteredPositions.add(certification);
                    }
                }
                    if (FilteredPositions.size()>0){
                        Skill_RecyclerView_Adapter adapter = new Skill_RecyclerView_Adapter(getContext(),FilteredPositions);
                        skills.setLayoutManager(new GridLayoutManager(getContext(),8));
                        skills.setAdapter(adapter);
                        message.setVisibility(View.GONE);
                    }
                }

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void getCurrentUserRole(User user){
        try {
            if (user.roles!=null){
                ArrayList<CustomRole> customRoles = CustomRole.getAllCustomRole(user.roles);

                if (customRoles.size()>=1){
                    CustomRole customRole = customRoles.get(0);
                    title.setText(customRole.getName());
                }else {
                    title.setText("");
                }

            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private ArrayList<String>getPositionSkills(){
        ArrayList<String>skills = new ArrayList<>();
        ArrayList<CertificationDefinition>certifications = CrewUDataManager.getInstance().getCertificationDefinitions();
        if (certifications!=null){
            for (CertificationDefinition certification : certifications){
                if (certification.category.equalsIgnoreCase("0")){
                    skills.add(certification.certification);
                }
            }
        }
        return skills;
    }


    public void DisplaySkillPopUp(final Context context, View anchor) {

        final SimpleTooltip tooltip = new SimpleTooltip.Builder(context)
                .anchorView(anchor)
                .gravity(Gravity.BOTTOM)
                .dismissOnOutsideTouch(true)
                .dismissOnInsideTouch(false)
                .showArrow(true)
                .arrowColor(ContextCompat.getColor(context, R.color.white))
                .transparentOverlay(false)
                .overlayOffset(0)
                .overlayWindowBackgroundColor(ContextCompat.getColor(context,R.color.tooltipOverlay))
                .contentView(R.layout.user_header_skill_popup)
                .focusable(true)
                .build();
        tooltip.show();

        info.hoang8f.android.segmented.SegmentedGroup infgroup;
        final RadioButton opsSkill,positionSkill,productSkill,mgmtskill;
        final RecyclerView recyclerView;

        infgroup = tooltip.findViewById(R.id.segmented2);
        mgmtskill = tooltip.findViewById(R.id.mgmtskill);
        recyclerView = tooltip.findViewById(R.id.recyclerView);
        opsSkill = tooltip.findViewById(R.id.opsSkill);
        positionSkill = tooltip.findViewById(R.id.position_skills);
        productSkill = tooltip.findViewById(R.id.product_skills);

        infgroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == opsSkill.getId()){
                    DisplaypOpsSkills(recyclerView,CrewUDataManager.getInstance().getUser());

                }else if (checkedId == positionSkill.getId()){
                    DisplaypositionSkills(recyclerView,CrewUDataManager.getInstance().getUser());

                }else if (checkedId == productSkill.getId()){
                    DisplaypProductSkills(recyclerView,CrewUDataManager.getInstance().getUser());
                }
                else if (checkedId == mgmtskill.getId()){
                    DisplayManagementSkills(recyclerView,CrewUDataManager.getInstance().getUser());
                }
            }
        });

        positionSkill.setChecked(true);

    }

    private void DisplaypositionSkills(RecyclerView recyclerView,User user){
        //CATEGORY 1
        DisplayData(getSKillsByCategory("0"),recyclerView,user);
    }

    private void DisplaypProductSkills(RecyclerView recyclerView,User user){
        //CATEGORY 1
        DisplayData(getSKillsByCategory("1"),recyclerView,user);
    }

    private void DisplaypOpsSkills(RecyclerView recyclerView,User user){
        //CATEGORY 2
        DisplayData(getSKillsByCategory("2"),recyclerView,user);
    }
    private void DisplayManagementSkills(RecyclerView recyclerView,User user){
        //CATEGORY 3
        DisplayData(getSKillsByCategory("3"),recyclerView,user);
    }

    private ArrayList<CertificationDefinition> getSKillsByCategory(String category){
        ArrayList<CertificationDefinition> skills = new ArrayList<>();
        ArrayList<CertificationDefinition>certificationDefinitions = CrewUDataManager.getInstance().getCertificationDefinitions();

        for (CertificationDefinition certificationDefinition : certificationDefinitions){
            if (certificationDefinition.category.equalsIgnoreCase(category)){
                skills.add(certificationDefinition);
            }
        }
        return  skills;
    }

    private void DisplayData(ArrayList<CertificationDefinition> skills,RecyclerView recyclerView,User user){
        User_Skills_RecyclerViewAdapter recyclerViewAdapter  = new User_Skills_RecyclerViewAdapter(getContext(),skills,false,user);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(),3));
        recyclerView.setAdapter(recyclerViewAdapter);
    }


}
