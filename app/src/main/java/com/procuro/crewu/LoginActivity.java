package com.procuro.crewu;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.IntentCompat;
import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.Layout;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.animation.GridLayoutAnimationController;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.gson.Gson;
import com.procuro.apimmdatamanagerlib.CertificationDefinition;
import com.procuro.apimmdatamanagerlib.OnCompleteListeners;
import com.procuro.apimmdatamanagerlib.Site;
import com.procuro.apimmdatamanagerlib.User;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;
import com.procuro.crewu.Chat.ChatData;
import com.procuro.crewu.Profile.UserProfileData;

import org.json.JSONArray;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.procuro.crewu.Tools.SetStatusBarColor;
import static com.procuro.crewu.Tools.clearAppData;
import static com.procuro.crewu.Tools.dpToPx;

public class LoginActivity extends AppCompatActivity {

    Button LoginButton;

    private LinearLayout spidContainer,loginContainer,imge_container,layout_container;
    private EditText txtPassword, txtUsername, txtSPID;
    private View.OnTouchListener onTouchListener;
    private View.OnClickListener onClickListener;
    private com.procuro.apimmdatamanagerlib.aPimmDataManager dataManager ;
    private CheckBox rememberme;
    private ConstraintLayout root,BottomimageContainer;
    private ImageView profile_picture;
    private ImageView bg_image;
    private TextView version;

    private LoadingDialog loadingDialog;
    private Drawable drawable;

    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS= 7;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        LoginButton = findViewById(R.id.login);
        root = findViewById(R.id.root);
        spidContainer = findViewById(R.id.spid_container);
        txtSPID = findViewById(R.id.spid);
        txtUsername = findViewById(R.id.username);
        txtPassword = findViewById(R.id.password);
        rememberme = findViewById(R.id.checkBox);
        profile_picture = findViewById(R.id.new_profilePicture);
        bg_image = findViewById(R.id.bg_image);
        BottomimageContainer = findViewById(R.id.BottomimageContainer);
        version = findViewById(R.id.version);

        DisplayVersion();

        Pref.getInstance().Initialize(getApplicationContext());

        checkAndRequestPermissions();

        getSharedPref();

        setUpOnlick();

        KeyBoandOnlisterner(root);

        SetupLoadingBackground();
    }

    private void SetupLoadingBackground(){
        final boolean[] isDisplayed = {false};
        root.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (!isDisplayed[0]){
                    isDisplayed[0] = true;

                    loadingDialog = new LoadingDialog(LoginActivity.this);
                    loadingDialog.setBackroundBitmap(root,LoginActivity.this);
                }

            }
        });
    }

    @SuppressLint("ClickableViewAccessibility")
    private void setUpOnlick(){
        LoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InitializeLogin();
            }
        });

        this.setEventListeners();

//        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
////        txtUsername.setOnTouchListener(onTouchListener);
////        txtPassword.setOnTouchListener(onTouchListener);
////        txtUsername.setOnClickListener(onClickListener);
////        txtPassword.setOnClickListener(onClickListener);
////        txtSPID.setOnTouchListener(onTouchListener);
////        txtSPID.setOnTouchListener(onTouchListener);

        txtPassword.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    InitializeLogin();
                    return true;
                }
                return false;
            }
        });
    }

    private void InitializeLogin(){
        Tools.HideKeyboard(this);
        loadingDialog.setCancelable(false);

        loadingDialog.show();

        if (ValidInputs()){
            if (isConnectedToInternet()){
                LoginToServer();
            }
        }else {
            loadingDialog.dismiss();
            Tools.DisplaySnackbar("Please complete all the fields",root);

        }
    }

    private void LoginToServer(){
        loadingDialog.setDialogMessage("Loading Data, Please Wait...");
        dataManager = aPimmDataManager.getInstance();
        dataManager.initializeWithSPID(txtSPID.getText().toString());
        dataManager.initializeWithAppName("CrewU");
        dataManager.loginWithUsernameAndPassword(txtUsername.getText().toString(), txtPassword.getText().toString(), new OnCompleteListeners.OnLoginCompleteListener() {
            @Override
            public void onLoginComplete(User user, Error error) {
                if (error == null){
                    CrewUDataManager.getInstance().setSPID(txtSPID.getText().toString());
                    CrewUDataManager.getInstance().setUsername(txtUsername.getText().toString());
                    CrewUDataManager.getInstance().setPassword(txtPassword.getText().toString());
                    CrewUDataManager.getInstance().setRememberMe(rememberme.isChecked());
                    CrewUDataManager.getInstance().setUser(user);
                    loadingDialog.setProgressAnimate(30);

                    if (CrewUDataManager.getInstance().getCountrycode()==null){
                        ParseCounrtryJson();
                    }
                    DownloadSites();


                }else {
                    Tools.HideKeyboard(LoginActivity.this);
                    loadingDialog.dismiss();
                    Tools.DisplaySnackbar("Incorrect SPID , username or password",root);

                }
            }
        });
    }

    private boolean ValidInputs(){
        return txtSPID.getText().toString().length() > 0 &&
                txtUsername.getText().toString().length() > 0 &&
                txtPassword.getText().toString().length() > 0;
    }

    private boolean isConnectedToInternet() {
        boolean connected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        assert connectivityManager != null;
        if (Objects.requireNonNull(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)).getState() == NetworkInfo.State.CONNECTED ||
                Objects.requireNonNull(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI)).getState() == NetworkInfo.State.CONNECTED) {
            //we are connected to a network
            connected = true;
        }

        return connected;
    }

    private void setEventListeners() {
        onTouchListener = new View.OnTouchListener() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                return false;
            }
        };

        onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
            }
        };
    }

    private void DownloadSites(){
      dataManager.getSiteListForLoggedInUser(new OnCompleteListeners.getSiteListForLoggedInUserWithCallbackListener() {
            @Override
            public void getSiteListForLoggedInUserWithCallback(ArrayList<Site> siteList, Error error) {
                if (error == null){
                    CrewUDataManager.getInstance().setSites(siteList);
                    loadingDialog.setProgressAnimate(60);
                    if (siteList.size()>0){
                        DownloadGetStoreUsers(siteList.get(0));
                    }

                    Download_getCertificationDefinitionsWithCallback();

                }else {
                    DownloadSites();
                }
            }
        });
    }

    private void Download_getCertificationDefinitionsWithCallback(){
        dataManager.getCertificationDefinitionsWithCallback(new OnCompleteListeners.getCertificationDefinitionsWithCallbackListener() {
            @Override
            public void getCertificationDefinitionsWithCallback(ArrayList<CertificationDefinition> certificationDefinitions, Error error) {
                if (error == null){
                    loadingDialog.setProgressAnimate(100);
                    CrewUDataManager.getInstance().setCertificationDefinitions(certificationDefinitions);
                    Pref.getInstance().setSharedpref();
                    Intent intent = new Intent(LoginActivity.this,MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                            Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    loadingDialog.dismiss();
                    finish();
                }else {
                    Download_getCertificationDefinitionsWithCallback();
                }
            }
        });
    }

    public void ParseCounrtryJson(){

        if (CrewUDataManager.getInstance().getCountries()==null){

            InputStream inputStream = this.getResources().openRawResource(R.raw.countries);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

            int ctr;
            try {
                ctr = inputStream.read();
                while (ctr != -1) {
                    byteArrayOutputStream.write(ctr);
                    ctr = inputStream.read();
                }
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                JSONArray jsonArray = new JSONArray(byteArrayOutputStream.toString());
                String[]countries = new String[jsonArray.length()];
                String[]code = new String[jsonArray.length()];

                for (int i = 0; i <jsonArray.length() ; i++) {
                    countries[i] = jsonArray.getJSONObject(i).getString("name");
                    code[i] = jsonArray.getJSONObject(i).getString("abbreviation");
                }

                CrewUDataManager.getInstance().setCountries(countries);
                CrewUDataManager.getInstance().setCountrycode(code);

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    private void DownloadGetStoreUsers(final Site site){
        dataManager.getStoreUsers(site.siteid, new OnCompleteListeners.getStoreUsersCallbackListener() {
            @Override
            public void getStoreUsersCallback(ArrayList<User> storeUsers, Error error) {
                if (error == null){
                    CrewUDataManager.getInstance().setUsers(storeUsers);
                }else {
                    DownloadGetStoreUsers(site);
                }
            }
        });
    }


    private boolean checkAndRequestPermissions() {
        int camera = ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA);
        int wtite = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int read = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (wtite != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (camera != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (read != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        Log.d("in fragment on request", "Permission callback called-------");
        if (requestCode == REQUEST_ID_MULTIPLE_PERMISSIONS) {
            Map<String, Integer> perms = new HashMap<>();
            // Initialize the map with both permissions
            perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
            perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
            perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
            // Fill with actual results from user
            if (grantResults.length > 0) {
                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);
                // Check for both permissions
                if (perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED && perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    Log.d("in fragment on request", "CAMERA & WRITE_EXTERNAL_STORAGE READ_EXTERNAL_STORAGE permission granted");
                    // process the normal flow
                    //else any one or both the permissions are not granted
                } else {
                    Log.d("in fragment on request", "Some permissions are not granted ask again ");
                    //permission is denied (this is the first time, when "never ask again" is not checked) so ask again explaining the usage of permission
//                        // shouldShowRequestPermissionRationale will return true
                    //show the dialog or snackbar saying its necessary and try again otherwise proceed with setup.
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA) ||
                            ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        showDialogOK("Camera and Storage Permission required for this app",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        switch (which) {
                                            case DialogInterface.BUTTON_POSITIVE:
                                                clearAppData(getApplicationContext());
                                                break;
                                            case DialogInterface.BUTTON_NEGATIVE:
                                                // proceed with logic by disabling the related features or quit the app.
                                                break;
                                        }
                                    }
                                });
                    }
                    //permission is denied (and never ask again is  checked)
                    //shouldShowRequestPermissionRationale will return false
                    else {
                        Toast.makeText(this, "Go to settings and enable permissions", Toast.LENGTH_LONG)
                                .show();
                        //                            //proceed with logic by disabling the related features or quit the app.
                    }
                }
            }
        }

    }

    private void showDialogOK(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setTitle("Crew U Permission")
                .setCancelable(false)
                .setMessage("Allow the permissions in order to use the Application")
                .setPositiveButton("Quit", okListener)
                .create()
                .show();
    }

    private void getSharedPref() {
        CrewUDataManager prev = Pref.getInstance().getSharedPref();
        if (prev!=null){
            CrewUDataManager obj = CrewUDataManager.getInstance();
            obj.setSPID(prev.getSPID());
            obj.setUsername(prev.getUsername());
            obj.setPassword(prev.getPassword());
            obj.setUser(prev.getUser());
            obj.setProfilePath(prev.getProfilePath());
            obj.setCountries(prev.getCountries());
            obj.setCountrycode(prev.getCountrycode());
            obj.setRememberMe(prev.isRememberMe());
        }

//        CrewUDataManager.getInstance().setSPID("wendys");
        drawable = getResources().getDrawable(R.drawable.main_background_crop,getTheme());
        SetStatusBarColor(this,R.color.status_gray);
        root.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.status_gray));

        if (CrewUDataManager.getInstance().getSPID() != null) {

            if (CrewUDataManager.getInstance().getSPID().equalsIgnoreCase("wendys")){
                drawable =  getResources().getDrawable(R.drawable.wendys_new_menu_bg,getTheme());
                SetStatusBarColor(this,R.color.status_red);
                root.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.status_red));
            }
            if (CrewUDataManager.getInstance().getProfilePath()!=null){
                File file = new File(CrewUDataManager.getInstance().getProfilePath());
                if (file.exists()){
                    try {
                        Glide.with(this).load(file)
                                .apply(new RequestOptions().centerInside().circleCrop().placeholder(R.drawable.crew_u_default_logo)).into(profile_picture);
//                        Glide.with(this).load(file).centerInside().into(profile_picture);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }

            txtSPID.setText(CrewUDataManager.getInstance().getSPID());
            spidContainer.setVisibility(View.GONE);
            if (CrewUDataManager.getInstance().isRememberMe()) {
                txtUsername.setText(CrewUDataManager.getInstance().getUsername());
                txtPassword.setText(CrewUDataManager.getInstance().getPassword());
                rememberme.setChecked(true);
            } else {
                txtUsername.setText("");
                txtPassword.setText("");
                rememberme.setChecked(false);
            }
        }else {
            spidContainer.setVisibility(View.VISIBLE);
            bg_image.setBackgroundResource(R.drawable.main_background_crop);
        }

        getWindow().setBackgroundDrawable(drawable);
        ChatData prevChatData = Pref.getInstance().getChatPref();
        if (prevChatData!=null){
            ChatData.getInstance().setMessages(prevChatData.getMessages());
        }
    }

    private void KeyBoandOnlisterner(final View contentView){
        try {
            contentView.getViewTreeObserver().addOnGlobalLayoutListener(
                    new ViewTreeObserver.OnGlobalLayoutListener() {
                        @Override
                        public void onGlobalLayout() {
                            Rect r = new Rect();
                            contentView.getWindowVisibleDisplayFrame(r);
                            int screenHeight = contentView.getRootView().getHeight();

                            // r.bottom is the position above soft keypad or device button.
                            // if keypad is shown, the r.bottom is smaller than that before.
                            int keypadHeight = screenHeight - r.bottom;

                            if (keypadHeight > screenHeight * 0.15) { // 0.15 ratio is perhaps enough to determine keypad height.
                                // keyboard is opened
                                BottomimageContainer.setVisibility(View.INVISIBLE);
                                root.setBackgroundResource(R.drawable.gray_bg);
//                                ConstraintLayout.LayoutParams params = new ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,1);
//                                params.bottomToBottom= 0;
//                                BottomimageContainer.setLayoutParams(params);

                            }else {
                                BottomimageContainer.setVisibility(View.VISIBLE);
                                root.setBackground(drawable);
                            }
                        }
                    });
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void DisplayVersion(){
        PackageManager manager = this.getPackageManager();
        PackageInfo info = null;
        try {
            info = manager.getPackageInfo(this.getPackageName(), PackageManager.GET_ACTIVITIES);

            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("PIMM™ CrewU Version ");
            stringBuilder.append(info.versionName);

            version.setText(stringBuilder.toString());

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

    }

}
