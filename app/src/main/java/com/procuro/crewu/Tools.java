package com.procuro.crewu;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import com.bumptech.glide.Glide;
import com.google.android.material.snackbar.Snackbar;
import com.procuro.apimmdatamanagerlib.Certification;
import com.procuro.apimmdatamanagerlib.Site;
import com.procuro.apimmdatamanagerlib.User;

import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;

public class Tools {
    public static void DisplaySnackbar(String Message, View view){
        try {
            Snackbar snackbar = Snackbar
                    .make(view, Message, Snackbar.LENGTH_LONG);
            snackbar.show();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void HideKeyboard(FragmentActivity activity) {
        try {
            View view = activity.getCurrentFocus();
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);

            if (view == null) {
                view = new View(activity);
            }
            assert imm != null;
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public static boolean ValidString(String value){
        boolean valid = false;
        if (value!=null){
            if (!value.equalsIgnoreCase("null") && !value.equalsIgnoreCase("")){
                valid = true;
            }
        }
        return valid;
    }

    public static String CheckString(String value,String ifnull){
        if (value!=null){
            if (value.equalsIgnoreCase("null") || value.equalsIgnoreCase("")){
                value = ifnull;
            }
        }else {
            value = ifnull;
        }
        return value;
    }

    public static int getSkillIconByCertification(Certification certification){

        if (certification.Certification.equalsIgnoreCase("0")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.dining_1;

            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.dining_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.dining_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.dining_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.dining_5;

            }else {
                return R.drawable.dining_room;
            }


        }else if (certification.Certification.equalsIgnoreCase("1")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.frontregister_1;

            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.frontregister_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.frontregister_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.frontregister_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.frontregister_5;

            }else {
                return R.drawable.front_register_gray;
            }
        }
        else if (certification.Certification.equalsIgnoreCase("2")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.friesposition_1;

            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.friesposition_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.friesposition_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.friesposition_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.friesposition_5;

            }else {
                return R.drawable.friesposition;

            }
        }
        else if (certification.Certification.equalsIgnoreCase("3")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.grill_1;

            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.grill_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.grill_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.grill_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.grill_5;

            }else {
                return R.drawable.grill;
            }
        }
        else if (certification.Certification.equalsIgnoreCase("4")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.puwregister_1;
            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.puwregister_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.puwregister_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")) {
                return R.drawable.puwregister_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.puwregister_5;

            }else {
                return R.drawable.puwregister;

            }

        }
        else if (certification.Certification.equalsIgnoreCase("5")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.sandwichposition_1;

            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.sandwichposition_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.sandwichposition_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.sandwichposition_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.sandwichposition_5;

            }else {
                return R.drawable.sandwichposition;
            }

        }
        else if (certification.Certification.equalsIgnoreCase("6")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.productcoord_1;
            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.productcoord_2;
            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.productcoord_3;
            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.productcoord_4;
            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.productcoord_5;
            }else {
                return R.drawable.productcoordinator_gray;
            }

        }
        else if (certification.Certification.equalsIgnoreCase("7")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.servicecoord_1;

            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.servicecoord_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.servicecoord_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.servicecoord_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.servicecoord_5;

            }else {
                return R.drawable.servicecoord;

            }


        }
        else if (certification.Certification.equalsIgnoreCase("8")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.bacon_1;
            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.bacon_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.bacon_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.bacon_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.bacon_5;

            }else {
                return R.drawable.bacon_gray;

            }


        }
        else if (certification.Certification.equalsIgnoreCase("9")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.beverages_1;

            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.beverage_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.beverage_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.beverage_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.beverage_5;

            }else {
                return R.drawable.beverage_gray;
            }

        }
        else if (certification.Certification.equalsIgnoreCase("10")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.chili_1;

            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.chili_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.chili_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.chili_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.chili_5;

            }else {
                return R.drawable.chili_gray;

            }

        }
        else if (certification.Certification.equalsIgnoreCase("11")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.foodprep_1;
            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.foodprep_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.foodprep_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.foodprep_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.foodprep_5;

            }else {
                return R.drawable.foodprep;
            }

        }
        else if (certification.Certification.equalsIgnoreCase("12")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.fries_1;

            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.fries_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.fries_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.fries_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.fries_5;

            }else {
                return R.drawable.fries_gray;

            }
        }
        else if (certification.Certification.equalsIgnoreCase("13")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.salad_1;

            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.salad_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.salad_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.salad_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.salad_5;

            }else {
                return R.drawable.salad;

            }

        }
        else if (certification.Certification.equalsIgnoreCase("14")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.sandwich_1;

            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.sandwich_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.sandwich_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.sandwich_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.sandwich_5;

            }else {
                return R.drawable.sandwich;

            }

        }
        else if (certification.Certification.equalsIgnoreCase("15")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.newproduct_1;

            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.newproduct_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.newproduct_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.newproduct_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.newproduct_5;

            }else {
                return R.drawable.newproduct;

            }

        }
        else if (certification.Certification.equalsIgnoreCase("16")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.welcome_1;

            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.welcome_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.welcome_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.welcome_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.welcome_5;

            }else {
                return R.drawable.orientation_gray;

            }

        }
        else if (certification.Certification.equalsIgnoreCase("17")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.foodsafety_1;
            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.foodsafety_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.foodsafety_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.foodsafety_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.foodsafety_5;

            }else {
                return R.drawable.foodsafety_gray;
            }

        }
        else if (certification.Certification.equalsIgnoreCase("18")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.cleaning_1;
            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.cleaning_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.cleaning_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.cleaning_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.cleaning_5;

            }else {
                return R.drawable.cleaning_gray;

            }

        }
        else if (certification.Certification.equalsIgnoreCase("19")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.cus_cur_1;

            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.cus_cur_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")) {
                return R.drawable.cus_cur_3;
            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.cus_cur_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.cus_cur_5;

            }else {
                return R.drawable.customer_courtesy_gray;

            }

        }
        else if (certification.Certification.equalsIgnoreCase("20")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.fryerops_1;

            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.fryerops_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.fryerops_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.fryerops_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.fryerops_5;

            }else {
                return R.drawable.fryerops;

            }

        }
        else if (certification.Certification.equalsIgnoreCase("21")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.grillops_1;

            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.grillops_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.grillops_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.grillops_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.grillops_5;

            }else {
                return R.drawable.grillops;

            }

        }
        else if (certification.Certification.equalsIgnoreCase("22")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.openclose_1;

            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.openclose_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.openclose_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.openclose_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.openclose_5;

            }else {
                return R.drawable.openclose;

            }
        }
        else if (certification.Certification.equalsIgnoreCase("23")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.linesetup_1;

            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.linesetup_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.linesetup_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.linesetup_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.linesetup_5;

            }else {
                return R.drawable.linesetup;

            }
        }

        else if (certification.Certification.equalsIgnoreCase("24")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.welcome_1;

            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.welcome_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.welcome_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.welcome_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.welcome_5;

            }else {
                return R.drawable.orientation_gray;

            }
        }

        else if (certification.Certification.equalsIgnoreCase("25")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.foodsafety_1;
            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.foodsafety_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.foodsafety_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.foodsafety_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.foodsafety_5;

            }else {
                return R.drawable.foodsafety_gray;
            }

        }

        else if (certification.Certification.equalsIgnoreCase("26")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.foodm_1;
            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.foodm_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.foodm_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.foodm_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.foodm_5;

            }else {
                return R.drawable.foodm;
            }

        }

        else if (certification.Certification.equalsIgnoreCase("27")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.bm_1;
            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.bm_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.bm_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.bm_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.bm_5;

            }else {
                return R.drawable.bm;
            }

        }

        else if (certification.Certification.equalsIgnoreCase("28")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.coaching_1;
            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.coaching_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.coaching_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.coaching_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.coaching_5;

            }else {
                return R.drawable.coaching;
            }

        }

        else if (certification.Certification.equalsIgnoreCase("29")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.talentmgmt_1;
            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.talentmgmt_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.talentmgmt_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.talentmgmt_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.talentmgmt_5;

            }else {
                return R.drawable.talentmgmt;
            }

        }

        else if (certification.Certification.equalsIgnoreCase("30")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.labormgmt_1;
            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.labormgmt_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.labormgmt_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.labormgmt_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.labormgmt_5;

            }else {
                return R.drawable.labormgmt;
            }

        }

        else if (certification.Certification.equalsIgnoreCase("31")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.opsleader_1;
            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.opsleader_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.opsleader_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.opsleader_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.opsleader_5;

            }else {
                return R.drawable.opsleader;
            }

        }

        else if (certification.Certification.equalsIgnoreCase("32")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.positioning_1;
            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.positioning_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.positioning_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.positioning_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.positioning_5;

            }else {
                return R.drawable.positioning;
            }

        }

        else if (certification.Certification.equalsIgnoreCase("33")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.training_1;
            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.training_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.training_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.training_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.training_5;

            }else {
                return R.drawable.training;
            }

        }

        else if (certification.Certification.equalsIgnoreCase("34")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.cei_1;
            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.cei_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.cei_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.cei_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.cei_5;

            }else {
                return R.drawable.cei;
            }

        }

        else if (certification.Certification.equalsIgnoreCase("35")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.voc_1;
            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.voc_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.voc_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.voc_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.voc_5;

            }else {
                return R.drawable.voc;
            }

        }

        return 0;

    }

    public static void DisableView(View view, float alpha){
        view.setAlpha(alpha);
        view.setEnabled(false);
    }

    public static void EnableView(View view, float alpha){
        view.setAlpha(alpha);
        view.setEnabled(true);
    }

    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static void clearAppData(Context context) {
        try {
            Runtime runtime = Runtime.getRuntime();
            runtime.exec("pm clear " + context.getPackageName() + " HERE");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void DisplayProfilePicture(CircleImageView imageView,Context context){
        try {
            CrewUDataManager crewUDataManager = CrewUDataManager.getInstance();
            if (crewUDataManager.getProfilePath()!=null){
                File file = new File(crewUDataManager.getProfilePath());
                if (file.exists()){
                    Glide.with(context).load(file).into(imageView);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    public static Site getSelectedSite(){
        ArrayList<Site>sites = new ArrayList<>();
        sites.addAll(CrewUDataManager.getInstance().getSites());
        if (sites.size()>0){
            return sites.get(0);
        }
        return null;
    }


    public static Calendar getCurrentDateOfSite(){
        Site site = getSelectedSite();
        Calendar calendar = Calendar.getInstance();
        calendar.setFirstDayOfWeek(Calendar.MONDAY);
        calendar.setTimeZone(TimeZone.getTimeZone("UTC"));
        assert site != null;
        calendar.add(Calendar.MINUTE,site.effectiveUTCOffset);
        if (calendar.get(Calendar.DAY_OF_WEEK)== Calendar.SUNDAY){
            calendar.add(Calendar.WEEK_OF_YEAR,-1);
        }
        return calendar;
    }

    public static ArrayList<User> getActiveUser(ArrayList<User>users){
        ArrayList<User> newUsers = new ArrayList<>();
        if (users!=null){
            for (User user : users){
                if (user.active){
                    newUsers.add(user);
                }
            }
        }
        return newUsers;
    }

    public static int FloatRGB(double value){
        return (int)Math.round(value*255) ;
    }
    public static String getPositionHEXColor(int position) {

        int r =0;
        int g = 0;
        int b = 0;
//        if (position == 0){
//            return Color.rgb(FloatRGB(0.73),FloatRGB(0.73),FloatRGB(0.73));
//        }
//        else if (position == 1){
//            return Color.rgb(FloatRGB(0.98),FloatRGB(0.90),FloatRGB(0.44));
//        }
//        else if (position == 2){
//            return Color.rgb(FloatRGB(0.52),FloatRGB(0.65),FloatRGB(0.92));
//        }
//        else if (position == 3){
//            return Color.rgb(FloatRGB(0.93),FloatRGB(0.43),FloatRGB(0.47));
//        }
//        else if (position == 4){
//            return Color.rgb(FloatRGB(0.74),FloatRGB(0.58),FloatRGB(0.91));
//        }
//        else if (position == 5){
//            return Color.rgb(FloatRGB(0.42),FloatRGB(0.86),FloatRGB(0.87));
//        }
//        else if (position == 6){
//            return Color.rgb(FloatRGB(0.99),FloatRGB(0.84),FloatRGB(0.71));
//        }
//        else if (position == 7){
//            return Color.rgb(FloatRGB(0.49),FloatRGB(0.77),FloatRGB(0.53));
//        }
//        else if (position == 8){
//            return Color.rgb(FloatRGB(0.96),FloatRGB(0.64),FloatRGB(0.44));
//        }
//        else if (position == 9){
//            return Color.rgb(FloatRGB(0.65),FloatRGB(0.92),FloatRGB(0.44));
//        }
//        else if (position == 10){
//            return Color.rgb(FloatRGB(0.76),FloatRGB(0.32),FloatRGB(0.53));
//        }
//        else if (position == 11){
//            return Color.rgb(FloatRGB(0.93),FloatRGB(0.71),FloatRGB(0.75));
//        }
//        else if (position == 12){
//            return Color.rgb(FloatRGB(0.85),FloatRGB(0.88),FloatRGB(0.99));
//        }
        if (position == 13) {
            r =FloatRGB(0.56);
            g = FloatRGB(0.83);
            b = FloatRGB(0.97);

        } else if (position == 14) {
            r =FloatRGB(0.19);
            g = FloatRGB(0.75);
            b = FloatRGB(0.87);
        }
//        else if (position == 15){
//            return Color.rgb(FloatRGB(0.80),FloatRGB(1.00),FloatRGB(1.00));
//        }
//        else if (position == 16){
//            return Color.rgb(FloatRGB(0.44),FloatRGB(0.88),FloatRGB(0.59));
//        }
//        else if (position == 17){
//            return Color.rgb(FloatRGB(0.91),FloatRGB(0.81),FloatRGB(0.21));
//        }
//        else if (position == 18){
//            return Color.rgb(FloatRGB(0.75),FloatRGB(1.00),FloatRGB(0.90));
//        }
//        else if (position == 19){
//            return Color.rgb(FloatRGB(0.04),FloatRGB(0.59),FloatRGB(0.61));
//        }
//        else if (position == 20){
//            return Color.rgb(FloatRGB(0.59),FloatRGB(0.42),FloatRGB(1.00));
//        }
//        else if (position == 21){
//            return Color.rgb(FloatRGB(0.95),FloatRGB(0.56),FloatRGB(0.38));
//        }
//        else if (position == 22){
//            return Color.rgb(FloatRGB(0.96),FloatRGB(0.85),FloatRGB(0.42));
//        }
//        else if (position == 23){
//            return Color.rgb(FloatRGB(0.94),FloatRGB(0.76),FloatRGB(0.63));
//        }
//        else if (position == 24){
//            return Color.rgb(FloatRGB(0.67),FloatRGB(0.87),FloatRGB(0.83));
//        }
//        else if (position == 25){
//            return Color.rgb(FloatRGB(1.00),FloatRGB(0.54),FloatRGB(0.71));
//        }else {
//            return Color.rgb(FloatRGB(0.73),FloatRGB(0.73),FloatRGB(0.73));
//        }

        System.out.println("R : "+ r + " G: "+ g+" b : "+b);

        return String.format("#%02x%02x%02x", r, g, b);
    }


    public static int getPositionStringColor (int position){
        if (position>=0){
            return R.color.black;
        }else {
            return R.color.white;
        }
    }

    public static String getPositionString(int position){
        if(position == 0) {
            return "P";
        }
        else if (position == -1){
            return "A";
        }
        else if (position == -2){
            return "B15";
        }
        else if (position == -3){
            return "B30";
        }
        else if (position == -4){
            return "C";
        }
        else if (position == -5){
            return "T";
        }
        else if (position >100){

            return  String.valueOf(position - 100);
        }
        else {
            return String.valueOf(position);
        }
    }

    public static int getPositionColor(int position){

        if (position == -5){
            return Color.rgb(FloatRGB(0.5),FloatRGB(0.0),FloatRGB(0.5));
        }
        else if (position == -4){
            return Color.rgb(FloatRGB(0.04),FloatRGB(0.64),FloatRGB(0.04));
        }
        else if (position == -3){
            return Color.BLUE;
        }
        else if (position == -2){
            return Color.BLUE;
        }
        else if (position == -1){
            return R.color.black;
        }
        else if (position == 0){
            return Color.rgb(FloatRGB(1.0),FloatRGB(0.5),FloatRGB(0.0));
        }
        else if (position == 1){
            return Color.argb(255,FloatRGB(0.98),FloatRGB(0.90),FloatRGB(0.44));
        }
        else if (position == 2){
            return Color.rgb(FloatRGB(0.52),FloatRGB(0.65),FloatRGB(0.92));
        }
        else if (position == 3){
            return Color.rgb(FloatRGB(0.93),FloatRGB(0.43),FloatRGB(0.47));
        }
        else if (position == 4){
            return Color.rgb(FloatRGB(0.74),FloatRGB(0.58),FloatRGB(0.91));
        }
        else if (position == 5){
            return Color.rgb(FloatRGB(0.42),FloatRGB(0.86),FloatRGB(0.87));
        }
        else if (position == 6){
            return Color.rgb(FloatRGB(0.99),FloatRGB(0.84),FloatRGB(0.71));
        }
        else if (position == 7){
            return Color.rgb(FloatRGB(0.49),FloatRGB(0.77),FloatRGB(0.53));
        }
        else if (position == 8){
            return Color.rgb(FloatRGB(0.96),FloatRGB(0.64),FloatRGB(0.44));
        }
        else if (position == 9){
            return Color.rgb(FloatRGB(0.65),FloatRGB(0.92),FloatRGB(0.44));
        }
        else if (position == 10){
            return Color.rgb(FloatRGB(0.76),FloatRGB(0.32),FloatRGB(0.53));
        }
        else if (position == 11){
            return Color.rgb(FloatRGB(0.93),FloatRGB(0.71),FloatRGB(0.75));
        }
        else if (position == 12){
            return Color.rgb(FloatRGB(0.85),FloatRGB(0.88),FloatRGB(0.99));
        }
        else if (position == 13){
            return Color.rgb(FloatRGB(0.56),FloatRGB(0.83),FloatRGB(0.97));
        }
        else if (position == 14){
            return Color.rgb(FloatRGB(0.19),FloatRGB(0.75),FloatRGB(0.87));
        }
        else if (position == 15){
            return Color.rgb(FloatRGB(0.80),FloatRGB(1.00),FloatRGB(1.00));
        }
        else if (position == 16){
            return Color.rgb(FloatRGB(0.44),FloatRGB(0.88),FloatRGB(0.59));
        }
        else if (position == 17){
            return Color.rgb(FloatRGB(0.91),FloatRGB(0.81),FloatRGB(0.21));
        }
        else if (position == 18){
            return Color.rgb(FloatRGB(0.75),FloatRGB(1.00),FloatRGB(0.90));
        }
        else if (position == 19){
            return Color.rgb(FloatRGB(0.04),FloatRGB(0.59),FloatRGB(0.61));
        }
        else if (position == 20){
            return Color.rgb(FloatRGB(0.59),FloatRGB(0.42),FloatRGB(1.00));
        }
        else if (position == 21){
            return Color.rgb(FloatRGB(0.95),FloatRGB(0.56),FloatRGB(0.38));
        }
        else if (position == 22){
            return Color.rgb(FloatRGB(0.96),FloatRGB(0.85),FloatRGB(0.42));
        }
        else if (position == 23){
            return Color.rgb(FloatRGB(0.94),FloatRGB(0.76),FloatRGB(0.63));
        }
        else if (position == 24){
            return Color.rgb(FloatRGB(0.67),FloatRGB(0.87),FloatRGB(0.83));
        }
        else if (position == 25){
            return Color.rgb(FloatRGB(1.00),FloatRGB(0.54),FloatRGB(0.71));
        }
        else if (position > 100){
            return Color.WHITE;
        }
        else {
            return Color.rgb(FloatRGB(0.73),FloatRGB(0.73),FloatRGB(0.73));
        }
    }

    public static int getPositionColorRGB(int position){

        if (position == 0){
            return Color.rgb(FloatRGB(0.73),FloatRGB(0.73),FloatRGB(0.73));
        }
        else if (position == 1){
            return Color.rgb(FloatRGB(0.98),FloatRGB(0.90),FloatRGB(0.44));
        }
        else if (position == 2){
            return Color.rgb(FloatRGB(0.52),FloatRGB(0.65),FloatRGB(0.92));
        }
        else if (position == 3){
            return Color.rgb(FloatRGB(0.93),FloatRGB(0.43),FloatRGB(0.47));
        }
        else if (position == 4){
            return Color.rgb(FloatRGB(0.74),FloatRGB(0.58),FloatRGB(0.91));
        }
        else if (position == 5){
            return Color.rgb(FloatRGB(0.42),FloatRGB(0.86),FloatRGB(0.87));
        }
        else if (position == 6){
            return Color.rgb(FloatRGB(0.99),FloatRGB(0.84),FloatRGB(0.71));
        }
        else if (position == 7){
            return Color.rgb(FloatRGB(0.49),FloatRGB(0.77),FloatRGB(0.53));
        }
        else if (position == 8){
            return Color.rgb(FloatRGB(0.96),FloatRGB(0.64),FloatRGB(0.44));
        }
        else if (position == 9){
            return Color.rgb(FloatRGB(0.65),FloatRGB(0.92),FloatRGB(0.44));
        }
        else if (position == 10){
            return Color.rgb(FloatRGB(0.76),FloatRGB(0.32),FloatRGB(0.53));
        }
        else if (position == 11){
            return Color.rgb(FloatRGB(0.93),FloatRGB(0.71),FloatRGB(0.75));
        }
        else if (position == 12){
            return Color.rgb(FloatRGB(0.85),FloatRGB(0.88),FloatRGB(0.99));
        }
        else if (position == 13){
            return Color.rgb(FloatRGB(0.56),FloatRGB(0.83),FloatRGB(0.97));
        }
        else if (position == 14){
            return Color.rgb(FloatRGB(0.19),FloatRGB(0.75),FloatRGB(0.87));
        }
        else if (position == 15){
            return Color.rgb(FloatRGB(0.80),FloatRGB(1.00),FloatRGB(1.00));
        }
        else if (position == 16){
            return Color.rgb(FloatRGB(0.44),FloatRGB(0.88),FloatRGB(0.59));
        }
        else if (position == 17){
            return Color.rgb(FloatRGB(0.91),FloatRGB(0.81),FloatRGB(0.21));
        }
        else if (position == 18){
            return Color.rgb(FloatRGB(0.75),FloatRGB(1.00),FloatRGB(0.90));
        }
        else if (position == 19){
            return Color.rgb(FloatRGB(0.04),FloatRGB(0.59),FloatRGB(0.61));
        }
        else if (position == 20){
            return Color.rgb(FloatRGB(0.59),FloatRGB(0.42),FloatRGB(1.00));
        }
        else if (position == 21){
            return Color.rgb(FloatRGB(0.95),FloatRGB(0.56),FloatRGB(0.38));
        }
        else if (position == 22){
            return Color.rgb(FloatRGB(0.96),FloatRGB(0.85),FloatRGB(0.42));
        }
        else if (position == 23){
            return Color.rgb(FloatRGB(0.94),FloatRGB(0.76),FloatRGB(0.63));
        }
        else if (position == 24){
            return Color.rgb(FloatRGB(0.67),FloatRGB(0.87),FloatRGB(0.83));
        }
        else if (position == 25){
            return Color.rgb(FloatRGB(1.00),FloatRGB(0.54),FloatRGB(0.71));
        }else {
            return Color.rgb(FloatRGB(0.73),FloatRGB(0.73),FloatRGB(0.73));
        }
    }

    public static String getTimeDuration(Date endTime , Date startTime){
        long diff = endTime.getTime() - startTime.getTime();
        int diffhours = (int) (diff / (60 * 60 * 1000));
        int Minute = (int) diff / (60 * 1000) % 60;
        return  (String.format("%02d", diffhours)+":"+String.format("%02d", Minute));
    }

    public static String getDecimalTimeDuration(Date endTime , Date startTime)throws Exception{
        DecimalFormat decimalFormat = new DecimalFormat("0.00");
        long diff = endTime.getTime() - startTime.getTime();
        int Minute = (int) diff / (60 * 1000) % 60;
        int totalHr = (int) (diff / (60 * 60 * 1000));
        double finalminute = Minute/60.0;
        double total = totalHr +finalminute;
        return  decimalFormat.format(total);
    }

    public static double getDecimalTimeDuration2(Date endTime , Date startTime)throws Exception{
        DecimalFormat decimalFormat = new DecimalFormat("0.00");
        long diff = endTime.getTime() - startTime.getTime();
        int Minute = (int) diff / (60 * 1000) % 60;
        int totalHr = (int) (diff / (60 * 60 * 1000));
        double finalminute = Minute/60.0;
        double total = totalHr +finalminute;
        try {
            total = Double.parseDouble( decimalFormat.format(total));
        }catch (Exception e){
            e.printStackTrace();
        }
        return total;
    }


    public static void SetStatusBarColor(Activity activity,int Color) {
        Window window = activity.getWindow();
        window.setStatusBarColor(ContextCompat
                .getColor(activity,Color));
    }
}
