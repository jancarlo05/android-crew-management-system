package com.procuro.crewu.Profile;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.procuro.apimmdatamanagerlib.OnCompleteListeners;
import com.procuro.apimmdatamanagerlib.User;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;
import com.procuro.crewu.CrewUDataManager;
import com.procuro.crewu.Pref;
import com.procuro.crewu.R;
import com.procuro.crewu.Tools;

public class UserResetPasswordActivity extends AppCompatActivity {

    User user;
    TextView userName, password_match_message;
    Button back , save;
    EditText new_password,confirm_password;
    LinearLayout root;

    aPimmDataManager dataManager = aPimmDataManager.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_reset_password);

        userName = findViewById(R.id.username);
        back = findViewById(R.id.home);
        save = findViewById(R.id.save);

        new_password = findViewById(R.id.new_password);
        confirm_password = findViewById(R.id.confirm_password);
        root = findViewById(R.id.root);
        password_match_message = findViewById(R.id.password_match_message);

        setUpOnclicks();

        getArgument();

    }
    private void setUpOnclicks(){
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        new_password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length()>0){
                    confirm_password.setEnabled(true);
                    confirm_password.setAlpha(1);

                }else {
                    confirm_password.setEnabled(false);
                    confirm_password.setAlpha(.5f);
                    password_match_message.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        confirm_password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length()>0){
                    password_match_message.setVisibility(View.VISIBLE);
                    if (s.toString().equalsIgnoreCase(new_password.getText().toString())){
                        password_match_message.setText("Password Match");
                        password_match_message.setTextColor(ContextCompat.getColor(UserResetPasswordActivity.this,R.color.green));

                        save.setAlpha(1);
                        save.setEnabled(true);


                    }else {
                        password_match_message.setText("Password Not Match");
                        password_match_message.setTextColor(ContextCompat.getColor(UserResetPasswordActivity.this,R.color.red));

                        save.setAlpha(.5f);
                        save.setEnabled(false);

                    }

                }else {
                    password_match_message.setVisibility(View.INVISIBLE);


                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


    }

    private void getArgument(){
        this.user = CrewUDataManager.getInstance().getUser();
        userName.setText(Tools.CheckString(user.getFullName(),"No User Selected"));
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SaveUpdate();
            }
        });
    }


    private void SaveUpdate(){
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Please Wait");
        progressDialog.show();

        try {
            UpdateBasicInfo(user,progressDialog);

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void UpdateBasicInfo( final User user, final ProgressDialog progressDialog){
        user.password = confirm_password.getText().toString();
        progressDialog.setMessage("Saving Update");

        try {
            dataManager.updateUserRecordWithUserId(user, new OnCompleteListeners.updateUserRecordWithUserIdListener() {
                @Override
                public void updateUserRecordWithUserIdCallback(Boolean status, Error error) {
                    if (error == null){
                        Tools.DisplaySnackbar("Password Saved",root);
                        new_password.setText("");
                        confirm_password.setText("");
                        password_match_message.setVisibility(View.INVISIBLE);
                        Tools.DisableView(save,.5f);
                        Pref.getInstance().setSharedpref();
                    }else {
                        Tools.DisplaySnackbar("Reset Password Failed",root);

                    }progressDialog.dismiss();
                }
            });
        }catch (Exception e){
            e.printStackTrace();
            Tools.DisplaySnackbar("Reset Password Failed",root);
            progressDialog.dismiss();
        }

    }



}
