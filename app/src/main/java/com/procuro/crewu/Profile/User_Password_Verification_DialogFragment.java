package com.procuro.crewu.Profile;

import android.app.ActivityOptions;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.DialogFragment;

import com.procuro.apimmdatamanagerlib.User;
import com.procuro.crewu.CrewUDataManager;
import com.procuro.crewu.R;
import com.procuro.crewu.Tools;

import java.util.Objects;

public class User_Password_Verification_DialogFragment extends DialogFragment {

    public  static User user;
    private EditText editText;
    private TextView password_indicator;
    private Button cancel,verify;

    public static User_Password_Verification_DialogFragment newInstance() {
        return new User_Password_Verification_DialogFragment();
    }
    @Override
        public int getTheme() {
        return R.style.slide_up_down;
        }

    @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.user_profile_password_verification_popup, container, false);

//                    int height = (int)(getResources().getDisplayMetrics().heightPixels*0.90);
//                    int width = 320;
//
        int width = (int)(getResources().getDisplayMetrics().widthPixels*0.90);
        int height = LinearLayout.LayoutParams.MATCH_PARENT;


        Objects.requireNonNull(getDialog().getWindow()).setLayout(width, height);
        getDialog().setCanceledOnTouchOutside(false);
        getDialog().getWindow().setGravity(Gravity.CENTER);
        view.setFocusableInTouchMode(true);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

         cancel = view.findViewById(R.id.cancel);
         verify = view.findViewById(R.id.verify);
         editText = view.findViewById(R.id.password);
         password_indicator = view.findViewById(R.id.password_indicator);

         setUPOnclicks();


        return view; }

    private static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }


    private void setUPOnclicks(){

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                VerifyPassword();
            }
        });

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length()>0){
                    Tools.EnableView(verify,1);
                }
                else {
                    Tools.DisableView(verify,.5f);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }


    private void VerifyPassword(){
        User currentUser = CrewUDataManager.getInstance().getUser();
        if (editText.getText().toString().equalsIgnoreCase(currentUser.password)){
            Intent intent = new Intent(getContext(), UserResetPasswordActivity.class);
            getActivity().startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(getActivity()).toBundle());
            dismiss();
        }else {
            password_indicator.setVisibility(View.VISIBLE);
        }
    }

}
