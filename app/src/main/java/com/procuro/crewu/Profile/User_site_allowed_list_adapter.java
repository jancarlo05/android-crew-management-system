package com.procuro.crewu.Profile;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import com.procuro.apimmdatamanagerlib.Site;
import com.procuro.crewu.CrewUDataManager;
import com.procuro.crewu.R;

import java.util.ArrayList;

public class User_site_allowed_list_adapter extends BaseAdapter {

    // Declare Variables

    private Context mContext;
    private LayoutInflater inflater;
    private ArrayList<CustomSite>customSites;
    private boolean EnableEdit ;


    public User_site_allowed_list_adapter(Context context, ArrayList<CustomSite> arraylist, boolean EnableEdit) {
        mContext = context;
        inflater = LayoutInflater.from(mContext);
        this.customSites = arraylist;
        this.EnableEdit =  EnableEdit;
    }

    public class ViewHolder {
        TextView name;
        CheckBox checkBox;
    }

    @Override
    public int getCount() {
        return customSites.size();
    }

    @Override
    public Object getItem(int position) {
        return customSites.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("ViewHolder")
    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        holder = new ViewHolder();
        view = inflater.inflate(R.layout.user_sitelist_data, null);
        view.setTag(holder);

        final CustomSite site = customSites.get(position);

        holder.checkBox = view.findViewById(R.id.checkbox);
        holder.name = view.findViewById(R.id.name);

        holder.name.setText(site.getSite().sitename);

        if (site.isTempValue()){
            holder.checkBox.setChecked(true);
        }else {
            holder.checkBox.setChecked(false);
        }

        if (!EnableEdit){
            holder.checkBox.setEnabled(false);
            view.setEnabled(false);
        }else {
            view.setEnabled(true);
            holder.checkBox.setEnabled(true);
        }

        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                site.setTempValue(isChecked);
                site.setUpdated(true);
            }
        });
        return view;
    }

}

