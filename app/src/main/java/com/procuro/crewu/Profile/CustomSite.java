package com.procuro.crewu.Profile;

import com.procuro.apimmdatamanagerlib.Site;

public class CustomSite {

    private Site site;
    private boolean currentSelected;
    private boolean tempValue;
    private boolean isUpdated;

    public boolean isUpdated() {
        return isUpdated;
    }

    public void setUpdated(boolean updated) {
        isUpdated = updated;
    }

    public Site getSite() {
        return site;
    }

    public void setSite(Site site) {
        this.site = site;
    }

    public boolean isCurrentSelected() {
        return currentSelected;
    }

    public void setCurrentSelected(boolean currentSelected) {
        this.currentSelected = currentSelected;
    }

    public boolean isTempValue() {
        return tempValue;
    }

    public void setTempValue(boolean tempValue) {
        this.tempValue = tempValue;
    }
}
