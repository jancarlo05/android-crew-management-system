package com.procuro.crewu.Profile;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.service.autofill.UserData;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.bumptech.glide.Glide;
import com.procuro.apimmdatamanagerlib.CertificationDefinition;
import com.procuro.apimmdatamanagerlib.OnCompleteListeners;
import com.procuro.apimmdatamanagerlib.Site;
import com.procuro.apimmdatamanagerlib.User;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;
import com.procuro.crewu.CrewUDataManager;
import com.procuro.crewu.Pref;
import com.procuro.crewu.R;
import com.procuro.crewu.Tools;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Objects;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;
import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static androidx.constraintlayout.widget.Constraints.TAG;
import static com.procuro.crewu.Tools.DisableView;
import static com.procuro.crewu.Tools.DisplaySnackbar;
import static com.procuro.crewu.Tools.ValidString;


public class User_Profile_Overall_Info_Fragment extends Fragment {



    public static TextView birthdate,state,permissions,pin,fullname,userRole,
            carrier,hiredate,reviewdate;
    private View view;

    public static EditText firstname,lastname,username,employee_id,email,city,address,mobile,zipcode,password;

    public static LinearLayout root;
    private ConstraintLayout bod_container,hiredate_container,reviewDate_container,
            stateContainer,permissionContainer,carrierContainer ;

    private RecyclerView recyclerView;
    private info.hoang8f.android.segmented.SegmentedGroup infgroup;
    private RadioButton opsSkill,positionSkill,productSkill,mgmtskill;
    private ConstraintLayout siteAllowed_Container;
    public static TextView siteAllowed;
    private String siteTitle = "" ;
    private boolean EnableEdit;
    private User user;

    private Button resetPassword,save;
    private aPimmDataManager dataManager = aPimmDataManager.getInstance();
    private CircleImageView imageView9 ;
    private String imageTitle;

    public User_Profile_Overall_Info_Fragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.user_personal_overall_info_fragment, container, false);

        firstname = view.findViewById(R.id.firstname);
        lastname = view.findViewById(R.id.lastname);
        username = view.findViewById(R.id.username);
        employee_id = view.findViewById(R.id.employee_id);
        email = view.findViewById(R.id.email);
        birthdate = view.findViewById(R.id.birthdate);
        pin = view.findViewById(R.id.personal_id_number);
        address = view.findViewById(R.id.address);
        city = view.findViewById(R.id.city);
        state = view.findViewById(R.id.state);
        zipcode = view.findViewById(R.id.zipcode);
        mobile = view.findViewById(R.id.mobile);
        carrier = view.findViewById(R.id.carrier);
        hiredate = view.findViewById(R.id.hire_date);
        reviewdate = view.findViewById(R.id.review_date);
        bod_container = view.findViewById(R.id.bod_container);
        hiredate_container = view.findViewById(R.id.hire_date_container);
        reviewDate_container = view.findViewById(R.id.review_date_container);
        stateContainer = view.findViewById(R.id.state_container);
        carrierContainer = view.findViewById(R.id.carrier_container);
        fullname = view.findViewById(R.id.fullname);
        userRole = view.findViewById(R.id.userRole);
        root = view.findViewById(R.id.root);

        mgmtskill = view.findViewById(R.id.mgmtskill);
        recyclerView = view.findViewById(R.id.recyclerView);
        infgroup = view.findViewById(R.id.segmented2);
        opsSkill = view.findViewById(R.id.opsSkill);
        positionSkill = view.findViewById(R.id.position_skills);
        productSkill = view.findViewById(R.id.product_skills);
        siteAllowed_Container = view.findViewById(R.id.site_allowed_container);
        siteAllowed = view.findViewById(R.id.site_allowed);
        resetPassword = view.findViewById(R.id.resetPassword);
        save = view.findViewById(R.id.save);
        imageView9 = view.findViewById(R.id.imageView9);


        User user = CrewUDataManager.getInstance().getUser();
        if (user!=null){
            DisplayData(user);
        }

        setUpOnlicks();

        DisplayProfileImage();

        return view;

    }

    private void DisplayProfileImage(){
        try {
            CrewUDataManager data = CrewUDataManager.getInstance();
            if (data.getProfilePath()!=null){
                File file = new File(CrewUDataManager.getInstance().getProfilePath());
                if (file.exists()){
                    Glide.with(getContext()).load(file).into(imageView9);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void getCurrentUserRole(User currentUser){
        if (currentUser!=null){
            if (currentUser.roles!=null){
                ArrayList<CustomRole>customRoles = CustomRole.getAllCustomRole(currentUser.roles);

                if (customRoles.size()>=1){
                    CustomRole customRole = customRoles.get(0);
                    userRole.setText(customRole.getName());

                    userRole.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            DisplayCarrier(getContext(),userRole,"Select Title",userRole);
                        }
                    });

                    if (customRole.getStatus()>4){
                        EnableEdit = false;
                        DisableView(siteAllowed_Container,.7f);
                    }else {
                        EnableEdit = true;

                    }
                }else {
                    userRole.setText("");
                }
            }

            DisableView(hiredate_container,1);
            DisableView(reviewDate_container,1);
            DisableView(siteAllowed_Container,1);
            DisableView(userRole,1);
            DisableView(username,1);
        }

    }

    private void DisplayCHangePasswordPopup(){
        DialogFragment newFragment = User_Password_Verification_DialogFragment.newInstance();
        assert getActivity().getFragmentManager() != null;
        newFragment.show(Objects.requireNonNull(this).getActivity().getSupportFragmentManager(), "dialog");
    }

    private void setUpOnlicks(){

        imageView9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage(getContext());
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SaveUpdate();
            }
        });

        resetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplayCHangePasswordPopup();
            }
        });

        infgroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == opsSkill.getId()){
                    DisplaypOpsSkills();
                }else if (checkedId == positionSkill.getId()){
                    DisplaypositionSkills();

                }else if (checkedId == productSkill.getId()){
                    DisplaypProductSkills();
                }
                else if (checkedId == mgmtskill.getId()){
                    DisplayManagementSkills();
                }
            }
        });

        positionSkill.setChecked(true);

        bod_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplayDatePicker(getContext(),bod_container,"bod",Gravity.BOTTOM,birthdate);
            }
        });

        reviewDate_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplayDatePicker(getContext(),reviewDate_container,"reviewDate",Gravity.TOP,reviewdate);
            }
        });

        hiredate_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplayDatePicker(getContext(),hiredate_container,"hiredate",Gravity.TOP,hiredate);
            }
        });

        pin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplayNumpad(getContext(),pin,pin);
            }
        });

        carrierContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplayCarrier(getContext(),carrierContainer,"Select Carrier",carrier);
            }
        });

        stateContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplayCarrier(getContext(),stateContainer,"Select State",state);

            }
        });

//        permissionContainer.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                DisplayCarrier(getContext(),permissionContainer,"Select Permission",permissions);
//            }
//        });

        siteAllowed_Container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplaySiteAllowedPopup(getContext(),siteAllowed_Container);
            }
        });

    }

    private void DisplayData(User user){
        this.user =user;
        getCurrentUserRole(user);
        DisableView(save,.5f);

        SimpleDateFormat format = new SimpleDateFormat("MMM/dd/yyyy");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {
            UserProfileData data = UserProfileData.getInstance();
            data.InitializeSaveButton(save);

            String userFullname = user.getFullName();

            fullname.setText(Tools.CheckString(userFullname,"--"));

            if (data.getFirstName()!=null){
                firstname.setText(Tools.CheckString(data.getFirstName(),""));

            }else {
                firstname.setText(Tools.CheckString(user.firstName,""));
            }


            if (data.getLastName()!=null){
                lastname.setText(Tools.CheckString(data.getLastName(),""));
            }else {
                lastname.setText(Tools.CheckString(user.lastName,""));
            }


            if (data.getUsername()!=null){
                username.setText(Tools.CheckString(data.getUsername(),""));
            }else {
                username.setText(Tools.CheckString(user.username,""));
            }

            if (data.getEmployeeID()!=null){
                employee_id.setText(Tools.CheckString(data.getEmployeeID(),""));
            }else {
                employee_id.setText(Tools.CheckString(user.employeeID,""));
            }

            if (data.getContactEmail()!=null){
                employee_id.setText(Tools.CheckString(data.getContactEmail(),""));
            }else {
                email.setText(Tools.CheckString(user.contactEmail,""));
            }

            if (data.getpID()!=null){
                pin.setText(Tools.CheckString(data.getpID(),""));
            }else {
                pin.setText(Tools.CheckString(user.pID,""));
            }

            if (data.getCity()!=null){
                city.setText(Tools.CheckString(data.getCity(),""));
            }else {
                city.setText(Tools.CheckString(user.city,""));
            }

            if (data.getState()!=null){
                state.setText(Tools.CheckString(data.getState(),""));
            }else {
                state.setText(Tools.CheckString(user.state,""));
            }

            if (data.getPostal()!=null){
                zipcode.setText(Tools.CheckString(data.getPostal(),""));
            }else {
                zipcode.setText(Tools.CheckString(user.postal,""));
            }
            if (data.getDayPhone()!=null){
                mobile.setText(Tools.CheckString(data.getDayPhone(),""));
            }else {
                mobile.setText(Tools.CheckString(user.dayPhone,""));
            }
            if (data.getMobileCarrier()!=null){
                carrier.setText(Tools.CheckString(data.getMobileCarrier(),""));
            }else {
                carrier.setText(Tools.CheckString(user.mobileCarrier,""));
            }
            if (data.getAddress1()!=null){
                address.setText(Tools.CheckString(data.getAddress1(),""));
            }else {
                address.setText(Tools.CheckString(user.address1,""));
            }

            if (data.getCustomSites()!=null){
                siteAllowed.setText(Tools.CheckString(getUpdatedSiteAllowed(),""));
            }else {
                siteAllowed.setText(Tools.CheckString(getSitesAllowed(user),""));
            }

            if (data.getDOB()!=null){
                birthdate.setText(Tools.CheckString(format.format(data.getDOB()),""));
            }else {
                if (user.DOB != null){
                    birthdate.setText(format.format(user.DOB));
                }
            }

            if (data.getHireDate()!=null){
                hiredate.setText(Tools.CheckString(format.format(data.getHireDate()),""));
            }else {
                if (user.hireDate != null){
                    hiredate.setText(format.format(user.hireDate));
                }
            }

            if (data.getReviewDate()!=null){
                reviewdate.setText(Tools.CheckString(format.format(data.getReviewDate()),""));
            }else {
                if (user.reviewDate != null){
                    reviewdate.setText(format.format(user.reviewDate));
                }
            }

            addTextWatcher(firstname,"firstname");
            addTextWatcher(lastname,"lastname");
            addTextWatcher(username,"username");
            addTextWatcher(email,"email");
            addTextWatcher(address,"address");
            addTextWatcher(city,"city");
            addTextWatcher(zipcode,"zip");
            addTextWatcher(employee_id,"employeeID");
            addTextWatcher(mobile,"mobile");

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private String getSitesAllowed(User user){
        CrewUDataManager crewUDataManager = CrewUDataManager.getInstance();
        if (crewUDataManager.getSites()!=null){
            if (user.roles!=null){
                for (String role : user.roles){
                    for (Site site : crewUDataManager.getSites()){
                        if (site.sitename.equalsIgnoreCase(role)){
                            return role;
                        }
                    }
                }
            }
        }

        return "";
    }


    private void DisplaypositionSkills(){
        //CATEGORY 1
        DisplayData(getSKillsByCategory("0"));
    }

    private void DisplaypProductSkills(){
        //CATEGORY 1
        DisplayData(getSKillsByCategory("1"));
    }

    private void DisplaypOpsSkills(){
        //CATEGORY 2
        DisplayData(getSKillsByCategory("2"));

    }

    private void DisplayManagementSkills(){
        //CATEGORY 3
        DisplayData(getSKillsByCategory("3"));
    }

    private ArrayList<CertificationDefinition> getSKillsByCategory(String category){
        ArrayList<CertificationDefinition> skills = new ArrayList<>();
        ArrayList<CertificationDefinition>certificationDefinitions = CrewUDataManager.getInstance().getCertificationDefinitions();

        for (CertificationDefinition certificationDefinition : certificationDefinitions){
            if (certificationDefinition.category.equalsIgnoreCase(category)){
                skills.add(certificationDefinition);
            }
        }
        return  skills;
    }

    private void DisplayData(ArrayList<CertificationDefinition> skills){
        User_Skills_RecyclerViewAdapter recyclerViewAdapter  = new User_Skills_RecyclerViewAdapter(getContext(),skills,EnableEdit,user);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(),4));
        recyclerView.setAdapter(recyclerViewAdapter);
    }

    public void DisplaySiteAllowedPopup(final Context context, View anchor) {

        final SimpleTooltip tooltip = new SimpleTooltip.Builder(context)
                .anchorView(anchor)
                .gravity(Gravity.TOP)
                .dismissOnOutsideTouch(true)
                .dismissOnInsideTouch(false)
                .showArrow(true)
                .margin(R.dimen.simpletooltip_margin)
                .padding(R.dimen.simpletooltip_padding)
                .transparentOverlay(true)
                .arrowColor(ContextCompat.getColor(context, R.color.dirty_white))
                .contentView(R.layout.site_allowed_layout)
                .focusable(true)
                .build();
        tooltip.show();

        ListView listView = tooltip.findViewById(R.id.listView);
        Button button = tooltip.findViewById(R.id.apply);

        ArrayList<CustomSite>customSites = new ArrayList<>();

        CrewUDataManager crewUDataManager = CrewUDataManager.getInstance();
        UserProfileData userProfileData =  UserProfileData.getInstance();
        User user = CrewUDataManager.getInstance().getUser();

        if (userProfileData.getCustomSites()==null){
            if (crewUDataManager.getSites()!=null){
                for (Site site : crewUDataManager.getSites()){
                    boolean currentSelected = false;
                    if (user.roles!=null){
                        if (user.roles.contains(site.sitename)){
                            currentSelected = true;
                        }
                    }
                    CustomSite customSite = new CustomSite();
                    customSite.setSite(site);
                    customSite.setCurrentSelected(currentSelected);
                    customSite.setTempValue(currentSelected);
                    customSites.add(customSite);
                }
            }
            userProfileData.setCustomSites(customSites);
        }else {
            customSites = userProfileData.getCustomSites();
        }

        User_site_allowed_list_adapter adapter = new User_site_allowed_list_adapter(getContext(),customSites,EnableEdit);
        listView.setAdapter(adapter);

        try {
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    siteAllowed.setText(getUpdatedSiteAllowed());
                    tooltip.dismiss();
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private String getUpdatedSiteAllowed(){
        for (CustomSite customSite : UserProfileData.getInstance().getCustomSites()){
            if (customSite.isTempValue()){
                return customSite.getSite().sitename;
            }
        }
        return "";
    }


    public void DisplayDatePicker(final Context context, View anchor, final String title, int gravity, final TextView textView) {

        final SimpleTooltip tooltip = new SimpleTooltip.Builder(context)
                .anchorView(anchor)
                .gravity(gravity)
                .dismissOnOutsideTouch(true)
                .dismissOnInsideTouch(false)
                .showArrow(true)
                .arrowColor(ContextCompat.getColor(context,R.color.orange))
                .transparentOverlay(true)
                .contentView(R.layout.date_picker)
                .focusable(true)
                .build();
        tooltip.show();


        final DatePicker datePicker =tooltip.findViewById(R.id.datepicker);
        Button accept = tooltip.findViewById(R.id.apply);


        final SimpleDateFormat dateFormat = new SimpleDateFormat("MMM/dd/yyyy");


        datePicker.setMaxDate(System.currentTimeMillis());
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar date = Calendar.getInstance();

                date.set(datePicker.getYear(), datePicker.getMonth(), datePicker.getDayOfMonth());

                if (title.equalsIgnoreCase("BOD")){
                    UserProfileData.getInstance().setDOB(removeTime(date.getTime()));
                }
                else if (title.equalsIgnoreCase("hiredate")){
                    UserProfileData.getInstance().setHireDate(removeTime(date.getTime()));
                }
                else if (title.equalsIgnoreCase("reviewDate")){
                    UserProfileData.getInstance().setReviewDate(removeTime(date.getTime()));
                }

                textView.setText(dateFormat.format(date.getTime()));
                UserProfileData.getInstance().setHasUpdate(true);
                tooltip.dismiss();
            }
        });

    }
    private  Date removeTime(Date date) {
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    public void DisplayNumpad(final Context context, View anchor, final TextView textView) {

        final SimpleTooltip tooltip = new SimpleTooltip.Builder(context)
                .anchorView(anchor)
                .gravity(Gravity.BOTTOM)
                .dismissOnOutsideTouch(true)
                .dismissOnInsideTouch(false)
                .showArrow(true)
                .arrowColor(ContextCompat.getColor(context,R.color.dirty_white))
                .transparentOverlay(true)
                .contentView(R.layout.pin_number_register)
                .focusable(true)
                .build();
        tooltip.show();


        final TextView name,ans1,ans2,ans3,ans4,num1,num2,num3,num4,num5,num6,num7,num8,num9,num0,delete;

        ans1 = tooltip.findViewById(R.id.ans1);
        ans2 = tooltip.findViewById(R.id.ans2);
        ans3 = tooltip.findViewById(R.id.ans3);
        ans4 = tooltip.findViewById(R.id.ans4);
        num0 = tooltip.findViewById(R.id.num0);
        num1 = tooltip.findViewById(R.id.num1);
        num2 = tooltip.findViewById(R.id.num2);
        num3 = tooltip.findViewById(R.id.num3);
        num4 = tooltip.findViewById(R.id.num4);
        num5 = tooltip.findViewById(R.id.num5);
        num6 = tooltip.findViewById(R.id.num6);
        num7 = tooltip.findViewById(R.id.num7);
        num8 = tooltip.findViewById(R.id.num8);
        num9 = tooltip.findViewById(R.id.num9);
        delete = tooltip.findViewById(R.id.delete);

        final StringBuilder answer = new StringBuilder();


        try {

            num1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (answer.length()<4){
                        answer.append("1");
                        populateStringInBox(ans1,ans2,ans3,ans4,answer.toString(),tooltip,textView);
                    }
                }
            });

            num2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (answer.length()<4){
                        answer.append("2");
                        populateStringInBox(ans1,ans2,ans3,ans4,answer.toString(),tooltip,textView);
                    }
                }
            });

            num3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (answer.length()<4){
                        answer.append("3");
                        populateStringInBox(ans1,ans2,ans3,ans4,answer.toString(),tooltip,textView);
                    }
                }
            });

            num4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (answer.length()<4){
                        answer.append("4");
                        populateStringInBox(ans1,ans2,ans3,ans4,answer.toString(),tooltip,textView);
                    }
                }
            });

            num5.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (answer.length()<4){
                        answer.append("5");
                        populateStringInBox(ans1,ans2,ans3,ans4,answer.toString(),tooltip,textView);
                    }
                }
            });

            num6.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (answer.length()<4){
                        answer.append("6");
                        populateStringInBox(ans1,ans2,ans3,ans4,answer.toString(),tooltip,textView);
                    }
                }
            });

            num7.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (answer.length()<4){
                        answer.append("7");
                        populateStringInBox(ans1,ans2,ans3,ans4,answer.toString(),tooltip,textView);
                    }
                }
            });

            num8.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (answer.length()<4){
                        answer.append("8");
                        populateStringInBox(ans1,ans2,ans3,ans4,answer.toString(),tooltip,textView);
                    }
                }
            });

            num9.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (answer.length()<4){
                        answer.append("9");
                        populateStringInBox(ans1,ans2,ans3,ans4,answer.toString(),tooltip,textView);
                    }
                }
            });

            num0.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (answer.length()<4){
                        answer.append("0");
                        populateStringInBox(ans1,ans2,ans3,ans4,answer.toString(),tooltip,textView);
                    }
                }
            });

            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (answer.toString().length()>0){
                        answer.setLength(answer.length() - 1);
                        populateStringInBox(ans1,ans2,ans3,ans4,answer.toString(),tooltip,textView);
                    }
                }
            });


        }catch (Exception e){
            e.printStackTrace();
        }



    }

    private void populateStringInBox(TextView ans1,TextView ans2,TextView ans3,
                                     TextView ans4,String answer,SimpleTooltip tooltip,TextView pim){
        try {
            ans1.setBackgroundResource(R.drawable.empty_circle_black);
            ans2.setBackgroundResource(R.drawable.empty_circle_black);
            ans3.setBackgroundResource(R.drawable.empty_circle_black);
            ans4.setBackgroundResource(R.drawable.empty_circle_black);


            if (answer.length()>0){
                for (int i = 0; i <answer.length() ; i++) {
                    if (i == 0){
                        ans1.setBackgroundResource(R.drawable.full_circle_black);
                    }else if (i == 1){
                        ans2.setBackgroundResource(R.drawable.full_circle_black);
                    }else if (i == 2){
                        ans3.setBackgroundResource(R.drawable.full_circle_black);
                    }else if (i == 3){
                        ans4.setBackgroundResource(R.drawable.full_circle_black);
                    }
                }
            }

            if (answer.length()==4){
                pin.setText(answer);
                UserProfileData.getInstance().setHasUpdate(true);
                UserProfileData.getInstance().setpID(answer);
                tooltip.dismiss();
            }


        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public void DisplayCarrier(final Context context, View anchor, final String title, final TextView textView) {

        final SimpleTooltip tooltip = new SimpleTooltip.Builder(context)
                .anchorView(anchor)
                .gravity(Gravity.BOTTOM)
                .dismissOnOutsideTouch(true)
                .dismissOnInsideTouch(false)
                .showArrow(true)
                .arrowColor(ContextCompat.getColor(context,R.color.white))
                .transparentOverlay(true)
                .contentView(R.layout.custom_picker)
                .focusable(true)
                .build();
        tooltip.show();


        final NumberPicker picker =tooltip.findViewById(R.id.picker);
        TextView pickertitle = tooltip.findViewById(R.id.picker_title);
        Button accept = tooltip.findViewById(R.id.apply);
        pickertitle.setText(title);


        if (title.toLowerCase().contains("Carrier".toLowerCase())){

            final String[] values= {"AT&T","T-Mobile", "Verizon"};

            picker.setDisplayedValues(values);
            picker.setMinValue(0);
            picker.setMaxValue(values.length-1);
            picker.setWrapSelectorWheel(true);
            picker.setValue(values.length/2);


            accept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    UserProfileData.getInstance().setHasUpdate(true);
                    UserProfileData.getInstance().setMobileCarrier(values[picker.getValue()]);
                    textView.setText(values[picker.getValue()]);
                    tooltip.dismiss();
                }
            });



        }else if (title.toLowerCase().contains("State".toLowerCase())){
            final String[] countries = CrewUDataManager.getInstance().getCountries();
            final String[] countrycode = CrewUDataManager.getInstance().getCountrycode();

            picker.setDisplayedValues(countries);
            picker.setMinValue(0);
            picker.setMaxValue(countries.length-1);
            picker.setValue(1);


            accept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    UserProfileData.getInstance().setHasUpdate(true);
                    UserProfileData.getInstance().setState(countrycode[picker.getValue()]);
                    textView.setText(countrycode[picker.getValue()]);
                    tooltip.dismiss();
                }
            });


        }else if (title.toLowerCase().contains("Title".toLowerCase())){
            final String[] role_title= {"Auditor","Crew","District Manager","General Manager","Relief Crew","Relief Manager","Supervisor","Support Manager"};
            final String[] role_code= {"AUDITOR","EMPLOYEER", "DM","GM","RELIEF CREW","RELIEF MGR","SUPERVISOR","OPS LEADER"};

            picker.setDisplayedValues(role_title);
            picker.setMinValue(0);
            picker.setMaxValue(role_title.length-1);
            picker.setWrapSelectorWheel(true);
            picker.setValue(role_title.length/2);


            accept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    UserProfileData.getInstance().setHasUpdate(true);
                    textView.setText(role_title[picker.getValue()]);
                    UserProfileData.getInstance().setRole(role_code[picker.getValue()]);
                    tooltip.dismiss();

                }
            });
        }

    }

    private void addTextWatcher(EditText editText, final String type){
        final UserProfileData data = UserProfileData.getInstance();

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (type.equalsIgnoreCase("firstname")){
                    data.setFirstName(s.toString());
                    StringBuilder builder = new StringBuilder();
                    builder.append(s.toString());
                    builder.append(" ");
                    if (data.getLastName()!=null){
                        builder.append(data.getLastName());
                    }else {
                        builder.append(user.lastName);
                    }
                    fullname.setText(builder.toString());
                }
                else if (type.equalsIgnoreCase("lastname")){
                    data.setLastName(s.toString());

                    StringBuilder builder = new StringBuilder();
                    if (data.getFirstName()!=null){
                        builder.append(data.getFirstName());
                        builder.append(" ");
                    }else {
                        builder.append(user.firstName);
                    }
                    builder.append(s.toString());
                    fullname.setText(builder.toString());

                }
                else if (type.equalsIgnoreCase("email")){
                    data.setContactEmail(s.toString());
                }
                else if (type.equalsIgnoreCase("pin")){
                    data.setpID(s.toString());
                }
                else if (type.equalsIgnoreCase("address")){
                    data.setAddress1(s.toString());
                }
                else if (type.equalsIgnoreCase("city")){
                    data.setCity(s.toString());
                }
                else if (type.equalsIgnoreCase("zip")){
                    data.setPostal(s.toString());
                }
                else if (type.equalsIgnoreCase("username")){
                    data.setUsername(s.toString());
                }
                else if (type.equalsIgnoreCase("employeeID")){
                    data.setEmployeeID(s.toString());
                }
                else if (type.equalsIgnoreCase("mobile")){
                    data.setDayPhone(s.toString());
                }
                data.setHasUpdate(true);
            }
            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    private void SaveUpdate(){
        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Please Wait");
        progressDialog.show();

        UserProfileData data = UserProfileData.getInstance();
        User user = CrewUDataManager.getInstance().getUser();

        try {
            UpdateBasicInfo(data,user,progressDialog);

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void UpdateBasicInfo(final UserProfileData data, final User user, final ProgressDialog progressDialog){

        progressDialog.setMessage("Saving Update");
        ArrayList<Boolean> needtoupdate = new ArrayList<>();
        User CurrentGm = getCurrentGM(progressDialog);

        if (data.getUsername()!=null){
            if (ValidString(data.getUsername())){
                user.username = data.getUsername();
                needtoupdate.add(true);
            }else {
                needtoupdate.add(false);
                progressDialog.dismiss();
                DisplaySnackbar("Please Input  Username",root);

            }
        }

        if (data.getLastName()!=null){
            if (ValidString(data.getLastName())){
                user.lastName = data.getLastName();
                needtoupdate.add(true);
            }else {
                needtoupdate.add(false);
                progressDialog.dismiss();
                DisplaySnackbar("Please Input Last Name",root);

            }
        }

        if (data.getFirstName()!=null){
            if (ValidString(data.getFirstName())){
                user.firstName = data.getFirstName();
                needtoupdate.add(true);
            }else {
                needtoupdate.add(false);
                progressDialog.dismiss();
                DisplaySnackbar("Please Input First Name",root);
            }
        }

        if (CurrentGm !=null){
            if (data.getRole()!=null){
                if (!data.getRole().equalsIgnoreCase("Gm")){
                    needtoupdate.add(true);
                }else {
                    if (!CurrentGm.userId.equalsIgnoreCase(user.userId)){
                        progressDialog.dismiss();
                        DisplaySnackbar("General Manager is already taken by "+ user.getFullName(), root);

                    }
                }
            }
        }

        if (data.getPassword()!=null){
            user.password = data.getPassword();
            needtoupdate.add(true);

        }

        if (data.getEmployeeID()!=null){
            user.employeeID = data.getEmployeeID();
            needtoupdate.add(true);

        }

        if (data.getContactEmail()!=null){
            user.contactEmail = data.getContactEmail();
            needtoupdate.add(true);

        }

        if (data.getpID()!=null){
            user.pID = data.getpID();
            needtoupdate.add(true);

        }
        if (data.getAddress1()!=null){
            user.address1 = data.getAddress1();
            needtoupdate.add(true);

        }
        if (data.getCity()!=null){
            user.city = data.getCity();
            needtoupdate.add(true);
        }

        if (data.getPostal()!=null){
            user.postal = data.getPostal();
            needtoupdate.add(true);

        }
        if (data.getDayPhone()!=null){
            user.dayPhone = data.getDayPhone();
            needtoupdate.add(true);
        }

        if (data.getDOB()!=null){
            user.DOB = data.getDOB();
            needtoupdate.add(true);
        }

        if (data.getHireDate()!=null){
            user.hireDate = data.getHireDate();
            needtoupdate.add(true);
        }

        if (data.getReviewDate()!=null){
            user.reviewDate = data.getReviewDate();
            needtoupdate.add(true);

        }
        if (data.getState()!=null){
            user.state = data.getState();
            needtoupdate.add(true);
        }

        if (data.getMobileCarrier()!=null){
            user.mobileCarrier = data.getMobileCarrier();
            needtoupdate.add(true);
        }

        if (data.getMobileCarrier()!=null){
            user.dayPhone = data.getDayPhone();
            needtoupdate.add(true);
        }

        if (needtoupdate.contains(true)){
            try {
                dataManager.updateUserRecordWithUserId(user, new OnCompleteListeners.updateUserRecordWithUserIdListener() {
                    @Override
                    public void updateUserRecordWithUserIdCallback(Boolean status, Error error) {
                        if (error == null){
                            UpdateSiteAllowed(data,user,progressDialog);
                            System.out.println("updateUserRecordWithUserId : SAVED");
                        }else {
                            DisplaySnackbar("ERROR SAVING UPDATE",root);
                            progressDialog.dismiss();
                        }
                    }
                });
            }catch (Exception e){
                e.printStackTrace();
                DisplaySnackbar("ERROR SAVING UPDATE",root);
                progressDialog.dismiss();
            }

        }else {
            UpdateSiteAllowed(data,user,progressDialog);
        }

    }

    private void UpdateSiteAllowed(UserProfileData data,User user,ProgressDialog progressDialog){
        System.out.println("UpdateSiteAllowed START ");
        int counter = 0;
        if (data.getCustomSites()!=null){
            for (CustomSite site : data.getCustomSites()){
                counter++;
                if (site.isUpdated()){
                    if (site.isCurrentSelected() != site.isTempValue()){
                        if (site.isTempValue()){
                            AddSiteAllowed(site.getSite(),progressDialog,user);
                        }
                        else {
                            RemoveSiteAllowed(site.getSite(),progressDialog,user);
                        }
                    }
                }
            }

            System.out.println("COUNTER == "+counter + " | "+data.getCustomSites().size());
            if (counter == data.getCustomSites().size()){
                updateROle(data,user,progressDialog);
            }
        }else {
            updateROle(data,user,progressDialog);
        }
    }

    private void AddSiteAllowed(final Site site, final ProgressDialog progressDialog, final User user){
        try {
            dataManager.AddUserToStore(user.userId, site.siteid, new OnCompleteListeners.updateUserRecordWithUserIdListener() {
                @Override
                public void updateUserRecordWithUserIdCallback(Boolean status, Error error) {
                    if (error == null){
                        System.out.println("AddUserToStore Success!");
                        dataManager.addRoleForUser(user.userId, site.sitename, new OnCompleteListeners.updateUserRecordWithUserIdListener() {
                            @Override
                            public void updateUserRecordWithUserIdCallback(Boolean status, Error error) {
                                if (error ==null){

                                }else {
                                    if (progressDialog!=null){
                                        if (progressDialog.isShowing()){
                                            progressDialog.dismiss();
                                            DisplaySnackbar("ERROR SAVING UPDATE",root);
                                        }
                                    }
                                }
                            }
                        });
                    }else {
                        if (progressDialog!=null){
                            if (progressDialog.isShowing()){
                                progressDialog.dismiss();
                                DisplaySnackbar("ERROR SAVING UPDATE",root);
                            }
                        }
                    }
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void RemoveSiteAllowed(final Site site, final ProgressDialog progressDialog, final User user){
        try {
            dataManager.RemoveUserFromStore(user.userId, site.siteid, new OnCompleteListeners.updateUserRecordWithUserIdListener() {
                @Override
                public void updateUserRecordWithUserIdCallback(Boolean status, Error error) {
                    if (error == null){
                        System.out.println("RemoveUserFromStore Success!");
                        dataManager.removeRoleForUser(user.userId,site.sitename , new OnCompleteListeners.updateUserRecordWithUserIdListener() {
                            @Override
                            public void updateUserRecordWithUserIdCallback(Boolean status, Error error) {
                                if (error == null){
                                    System.out.println("removeRoleForUser Success!");
                                }else {
                                    if (progressDialog!=null){
                                        if (progressDialog.isShowing()){
                                            progressDialog.dismiss();
                                            DisplaySnackbar("ERROR SAVING UPDATE",root);
                                        }
                                    }
                                }
                            }
                        });
                    }else {
                        if (progressDialog!=null){
                            if (progressDialog.isShowing()){
                                progressDialog.dismiss();
                                DisplaySnackbar("ERROR SAVING UPDATE",root);
                            }
                        }
                    }
                }
            });

        }catch (Exception e){
            e.printStackTrace();
            if (progressDialog!=null){
                if (progressDialog.isShowing()){
                    progressDialog.dismiss();
                    DisplaySnackbar("ERROR SAVING UPDATE",root);
                }
            }
        }

    }

    private void updateROle(final UserProfileData data, final User user, final ProgressDialog progressDialog){

        if (UserProfileData.getInstance().getRole()!=null){
            try {
                String currentrole = null;
                final String updatedrole = data.getRole();
                ArrayList<CustomRole>customRoles = CustomRole.getAllCustomRole(user.roles);

                if (customRoles.size()>0){
                    currentrole = customRoles.get(0).getCode();

                    final String finalCurrentrole = currentrole;
                    dataManager.removeRoleForUser(user.userId, currentrole, new OnCompleteListeners.updateUserRecordWithUserIdListener() {
                        @Override
                        public void updateUserRecordWithUserIdCallback(Boolean status, Error error) {

                            if (error == null){
                                System.out.println("removeRoleForUser Success!");
                                for (int i = 0; i <user.roles.size() ; i++) {
                                    String role = user.roles.get(i);
                                    if (role.equalsIgnoreCase(finalCurrentrole)){
                                        user.roles.remove(i);
                                        break;
                                    }
                                }

                                dataManager.addRoleForUser(user.userId, updatedrole, new OnCompleteListeners.updateUserRecordWithUserIdListener() {
                                    @Override
                                    public void updateUserRecordWithUserIdCallback(Boolean status, Error error) {
                                        if (error == null){
                                            user.roles.add(updatedrole);
                                            System.out.println("addRoleForUser Success!");
                                            DisplaySnackbar("UPDATES SAVED!",root);
                                            Pref.getInstance().setSharedpref();
                                            UserProfileData.getInstance().ClearInstance();
                                            progressDialog.dismiss();
                                        }else {
                                            progressDialog.dismiss();
                                        }
                                    }
                                });
                            }else {
                                progressDialog.dismiss();
                            }
                        }
                    });
                }
            }catch (Exception e){
                e.printStackTrace();
            }

        }else {
            progressDialog.dismiss();
            DisplaySnackbar("UPDATES SAVED!",root);
            UserProfileData.getInstance().ClearInstance();
            Pref.getInstance().setSharedpref();
        }


    }

    private User getCurrentGM(final ProgressDialog progressDialog) {
        User CurrentGM = null;
        CrewUDataManager data = CrewUDataManager.getInstance();
        if (data.getUsers() != null) {
            ArrayList<User> users = data.getUsers();
            for (User customUser : users) {
                if (customUser.active){
                    if (customUser.roles.contains("GM")) {
                        CurrentGM = customUser;
                        break;
                    }
                }
            }
        }
        return CurrentGM;
    }

    private void selectImage(Context context) {
        final CharSequence[] options = { "Take Photo", "Choose from Gallery","Cancel" };

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Choose your profile picture");

        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("Take Photo")) {
                    Intent m_intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                    imageTitle = timeStamp + ".jpg";

                    File file = new File( Environment.getExternalStoragePublicDirectory(
                            Environment.DIRECTORY_DCIM), imageTitle);

                    Uri uri = FileProvider.getUriForFile(getContext(), getActivity().getPackageName() + ".provider", file);
                    m_intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, uri);
                    startActivityForResult(m_intent, 0);

                } else if (options[item].equals("Choose from Gallery")) {
                    Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto , 1);


                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode != RESULT_CANCELED) {
            switch (requestCode) {
                case 0:
                    if (resultCode == RESULT_OK) {
                        File file = new File( Environment.getExternalStoragePublicDirectory(
                                Environment.DIRECTORY_DCIM), imageTitle);
                        if (file.exists()){
                            CrewUDataManager.getInstance().setProfilePath(file.getAbsolutePath());
                            Glide.with(getContext()).load(file).into(imageView9);
                            Pref.getInstance().setSharedpref();
                        }
                    }
                    break;
                case 1:
                    if (resultCode == RESULT_OK && data != null) {
                        Uri selectedImage =  data.getData();
                        String[] filePathColumn = {MediaStore.Images.Media.DATA};
                        if (selectedImage != null) {
                            Cursor cursor = requireActivity().getContentResolver().query(selectedImage,
                                    filePathColumn, null, null, null);
                            if (cursor != null) {
                                cursor.moveToFirst();

                                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                                String picturePath = cursor.getString(columnIndex);
                                cursor.close();

                                imageView9.setImageBitmap(BitmapFactory.decodeFile(picturePath));
                                CrewUDataManager.getInstance().setProfilePath(picturePath);
                                Pref.getInstance().setSharedpref();
                            }
                        }

                    }
                    break;
            }
        }
    }


}
