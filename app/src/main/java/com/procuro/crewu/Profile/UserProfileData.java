package com.procuro.crewu.Profile;


import android.view.View;

import com.procuro.apimmdatamanagerlib.Site;
import com.procuro.crewu.Tools;

import java.util.ArrayList;
import java.util.Date;

public class UserProfileData {

    private static UserProfileData instance = new UserProfileData();
    private boolean HasUpdate;
    private View view;

    //edit profile data temporary storage
    private String username;
    private String password;
    private String firstName;
    private String lastName;
    private String address1;
    private String address2;
    private String city;
    private String state;
    private String postal;
    private String employeeID;
    private String pID;
    private String cellPhone;
    private String dayPhone;
    private Date hireDate;
    private Date DOB;
    private String role;
    private String contactEmail;
    private String mobileCarrier;
    private Date reviewDate;
    private ArrayList<String>certificationList;
    private ArrayList<String>roles;
    private ArrayList<CustomSite>customSites;

    public void InitializeSaveButton(View view) {
        this.view = view;
    }

    public boolean isHasUpdate() {
        return HasUpdate;
    }

    public void setHasUpdate(boolean hasUpdate) {
        HasUpdate = hasUpdate;
        if (hasUpdate){
            Tools.EnableView(view,1);
        }else {
            Tools.DisableView(view,.5f);
        }
    }

    public ArrayList<CustomSite> getCustomSites() {
        return customSites;
    }

    public void setCustomSites(ArrayList<CustomSite> customSites) {
        this.customSites = customSites;
    }

    public static UserProfileData getInstance() {
        if(instance == null) {
            instance = new UserProfileData();
        }
        return instance;
    }


    public ArrayList<String> getRoles() {
        return roles;
    }

    public void setRoles(ArrayList<String> roles) {
        this.roles = roles;
    }


    public static void setInstance(UserProfileData instance) {
        UserProfileData.instance = instance;
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPostal() {
        return postal;
    }

    public void setPostal(String postal) {
        this.postal = postal;
    }

    public String getEmployeeID() {
        return employeeID;
    }

    public void setEmployeeID(String employeeID) {
        this.employeeID = employeeID;
    }

    public String getpID() {
        return pID;
    }

    public void setpID(String pID) {
        this.pID = pID;
    }

    public String getCellPhone() {
        return cellPhone;
    }

    public void setCellPhone(String cellPhone) {
        this.cellPhone = cellPhone;
    }

    public String getDayPhone() {
        return dayPhone;
    }

    public void setDayPhone(String dayPhone) {
        this.dayPhone = dayPhone;
    }

    public Date getHireDate() {
        return hireDate;
    }

    public void setHireDate(Date hireDate) {
        this.hireDate = hireDate;
    }

    public Date getDOB() {
        return DOB;
    }

    public void setDOB(Date DOB) {
        this.DOB = DOB;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public String getMobileCarrier() {
        return mobileCarrier;
    }

    public void setMobileCarrier(String mobileCarrier) {
        this.mobileCarrier = mobileCarrier;
    }

    public Date getReviewDate() {
        return reviewDate;
    }

    public void setReviewDate(Date reviewDate) {
        this.reviewDate = reviewDate;
    }

    public ArrayList<String> getCertificationList() {
        return certificationList;
    }

    public void setCertificationList(ArrayList<String> certificationList) {
        this.certificationList = certificationList;
    }

    public void ClearInstance(){
         username = null;
         password = null;
         firstName= null;
         lastName= null;
         address1= null;
         address2= null;
         city= null;
         state= null;
         postal= null;
         employeeID= null;
         pID= null;
         cellPhone= null;
         dayPhone= null;
         hireDate= null;
         DOB= null;
         role= null;
         contactEmail= null;
         mobileCarrier= null;
         reviewDate= null;
         ArrayList<String>certificationList= null;
         ArrayList<String>roles= null;
         ArrayList<CustomSite>customSites= null;
         setHasUpdate(false);
    }
}
