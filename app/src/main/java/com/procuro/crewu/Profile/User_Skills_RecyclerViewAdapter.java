package com.procuro.crewu.Profile;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.procuro.apimmdatamanagerlib.Certification;
import com.procuro.apimmdatamanagerlib.CertificationDefinition;
import com.procuro.apimmdatamanagerlib.User;
import com.procuro.crewu.R;
import com.procuro.crewu.Tools;

import java.util.ArrayList;

import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;

public class User_Skills_RecyclerViewAdapter extends RecyclerView.Adapter<User_Skills_RecyclerViewAdapter.MyViewHolder> {

    private Context mContext ;
    private ArrayList<CertificationDefinition>certificationDefinitions;
    private ArrayList<Certification> certficationlist;
    private User user;
    private boolean EnableEdit;


    public User_Skills_RecyclerViewAdapter(Context mContext , ArrayList<CertificationDefinition>certificationDefinitions,boolean enableEdit,User user) {
        this.mContext = mContext;
        this.certificationDefinitions = certificationDefinitions;
        this.EnableEdit = enableEdit;
        this.user = user;
        this.certficationlist =user.certificationList;


    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view ;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.user_skills_data,parent,false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final CertificationDefinition certificationDefinition = certificationDefinitions.get(position);

        final Certification[] certification = {getCertification(certificationDefinition.certification)};

        if (certification[0] !=null) {
            holder.checkBox.setBackgroundResource(R.drawable.star_selected);
            holder.checkBox.setText(certification[0].StarRating);

            int icon = Tools.getSkillIconByCertification(certification[0]);
            Glide.with(mContext).asDrawable().load(icon).into(holder.icon);

        }else {
            holder.checkBox.setBackgroundResource(R.drawable.star);
            holder.checkBox.setText("");
            certification[0] = new Certification();
            certification[0].Certification = certificationDefinition.certification;
            certification[0].StarRating = "0";
            int icon = Tools.getSkillIconByCertification(certification[0]);
            Glide.with(mContext).asDrawable().load(icon).into(holder.icon);

        }
        holder.name.setText(certificationDefinition.description);

        holder.checkBox.setEnabled(EnableEdit);

        holder.checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplayOptions(mContext,holder.checkBox, certification[0]);

            }
        });

    }

    @Override
    public int getItemCount() {
        return certificationDefinitions.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView icon;
        TextView name;
        Button checkBox ;


        public MyViewHolder(View itemView) {
            super(itemView);
            icon = itemView.findViewById(R.id.icon);
            name = itemView.findViewById(R.id.name);
            checkBox = itemView.findViewById(R.id.checkbox);
        }
    }

    public Certification getCertification(String selected){
        Certification certification = null;
        try {
            if (certficationlist!=null){
                for (Certification certification1 : certficationlist){
                    if (certification1.Certification.equalsIgnoreCase(selected)){
                        return certification1;
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return certification;
    }

    public void DisplayOptions(final Context context, View anchor,Certification certification) {

        final SimpleTooltip tooltip = new SimpleTooltip.Builder(context)
                .anchorView(anchor)
                .gravity(Gravity.TOP)
                .dismissOnOutsideTouch(true)
                .dismissOnInsideTouch(false)
                .showArrow(true)
                .arrowColor(ContextCompat.getColor(context, R.color.white))
                .transparentOverlay(true)
                .contentView(R.layout.skill_options_popup)
                .focusable(true)
                .build();
        tooltip.show();

        ListView listView = tooltip.findViewById(R.id.listView);
        ArrayList<String>levels = new ArrayList<>();
        for (int i = 1; i <6 ; i++) {
            levels.add(String.valueOf(i));
        }

        User_Skill_Options_ListviewAdapter adapter = new User_Skill_Options_ListviewAdapter(context,levels,certification,tooltip,
                this,user);
        listView.setAdapter(adapter);

    }


}
