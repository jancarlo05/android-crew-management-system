package com.procuro.crewu.Profile;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.procuro.apimmdatamanagerlib.Certification;
import com.procuro.apimmdatamanagerlib.OnCompleteListeners;
import com.procuro.apimmdatamanagerlib.User;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;
import com.procuro.crewu.R;

import java.util.ArrayList;

import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;


public class User_Skill_Options_ListviewAdapter extends BaseAdapter {

    // Declare Variables

    private Context mContext;
    private LayoutInflater inflater;
    private ArrayList<String> levels;
    private Certification selectedCertification;
    private SimpleTooltip tooltip;
    private User_Skills_RecyclerViewAdapter adapter;
    private ArrayList<Certification>certficationlist;
    private User user;
    aPimmDataManager dataManager = aPimmDataManager.getInstance();

    public User_Skill_Options_ListviewAdapter(Context context, ArrayList<String> levels,
                                              Certification selectedCertification,
                                              SimpleTooltip tooltip,
                                              User_Skills_RecyclerViewAdapter adapter,User user) {
        this.mContext = context;
        this.inflater = LayoutInflater.from(mContext);
        this.levels = levels;
        this.selectedCertification = selectedCertification;
        this.tooltip = tooltip;
        this.user =user;
        this.adapter = adapter;
        this.certficationlist =user.certificationList;

    }

    public class ViewHolder {
        TextView name;
    }

    @Override
    public int getCount() {
        return levels.size();
    }

    @Override
    public Object getItem(int position) {
        return levels.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("ViewHolder")
    public View getView(final int position, View view, ViewGroup parent) {
        view = inflater.inflate(R.layout.skill_options_list_data, null);
        final String level = levels.get(position);

        TextView name = view.findViewById(R.id.name);
        ImageView icon = view.findViewById(R.id.icon);
        ImageView indication = view.findViewById(R.id.indication);
        Button star = view.findViewById(R.id.star);
        ConstraintLayout body = view.findViewById(R.id.body);

        final Certification certif = new Certification();
        certif.Certification = selectedCertification.Certification;
        certif.StarRating = level;

        String title = "";
        int bodyColor = 0;
        int selectedIcon  = 0;

        boolean selected = false;

        if (selectedCertification.StarRating.equalsIgnoreCase(level)) {
            selected = true;
            indication.setVisibility(View.VISIBLE);
        }else {
            indication.setVisibility(View.GONE);
        }

        if (level.equalsIgnoreCase("1")){
            title = "COMPLIANT";
            bodyColor = ContextCompat.getColor(mContext,R.color.level1);
            selectedIcon = R.drawable.edit_1;

        }else if (level.equalsIgnoreCase("2")){
            title = "NOVICE";
            bodyColor = ContextCompat.getColor(mContext,R.color.level2);
            selectedIcon = R.drawable.edit_2;

        }else if (level.equalsIgnoreCase("3")){
            title = "INTERMEDIATE";
            bodyColor = ContextCompat.getColor(mContext,R.color.level3);
            selectedIcon = R.drawable.edit_3;

        }else if (level.equalsIgnoreCase("4")){
            title = "ADVANCE";
            bodyColor = ContextCompat.getColor(mContext,R.color.level4);
            name.setTextColor(ContextCompat.getColor(mContext,R.color.white));
            selectedIcon = R.drawable.edit_4;

        }else if (level.equalsIgnoreCase("5")){
            title = "ACE";
            bodyColor = ContextCompat.getColor(mContext,R.color.level5);
            name.setTextColor(ContextCompat.getColor(mContext,R.color.white));
            selectedIcon = R.drawable.edit_5;

        }

        try {

            body.setBackgroundColor(bodyColor);
            name.setText(title);
            star.setText(level);
            Glide.with(mContext).asDrawable().load(selectedIcon).into(icon);


            final boolean finalSelected = selected;
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (finalSelected){
                        certif.StarRating ="0";
                        selectedCertification = certif;
                        removeCertification(selectedCertification,certficationlist);
                    }else {
                        selectedCertification = certif;
                        updateCert(selectedCertification,certficationlist);
                    }
                    notifyDataSetChanged();
                    adapter.notifyDataSetChanged();
                    tooltip.dismiss();

                }
            });

        }catch (Exception e){
            e.printStackTrace();
        }




        return view;
    }

    private void updateCert(Certification selectedCertification, ArrayList<Certification> certficationlist){
        try {
            boolean found = false;
            for (Certification certification1 : certficationlist){
                if (certification1.Certification.equalsIgnoreCase(selectedCertification.Certification)){
                    certification1.StarRating = selectedCertification.StarRating;
                    updateToServer(certification1,true);
                    found = true;
                    break;
                }
            }
            if (!found){
                certficationlist.add(selectedCertification);
                updateToServer(selectedCertification,false);
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void removeCertification(Certification selectedCertification, ArrayList<Certification> certficationlist){
        try {
            for (int i = 0; i <certficationlist.size() ; i++) {
                Certification certification = certficationlist.get(i);
                if (certification.Certification.equalsIgnoreCase(selectedCertification.Certification)){
                    certficationlist.remove(i);
                    removeToServer(certification);
                    break;
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }


    private void removeToServer(final Certification certification){
        try {
            dataManager.RemoveCertificationFromUser(user.userId, certification.Certification, new OnCompleteListeners.updateUserRecordWithUserIdListener() {
                @Override
                public void updateUserRecordWithUserIdCallback(Boolean status, Error error) {
                    if (error==null){
//                        Toast.makeText(mContext, "Skill Removed", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void updateToServer(final Certification certification, final boolean updated){
        try {
            dataManager.updateStarRatingForUserCertification(user.userId, certification.Certification,certification.StarRating, new OnCompleteListeners.updateUserRecordWithUserIdListener() {
                @Override
                public void updateUserRecordWithUserIdCallback(Boolean status, Error error) {
                    if (error==null){
                        if (updated){
//                            Toast.makeText(mContext, "Skill Updated!", Toast.LENGTH_SHORT).show();
                        }else {
//                            Toast.makeText(mContext, "Skill Added!", Toast.LENGTH_SHORT).show();
                        }

                    }
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }

    }


}

